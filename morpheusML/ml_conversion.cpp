#include "ml_conversion.h"
#include <regex>
#include "morpheusML_rules.h"
/*
struct AutoFix {
		string match_path;
		string target_path;
		string require_path;
		enum Operation { COPY, MOVE, DELETE } operation;
		bool replace_existing;
		std::map<string,string> value_conversions;
		enum Severty { SILENT, INFO, WARN, CHECK, ERROR };
		Severty severty;
		AutoFix() : operation(MOVE), replace_existing(true), severty(Severty::INFO) {};
};*/


namespace MorpheusML {
	
	using std::vector;
	using std::cout; using std::endl;
	
	vector<Edit> applyFixes(XMLNode node, const vector<AutoFix>& fixes);
	
	struct XPathMatch {
		XMLNode node;
		string attr;
		string wildcard;
		vector<string> captures;
	};

	struct XCondition {
			string attribute;
			string value;
			bool capture;
			bool negate = false;
			bool regex = false;
	};
	
	struct XPathElement {
		
		string tag;
		std::vector<XCondition> conditions;
	};

	typedef std::deque<XPathElement> XPath;

	XPath parseXPath(string path);
	string createXPath(XMLNode node);
		
	std::vector<XPathMatch> matchPath(XMLNode domNode, XPath search_tags, bool create = false);
	

std::vector< Edit > convert(XMLNode xNode, int version)
{
	int current_version = 0;
	if (! getXMLAttribute(xNode,"version",current_version) ) {
		throw string("Missing version attribute in MorpheusML model ") + xNode.getName();
	}
	std::vector< Edit > edits;
	if (version>current_version){
		auto upgrade_rules = getRuleSets();
		while (version != current_version) {
			auto ruleset = upgrade_rules.find( {current_version,current_version+1} );
			if (ruleset == upgrade_rules.end()) {
				throw string("Unable to upgrade MorpheusML model from ") + to_str(current_version) + " to " + to_str(current_version+1) + "!";
			}
			auto e = applyFixes(xNode, ruleset->second);
			current_version++;
			setXMLAttribute(xNode, "version", current_version);
			edits.insert(edits.end(), e.begin(),e.end());
		}
	}
	if (version<current_version){
		auto downgrade_rules = MorpheusML::getRuleSets();
		while (version != current_version) {
			auto ruleset = downgrade_rules.find( {current_version,current_version-1} );
			if (ruleset == downgrade_rules.end()) {
				throw string("Unable to downgrade MorpheusML model from ") + to_str(current_version) + " to " + to_str(current_version-1) + "!";
			}
			auto e = applyFixes(xNode, ruleset->second);
			current_version--;
			setXMLAttribute(xNode, "version", current_version);
			edits.insert(edits.end(), e.begin(),e.end());
		}
	}
	
	return edits;
}


XPath parseXPath(string path)
{
	XPath xpath;
	str::remove_all_of(path, " ");
	auto tags  =  str::split(path, "/", str::SkipEmptyParts);
	if (tags.empty()) return xpath;
	string xattribute;
	if (str::startsWith(tags.back(), "@") ) {
		// Last denotes an attribute, not a tag
		xattribute = tags.back();
		tags.pop_back();
	}
	for (auto tag : tags) {
		auto bracket_open = tag.find("[");
		if (bracket_open == string::npos) {
			xpath.push_back( { tag, {} });
			continue;
		}
		
		vector<string> raw_conditions;
		if (tag.back() == ']') {
			auto t = tag.substr(bracket_open+1, tag.length()-bracket_open-2);
			raw_conditions = split(t, ",", str::SkipEmptyParts);
			tag.resize(bracket_open);
		}
		else {
			throw string("Missing closing bracket in ") + tag;
		}
		
		std::vector<XCondition> conditions;
		for (auto c : raw_conditions) {
			if (c.empty()) continue;

			XCondition cond;
			if ( c.front() == '(' && c.back() == ')') {
				cond.capture = true;
				c.erase(0,1);
				c.erase(c.size()-1,1);
			}
			else { 
				cond.capture = false;
			}
			
			std::smatch operator_match;
			
			if ( std::regex_search(c, operator_match, std::regex("!?=~?")) ) {
				/// index 0 has the whole match
				cond.negate = operator_match[0].str().front() == '!';
				cond.regex = operator_match[0].str().back() == '~';
				cond.attribute = operator_match.prefix().str();
				cond.value = operator_match.suffix().str();
// 				cout << "Found condition "<< c << " -> " << cond.attribute << (cond.negate ? "!=" :"=") << cond.value << endl; 
			}
			else {
				cond.attribute = c;
			}
			if (cond.attribute[0] == '@')
				cond.attribute.erase(0,1);
			conditions.push_back(cond);
		}
		
		xpath.push_back( { tag, conditions });
	}
	
	if (!xattribute.empty()) {
		xpath.push_back({ xattribute, {} });
	}
	
	return xpath;
}

string createXPath(XMLNode node)
{
// 	if ( node.isDeclaration() ) return "";
	if ( node.isEmpty() ) return "";
	
	auto parentPath = createXPath(node.getParentNode());
	
	auto nSiblings = node.getParentNode().nChildNode(node.getName());
	if ( nSiblings > 1 ) {
		
		for (auto unique_attr : {"symbol", "symbol-ref", "type"} ) {
			if (node.isAttributeSet(unique_attr)) {
				return parentPath +"/"+node.getName()+"[@" + unique_attr + "=\""+ node.getAttribute(unique_attr)+"\"]";
			}
		}

		int sibling=0;
		for ( int i=0; i<nSiblings; i++) {
			if (node.getParentNode().getChildNode(node.getName(),i) == node) {
				return parentPath + "/" + node.getName() + "["+ to_string(sibling) + "]";
			}
			else sibling++;
		}
	}
	
	return parentPath + "/" + node.getName();
}

std::vector<XMLNode> findNodeRecursive(XMLNode parent, string tag, int max_depth = std::numeric_limits<int>::max()) {
	std::vector<XMLNode> result;

	bool is_attribute = str::startsWith( tag, "@");
	if (is_attribute) tag.erase(0,1);
	
	std::deque<XMLNode> candidates {parent};
	int current_depth_candidates = 1;
	int next_depth_candidates = 0;
	int current_depth=0;
	
	while (!candidates.empty()) {
		auto cand = candidates.front();
		current_depth_candidates--;
		candidates.pop_front();
		if (is_attribute) {
			if (current_depth != 0) {
				if (cand.isAttributeSet(tag.c_str())) {
					result.push_back(cand);
				}
			}
		}
		int count = cand.nChildNode();
		for (int i=0; i<count; i++) {
			auto child = cand.getChildNode(i);
			if (!is_attribute && child.getName() == tag) {
				result.push_back(child);
			}
			
			if ( current_depth < max_depth) {
				candidates.push_back(child);
				next_depth_candidates++;
			}
		}
		if (current_depth_candidates==0) {
			current_depth++;
			current_depth_candidates = next_depth_candidates;
			next_depth_candidates = 0;
		}
	}
	return result;
}


std::vector<XPathMatch> matchPath(XMLNode domNode, XPath search_tags, bool create)
{
	std::vector<XPathMatch> sub_matches;
	if (search_tags.empty()) {
		sub_matches.push_back({domNode, {}});
		return sub_matches;
	}
	
	sub_matches.push_back( { domNode, {} } );
	if (search_tags.front().tag == domNode.getName())
		search_tags.pop_front();
	
// 	string  search_plain_path = string("/");
	
	while ( ! search_tags.empty() && ! sub_matches.empty() ) {
		std::vector<XPathMatch> new_matches;
		if (str::startsWith( search_tags.front().tag, "@")) {
			if (search_tags.front().tag == "@text") {
				// no need to create text nodes ...
// 					qDebug() << "looking for text ";
				
				for (int j=0; j<sub_matches.size();j++) {
					auto match =  sub_matches[j];
					match.attr ="text";
					new_matches.push_back(match);
				}
// 				new_matches = sub_matches;

			}
			else {
// 					qDebug() << "looking for attr " << search_tags_copy.front().replace("@","");
				string attr_name = search_tags.front().tag.erase(0,1);
				for (int j=0; j<sub_matches.size(); j++) {
// 					Q_ASSERT(sub_matches[j].node.isElement());
					if (sub_matches[j].node.isAttributeSet(attr_name.c_str())) {
						auto match =  sub_matches[j];
						match.attr = attr_name;
						new_matches.push_back(match);
					}
				}
				if (new_matches.empty() && create) {
					auto match =  sub_matches[0];
					match.attr = attr_name;
					new_matches.push_back(match);
				}
			}

		}
		else { // if search tag does not start with @
			for (int j=0; j<sub_matches.size();j++) {
// 				Q_ASSERT(sub_matches[j].node.isElement());
				vector<XPathMatch> candidates;
				if (search_tags.front().tag == "*") {
					// wildcard search
					if (search_tags.size()<2) {
						int count = sub_matches[j].node.nChildNode();
						for (int n=0; n<count; n++ ) {
							auto match = sub_matches[j];
							match.node = match.node.getChildNode(n);
							match.wildcard = match.node.getName();
							cout << "wildcard_string " << match.wildcard << endl;
							candidates.push_back ( match );
						}
					}
					else {
						search_tags.pop_front();
						auto cand_nodes = findNodeRecursive(sub_matches[j].node, search_tags.front().tag);
						for (auto& cand : cand_nodes) {
							auto d_node = cand.getParentNode();
							if (d_node == sub_matches[j].node) {
								candidates.push_back({cand, "", "", sub_matches[j].captures});
								cout << "wildcard_string " << cand.getName() << endl;
								continue;
							}
							
							string wildcard_string;
							while (true) {
								wildcard_string = d_node.getName() + wildcard_string;
								d_node = d_node.getParentNode();
								if (d_node == sub_matches[j].node)
									break;
								wildcard_string = string("/") + wildcard_string;
							}
							cout << "wildcard_string " << wildcard_string << endl;
							candidates.push_back({cand,"",wildcard_string, sub_matches[j].captures});
						}
					}
				}
				else {
					int count = sub_matches[j].node.nChildNode(search_tags.front().tag.c_str());
					for (int n=0; n<count; n++ ) {
						candidates.push_back ({sub_matches[j].node.getChildNode(search_tags.front().tag.c_str(), n),"","",sub_matches[j].captures});
					}
				}
				
				for (auto& child : candidates  ) {
					bool all_match = true;
					// TODO check conditions extract captures ...
					std::vector< std::string > captures;
					for (auto cond : search_tags.front().conditions) {
						if (!child.node.isAttributeSet(cond.attribute.c_str()) ) { all_match = false; break; }
						// Check a condition if value is present
						string value = child.node.getAttribute(cond.attribute.c_str());
						str::trim(value);
						if (!cond.value.empty() ) {
							
							bool r =
								cond.regex
								? std::regex_match(value, std::regex(cond.value))
								:  cond.value == value;

							r = cond.negate ? !r : r;
							
							if (!r) {
								all_match = false; break;
							}
// 							cout << "Checking condition " << cond.attribute << " = " << cond.value << " succeeded." << endl;
						}
						// Capture the node value 
						if (cond.capture) { 
							captures.push_back(value);
						}
					}
					
					if (all_match) {
						child.captures.insert(child.captures.end(), captures.begin(), captures.end());
						new_matches.push_back( child );
// 						cout << "Matched " << search_tags.front().tag << endl;
					}
				}
				// we might also check, whether tags==1 and tag is identical to an attribute ...
			}
			if (new_matches.empty() && create) {
				auto match = sub_matches[0];
				match.node = sub_matches[0].node.addChild(search_tags.front().tag.c_str());
				for (auto cond : search_tags.front().conditions) {
					match.node.addAttribute(cond.attribute.c_str(),cond.value.c_str());
				}
				new_matches.push_back( match );
			}
		}
		sub_matches = new_matches;
// 		if (search_tags.front().tag=="*") search_tags.pop_front();
		if (!search_tags.empty()) search_tags.pop_front();
	}
// 	for (const auto& match : sub_matches) {
// 		cout << createXPath(match.node) << endl;
// 	}
	return sub_matches;
}


std::vector< Edit > applyFixes(XMLNode node, const std::vector< AutoFix >& fixes)
{
	std::vector<Edit> edits;
	for (int i=0; i<fixes.size(); i++) {
		
// 		qDebug() << "Autofix rule nr. " << (i+1);
		auto search_path_string = fixes[i].match_path;
		auto search_path = parseXPath(search_path_string);
		vector<string> search_tags;
		for (const auto& el : search_path) search_tags.push_back(el.tag);
// 		cout << "Searching " << search_path_string << endl;

		std::vector<XPathMatch> matches = matchPath(node, search_path );
		
		if (matches.size()) {
			// moving the nodes in tree
			int n_success=0;
			for (int j=0; j<matches.size();j++) {
				
				auto match_search_tags = search_tags;
				auto wildcard_pos = std::find(match_search_tags.begin(),match_search_tags.end(),"*");
				if (wildcard_pos!=match_search_tags.end()) {
					auto wildcard_tags = str::split(matches[j].wildcard,"/",str::SkipEmptyParts);
					if (wildcard_tags.size() ==0) {
						match_search_tags.erase(wildcard_pos);
					}
					else {
						*wildcard_pos = wildcard_tags[0];
						for (int i=1; i<wildcard_tags.size(); i++) {
							match_search_tags.insert(wildcard_pos+i, wildcard_tags[i]);
						}
					}
				}
				
				Edit ed;
				if (! fixes[i].require_path.empty()) {
					string require_path = fixes[i].require_path;
					// adjust target path placeholder with the captures
					for (const auto& cap : matches[j].captures ) {
						if ( require_path.find("%") == string::npos) break;
						require_path = str::arg(require_path,cap);
					}
					
					// Relative Path resolution
					// Creating relative path for checking require_path
					deque<string> search_req_tags  { match_search_tags.begin(), match_search_tags.end() };
					auto require_tags = parseXPath(require_path);
					
					while ( !search_req_tags.empty() && !require_tags.empty() && search_req_tags.front() == require_tags.front().tag) {
		// 				qDebug() << "Removed leading tag " << search_req_tags.front() << " for relative path";
						search_req_tags.pop_front();
						require_tags.pop_front();
					}
					XMLNode require_parent = matches[j].node;
					if ( matches[j].attr.empty())
						require_parent = require_parent.getParentNode();
					
					for (int i=1; i<search_req_tags.size(); i++) {
						require_parent = require_parent.getParentNode();
					}
					
					if ( matchPath(require_parent, require_tags).empty() ) {
// 						cout << "Required path " << require_path << " not found.";
						continue;
					}
				}
				
				string search_xpath = createXPath(matches[j].node);
				str::remove(search_xpath, "/MorpheusModel");
				if ( !matches[j].attr.empty() )
					search_xpath += string("/@")+matches[j].attr;
				
				
				if (fixes[i].operation == AutoFix::DELETE) {
					
					if (!matches[j].attr.empty()) {
						ed.name =  matches[j].attr;
						ed.xml_parent = matches[j].node;
						ed.edit_type = Edit::AttribRemove;
						if (matches[j].attr=="text") {
							matches[j].node.deleteText();
							ed.info = string("Text ") + search_xpath + " was removed";
						}
						else {
							matches[j].node.deleteAttribute(matches[j].attr.c_str());
							ed.info = string("Attribute ") + search_xpath + " was removed";
						}
					}
					else {
						ed.name =  matches[j].node.getName();
						ed.xml_parent = matches[j].node.getParentNode();
						
						matches[j].node.deleteNodeContent();
						
						ed.edit_type = Edit::NodeRemove;
						ed.info = string("Element ") + search_xpath + " was removed";
					}
					ed.severty = fixes[i].severty;
					edits.push_back(ed);
					n_success++;
					continue;
				}
				
				// Creating the new Path and put the nodes there
				string target_path = fixes[i].target_path;
				for (const auto& cap : matches[j].captures ) {
					if ( target_path.find ("%") == string::npos) break;
					target_path = str::arg(target_path, cap);
				}
				if ( target_path.find ("*") != string::npos) {
					target_path.replace(target_path.find ("*"),1, matches[j].wildcard);
				}
					
				auto target_tags = parseXPath(target_path);
				if (target_tags.empty()) continue;
				auto target_name = target_tags.back();
				target_tags.pop_back();
				string target_xpath = target_path;
				str::remove(target_xpath, "/MorpheusModel");
				
				// Relative Path resolution
				deque<string> search_target_tags { match_search_tags.begin(), match_search_tags.end() };
				while ( !search_target_tags.empty() && !target_tags.empty() && search_target_tags.front() == target_tags.front().tag) {
// 					cout << "Removed leading tag " << search_target_tags.front() << " for relative path";
					search_target_tags.pop_front();
					target_tags.pop_front();
				}
				
				XMLNode target_parent = matches[j].node;
				if ( matches[j].attr.empty())
						target_parent = target_parent.getParentNode();
				
				string target_xpath_relative="";
				for (int i=1; i<search_target_tags.size(); i++) {
					target_parent = target_parent.getParentNode();
					target_xpath_relative.append("../");
				}
				
				if (target_xpath_relative.empty()) 
					target_xpath_relative = "./";
				for (const auto& tag : target_tags ) { 
					target_xpath_relative.append(tag.tag + "/");
				}
				target_xpath_relative.append(target_name.tag);
			
				// Find and if required create Path
				auto target_matches = matchPath(target_parent, target_tags, true);
// 				cout << "Moving " << createXPath(matches[j].node) << " to " << createXPath(target_matches.back().node) /*target_xpath_relative*/ << endl;
				
				if (target_matches.empty()) {
					cout << "Autofix target not found. " << target_xpath_relative << endl;
					continue;
				}
				else if (target_matches.size() > 1) {
					cout << "Autofix target is not unique. Selecting first match ..." << endl;
				}
				target_parent = target_matches[0].node;

				if (str::startsWith(target_name.tag, "@")) {
					
					string target_attr_name = target_name.tag;
					target_attr_name.erase(0,1);
					
					
					if (matches[j].attr.empty()) {
						cerr << "Autofix " << search_xpath << " -> " << target_xpath_relative << "Unable to move a node to an attribute!"  << endl;
						continue;
					}
					
					bool match_attr_is_text =  matches[j].attr == "text";
					string match_display_name  = match_attr_is_text ? "Text " : "Attribute ";
					string new_value;
					if ( match_attr_is_text ) {
						new_value = matches[j].node.nText() > 0 ? matches[j].node.getText() : "";
					}
					else { 
						new_value = matches[j].node.getAttribute(matches[j].attr.c_str());
					}
					
					if (target_attr_name != "text") {
						str::replace_all( new_value, "\n", " ");
						str::replace_all( new_value, "\r", " ");
						str::trim(new_value);
					}
					
					auto conv = fixes[i].value_conversions.find(new_value);
					if (conv != fixes[i].value_conversions.end()) {
						new_value = conv->second;
					}
					
					ed.edit_type = Edit::AttribRename;
					ed.xml_parent = target_parent;
					ed.name = target_attr_name;
					ed.severty = fixes[i].severty;
					
					if (target_attr_name == "text") {
						if ( target_parent == matches[j].node && match_attr_is_text ) {
							// Text of same parental node, just convert values
							target_parent.updateText(new_value.c_str());
							ed.info = match_display_name + search_xpath + " was changed to " + new_value;
						}
						else if ( target_parent.nText() == 0 || string(target_parent.getText()).find_first_not_of(" \n\t\r") == string::npos || fixes[i].replace_existing ) {
							// Assign to text ...
							target_parent.updateText(new_value.c_str());
							ed.info = match_display_name + search_xpath + (fixes[i].operation == AutoFix::MOVE ? " was moved to " : " was copied to " ) + target_xpath;
							
							if (fixes[i].operation == AutoFix::MOVE) {
								matches[j].node.deleteText();
							}
						}
						else {
							continue;
						}
					}
					else {  // target_attr_name != "text"
						if (target_parent == matches[j].node && matches[j].attr == target_attr_name ) {
							// same attribute, just convert the values
							ed.info = match_display_name + search_xpath + " was changed to " + new_value;
							target_parent.updateAttribute(new_value.c_str(), target_attr_name.c_str(), matches[j].attr.c_str());
						}
						else if ( fixes[i].replace_existing || ! target_parent.isAttributeSet(target_attr_name.c_str()) ) {
							// Assign to an attribute ...
							ed.info = match_display_name + search_xpath + (fixes[i].operation == AutoFix::MOVE ? " was moved to " : " was copied to " ) + target_xpath;
							target_parent.addAttribute( ed.name.c_str(), new_value.c_str());
							
							if (fixes[i].operation == AutoFix::MOVE) {
								matches[j].node.deleteAttribute(matches[j].attr.c_str());
							}
						}
						else {
							continue;
						}
					}
				}
				else {
					if (target_parent.getChildNode(target_name.tag.c_str()).isEmpty() || fixes[i].replace_existing) {
						XMLNode target_node;
						if (fixes[i].operation == AutoFix::MOVE) 
							target_node = target_parent.addChild(matches[j].node);
						else 
							target_node = target_parent.addChild(matches[j].node.deepCopy());
						
						target_node.updateName(target_name.tag.c_str());
						
						for (const auto& cond : target_name.conditions) {
							if (cond.attribute == "text") {
								target_node.updateText(cond.value.c_str());
							}
							else
								target_node.addAttribute(cond.attribute.c_str(), cond.value.c_str());
						}
						
						ed.name =  matches[j].node.getName();
						ed.xml_parent = target_node;
						ed.severty =fixes[i].severty;
						if (fixes[i].operation == AutoFix::MOVE) {
							ed.edit_type = Edit::NodeMove;
							ed.info = string("Element ") + search_xpath + " was moved to " + target_xpath;
						}
						else {
							ed.edit_type = Edit::NodeAdd;
							ed.info = string("Element ") + search_xpath + " was copied to " + target_xpath;
						}
						
						if (ed.name.empty())
							ed.name = target_name.tag;
					}
				}
				edits.push_back(ed);
// 				cout << ed.info << endl;
				n_success++;
			}
			if (matches.size())
				cout << "AutoFix: Moved " << n_success << " element(s) " << " matching " << search_path_string << " to " << fixes[i].target_path << endl;
		}
	}
	return edits;
}


} // namspace MorpheusML
