set(util_src 
	parallel_thread_pool.cpp
)

target_sources_relpaths(MorpheusCore PRIVATE ${util_src})
