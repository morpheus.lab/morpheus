#include "gtest/gtest.h"
#include "core/random_functions.h"

const double min_prec_tol = 1e-15;
using ::testing::Gt;
using ::testing::Lt;

TEST (Random, BOOL) {
	
	uint random_seed = time(NULL);
	cout << "Time/RandomSeed not specified, using arbitray seed (based on time)." << endl;
	setRandomSeed(random_seed);
	#pragma omp parallel 
	{
		TP::setTeamNum(omp_get_thread_num());
	}
	// Draw a number of bools
	const int draws = 10000;
	// Parameters of the expected binomial distribution
	const double expect = 0.5 * draws;
	const double variance = 0.5 * 0.5 * draws;
	
	double sum=0;
#pragma omp parallel for  reduction(+: sum)
	for (uint i=0; i<draws; i++) {
		sum += getRandomBool();
	}
	// infer probability 
	double mean = 1.0 * sum / draws;
	double z_level = 1.96; // => 95% confidence
	double interval = 1.5 * z_level/pow(draws,1.5) * sqrt(sum * (draws-sum));
	cout << "Random bool " << mean << " +/- " << interval << endl;
	EXPECT_GT(mean+interval, 0.5) << "Mean of boolean random number outside of range";
	EXPECT_LT(mean-interval, 0.5) << "Mean of boolean random number outside of range";
// 	EXPECT_THAT(expect, testing::AllOf(Gt(mean-interval),Lt(mean+interval)) << "Mean of boolean random number outside of range";
}
