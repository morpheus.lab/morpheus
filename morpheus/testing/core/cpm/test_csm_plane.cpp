#include "model_test.h"
#include "core/simulation.h"
#include "core/celltype.h"
using namespace Eigen;
const double confidence = 0.95;

// Hex Stats
struct Data {double vol_mean; double vol_sigma; double surface_mean; double surface_sigma; };
std::vector<Data> hex_data =
{ { 1395.12, 2.92, 1090.81, 0.77 },
  { 1260.24, 5.46, 411.19, 0.31 },
  { 1255.39, 5.83, 293.18, 0.28 },
  { 1251.17, 6.46, 244.23, 0.22 },
  { 1267.33, 5.49, 519.91, 0.80 },
  { 1252.27, 6.44, 242.19, 0.38 },
  { 1243.68, 5.44, 194.34, 0.33 },
  { 1238.19, 6.62, 174.50, 0.19 },
  { 875.65, 14.34, 99.28, 0.80 },
  { 983.46, 11.68, 106.69, 0.59 },
  { 1033.46, 10.17, 110.70, 0.43 },
  { 1065.00, 9.00, 113.46, 0.32 },
  { 1.00, 0.00, 1.72, 0.01 },
  { 1.00, 0.00, 1.72, 0.01 },
  { 32.75, 24.61, 10.70, 6.14 },
  { 370.37, 16.23, 63.46, 1.35 }
};


std::vector<Data> square_data =
{ { 1329.09, 3.27, 938.41, 0.58 },
  { 1258.43, 4.74, 365.73, 0.29 },
  { 1255.37, 5.61, 266.51, 0.20 },
  { 1250.71, 6.39, 225.37, 0.18 },
  { 1257.43, 5.10, 365.90, 0.55 },
  { 1248.02, 5.37, 197.08, 0.28 },
  { 1238.74, 4.25, 168.10, 0.23 },
  { 1227.40, 6.97, 156.09, 0.19 },
  { 74.62, 61.40, 19.71, 13.48 },
  { 625.25, 14.23, 89.02, 1.00 },
  { 763.41, 10.42, 98.75, 0.65 },
  { 839.20, 6.65, 103.91, 0.36 },
  { 1.00, 0.00, 1.77, 0.01 },
  { 1.00, 0.00, 1.77, 0.01 },
  { 1.00, 0.00, 1.77, 0.01 },
  { 69.65, 3.26, 29.79, 0.68 }
};


TEST (CSMplane, Hexagonal) {
	
	auto file1 = ImportFile("csm_plane_hex.xml");
	auto model = TestModel(file1.getDataAsString());

	const double init_time = 1200;
	const int data_points = 50;
	const double data_interval = 10;

	auto generator = [&]() -> ArrayXd {
		VectorXd yd(data_points);
		try {
			model.init();
			auto celltype = CPM::getCellTypesMap()["Cell"].lock();
			
			auto cell_volume = celltype->getScope()->findSymbol<double>("cell.volume");
			EXPECT_TRUE(cell_volume);
			auto cell_surface = celltype->getScope()->findSymbol<double>("cell.surface");
			EXPECT_TRUE(cell_surface);
			
			model.run(init_time);
			const auto& cells = celltype->getCellIDs();
			ArrayXd data(2*cells.size());
			
			for (int i=0; i< cells.size(); i++) {
				data[2*i] = cell_volume->get(SymbolFocus(cells[i]));
				data[2*i+1] = cell_surface->get(SymbolFocus(cells[i]));
			}
			
			for (int j=1; j<data_points; j++ ) {
				model.run(data_interval);
				for (int i=0; i< cells.size(); i++) {
					data[2*i] += cell_volume -> get(SymbolFocus(cells[i]));
					data[2*i+1] += cell_surface -> get(SymbolFocus(cells[i]));
				}
			}
			data /= data_points;
			return data;
		}
		catch (const string &e) {
			cout << e;
		}
		catch (const MorpheusException& e) {
			cout << e.what();
		}
		return {};
	};
	
	ArrayXd means(hex_data.size()*2);
	ArrayXd stddevs(hex_data.size()*2);
	int i=0;
	for (const auto& data : hex_data) {
		means[i] = data.vol_mean; stddevs[i]=data.vol_sigma;
		i++;
		means[i] = data.surface_mean; stddevs[i] = data.surface_sigma;
		i++;
	}
	
	EXPECT_NORMAL_DISTRIB(means, stddevs, generator);
}

TEST (CSMplane, Square) {
	
	auto file1 = ImportFile("csm_plane_square.xml");
	auto model = TestModel(file1.getDataAsString());

	const double init_time = 1200;
	const int data_points = 50;
	const double data_interval = 10;

	auto generator = [&]() -> ArrayXd {
		VectorXd yd(data_points);
		try {
			model.init();
			auto celltype = CPM::getCellTypesMap()["Cell"].lock();
			
			auto cell_volume = celltype->getScope()->findSymbol<double>("cell.volume");
			EXPECT_TRUE(cell_volume);
			auto cell_surface = celltype->getScope()->findSymbol<double>("cell.surface");
			EXPECT_TRUE(cell_surface);
			
			model.run(init_time);
			const auto& cells = celltype->getCellIDs();
			ArrayXd data(2*cells.size());
			
			for (int i=0; i< cells.size(); i++) {
				data[2*i] = cell_volume->get(SymbolFocus(cells[i]));
				data[2*i+1] = cell_surface->get(SymbolFocus(cells[i]));
			}
			
			for (int j=1; j<data_points; j++ ) {
				model.run(data_interval);
				for (int i=0; i< cells.size(); i++) {
					data[2*i] += cell_volume -> get(SymbolFocus(cells[i]));
					data[2*i+1] += cell_surface -> get(SymbolFocus(cells[i]));
				}
			}
			data /= data_points;
			return data;
		}
		catch (const string &e) {
			cout << e;
		}
		catch (const MorpheusException& e) {
			cout << e.what();
		}
		return {};
	};
	
	ArrayXd means(square_data.size()*2);
	ArrayXd stddevs(square_data.size()*2);
	int i=0;
	for (const auto& data : square_data) {
		means[i] = data.vol_mean; stddevs[i]=data.vol_sigma;
		i++;
		means[i] = data.surface_mean; stddevs[i] = data.surface_sigma;
		i++;
	}
	
	EXPECT_NORMAL_DISTRIB(means, stddevs, generator);
}
