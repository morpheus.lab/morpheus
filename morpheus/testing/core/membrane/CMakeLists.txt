add_executable(runMembraneTests membrane_dyn.cpp)
#set_property(TARGET runFieldTests PROPERTY LABELS "ODESolver" APPEND)
InjectModels(runMembraneTests)
target_link_libraries(runMembraneTests PRIVATE ModelTesting gtest gtest_main) # MorpheusCore
# Register test to CTest infrastructure
gtest_discover_tests( runMembraneTests WORKING_DIRECTORY "${PROJECT_DIR}")

