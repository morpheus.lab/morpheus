add_executable(runFieldTests field_initialization_test.cpp field_diffusion.cpp)
#set_property(TARGET runFieldTests PROPERTY LABELS "ODESolver" APPEND)
InjectModels(runFieldTests)
target_link_libraries(runFieldTests PRIVATE ModelTesting gtest gtest_main) # MorpheusCore
# Register test to CTest infrastructure
gtest_discover_tests( runFieldTests WORKING_DIRECTORY "${PROJECT_DIR}")

