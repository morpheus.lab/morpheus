#include "model_test.h"
#include "core/simulation.h"
#include "eigen/unsupported/Eigen/LevenbergMarquardt"

using namespace Eigen;

struct sortingDecay_functor : DenseFunctor<double>
{
	const VectorXd &xd; const VectorXd &yd;
	sortingDecay_functor(const VectorXd &xd, const VectorXd &yd): Eigen::DenseFunctor<double>(3,xd.size()), xd(xd), yd(yd) {}
	
	int operator()(const VectorXd &p, VectorXd& fvec) const {
		assert(p.size() == 3);
		assert(fvec.size() == xd.size());
		assert(fvec.size() == yd.size());
		auto a = p[0];
		auto b = p[1];
		auto c = abs(p[2]);
		for ( int i=0; i<fvec.size(); i++ ) {
			fvec[i] = yd[i] -  (a*std::pow(xd[i],b) + c);
		}
		return 0;
	}
};




vector<double> generateStats(string model_file) 
{
	auto model = TestModel(model_file);
	
	const int repetitions = 50;
	ArrayXd boundary_exponents(repetitions);
	
	double init_time = 20;
	int data_points = 20;
	double data_interval = 10;
	
	for (int r=0; r<repetitions; r++ ) {
		
		VectorXd xd(data_points);
		VectorXd yd(data_points);
		
		try {
			model.init();
			auto boundary = SIM::findGlobalSymbol<double>("boundary");
			assert(boundary);
			
			model.run(init_time);
			xd[0] = init_time;
			yd[0] = boundary->get(SymbolFocus::global);
			for (int i=1; i<data_points; i++ ) {
				model.run(data_interval);
				xd[i] = init_time + i*data_interval;
				yd[i] = boundary->get(SymbolFocus::global);
			}
		}
		catch (const string &e) {
			cout << e;
			break;
		}
		catch (const MorpheusException& e) {
			cout << e.what();
			break;
		}
		
		// Fitting sorting boundary decay dynamics
		VectorXd p(3);
		p << 4000, -0.2, 300;
		sortingDecay_functor f_fitting(xd,yd);
		NumericalDiff<sortingDecay_functor> fitting_diff(f_fitting);
		LevenbergMarquardt<NumericalDiff<sortingDecay_functor>> fit(fitting_diff);
		/*auto info =*/ fit.minimize(p);
		std::cout << "Fitting result " << p << std::endl;
		
		boundary_exponents[r] = p[1];
		
	}
	
	double mean_boundary_exponent = boundary_exponents.mean();
	double std_boundary_exponent  = sqrt(( boundary_exponents - mean_boundary_exponent).square().sum() / (boundary_exponents.size() - 1));
	
	return {mean_boundary_exponent, std_boundary_exponent};
}


int main(int argc, char** argv) {
	auto hex_file = ImportFile("Test_CellSorting_2D_hex.xml");
	auto hex_stats = generateStats(hex_file.getDataAsString());
	auto square_file = ImportFile("Test_CellSorting_2D_square.xml");
	auto square_stats = generateStats(square_file.getDataAsString());
	auto cubic_file = ImportFile("Test_CellSorting_3D_cubic.xml");
	auto cubic_stats = generateStats(cubic_file.getDataAsString());
	
	std::cout << std::setprecision(8) << "Hexagonal stats:\nMean:  " << hex_stats[0] << "\nStdDev:  " << hex_stats[1] << std::endl;
	std::cout << std::setprecision(8) << "Square stats:\nMean:  " << square_stats[0] << "\nStdDev:  " << square_stats[1] << std::endl;
	std::cout << std::setprecision(8) << "Cubic stats:\nMean:  " << cubic_stats[0] << "\nStdDev:  " << cubic_stats[1] << std::endl;
	
	
	return 0;
}
