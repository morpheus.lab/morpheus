#include "gtest/gtest.h"
#include "model_test.h"
#include "core/simulation.h"
#include "eigen/unsupported/Eigen/LevenbergMarquardt"

using namespace Eigen;

struct sortingDecay_functor : DenseFunctor<double>
{
	const VectorXd &xd; const VectorXd &yd;
	sortingDecay_functor(const VectorXd &xd, const VectorXd &yd): Eigen::DenseFunctor<double>(3,xd.size()), xd(xd), yd(yd) {}
	
	int operator()(const VectorXd &p, VectorXd& fvec) const {
		assert(p.size() == 3);
		assert(fvec.size() == xd.size());
		assert(fvec.size() == yd.size());
		auto a = p[0];
		auto b = p[1];
		auto c = abs(p[2]);
		for ( int i=0; i<fvec.size(); i++ ) {
			fvec[i] = yd[i] -  (a*std::pow(xd[i],b) + c);
		}
		return 0;
	}
};

TEST (CellSorting, Hexagonal) {
	
	auto file1 = ImportFile("Test_CellSorting_2D_hex.xml");
	auto model = TestModel(file1.getDataAsString());

	const double init_time = 20;
	const int data_points = 20;
	const double data_interval = 10;

	const double mean_boundary_exponent = -0.149;
	const double std_boundary_exponent  =  0.012;

	auto generator = [&]() -> double {
		
		VectorXd xd(data_points);
		VectorXd yd(data_points);
		try {
			model.init();
			auto boundary = SIM::findGlobalSymbol<double>("boundary");
			EXPECT_TRUE(boundary != nullptr);
			model.run(init_time);
			xd[0] = init_time;
			yd[0] = boundary->get(SymbolFocus::global);
			for (int i=1; i<data_points; i++ ) {
				model.run(data_interval);
				xd[i] = init_time + i*data_interval;
				yd[i] = boundary->get(SymbolFocus::global);
			}
		}
		catch (const string &e) {
			cout << e;
			throw e;
		}
		catch (const MorpheusException& e) {
			cout << e.what();
			throw e;
		}
		
		// Fitting sorting boundary decay dynamics
		VectorXd p(3);
		p << 4000, -0.2, 300;
		sortingDecay_functor f_fitting(xd,yd);
		NumericalDiff<sortingDecay_functor> fitting_diff(f_fitting);
		LevenbergMarquardt<NumericalDiff<sortingDecay_functor>> fit(fitting_diff);
		auto info = fit.minimize(p);
// 		std::cout << "Fitting result " << p << std::endl;
		return p[1];
	};
	EXPECT_NORMAL_DISTRIB(mean_boundary_exponent, std_boundary_exponent, generator);

// 	double boundary_exponent_deviation = abs(p[1]-mean_boundary_exponent);
// 	std::cout << mean_boundary_exponent << " ~ " << p[1] << std::endl;
// 	EXPECT_TRUE( boundary_exponent_deviation < 2*std_boundary_exponent ) ;
	
}


TEST (CellDrift, Square) {
	auto file2 = ImportFile("Test_CellDrift_2D_square.xml");
	auto model = TestModel(file2.getDataAsString());
	
	model.init();
	auto drift = SIM::findGlobalSymbol<double>("drift.abs");
	EXPECT_TRUE(drift != nullptr);
	auto drift_tol = SIM::findGlobalSymbol<double>("drift_tol");
	EXPECT_TRUE(drift_tol != nullptr);
	model.run();
	EXPECT_LE(drift->get(SymbolFocus::global), drift_tol->get(SymbolFocus::global));
}


TEST (CellSorting, Square) {
	auto file2 = ImportFile("Test_CellSorting_2D_square.xml");
	auto model = TestModel(file2.getDataAsString());
	
	const double mean_boundary_exponent = -0.169;
	const double std_boundary_exponent  =  0.017;
	double init_time = 20;
	int data_points = 20;
	double data_interval = 10;
	
	auto generator = [&]() -> double {
		VectorXd xd(data_points);
		VectorXd yd(data_points);
		try {
			model.init();
			auto boundary = SIM::findGlobalSymbol<double>("boundary");
			EXPECT_TRUE(boundary != nullptr);
			model.run(init_time);
			xd[0] = init_time;
			yd[0] = boundary->get(SymbolFocus::global);
			for (int i=1; i<data_points; i++ ) {
				model.run(data_interval);
				xd[i] = init_time + i*data_interval;
				yd[i] = boundary->get(SymbolFocus::global);
			}
		}
		catch (const string &e) {
			cout << e;
		}
		catch (const std::exception& e) {
			cout << e.what();
		}
	
		// Fitting sorting boundary decay dynamics
		VectorXd p(3);
		p << 4000, -0.2, 300;
		sortingDecay_functor f_fitting(xd,yd);
		NumericalDiff<sortingDecay_functor> fitting_diff(f_fitting);
		LevenbergMarquardt<NumericalDiff<sortingDecay_functor>> fit(fitting_diff);
		auto info = fit.minimize(p);
		std::cout << "Fitting result " << p << std::endl;
		return p[1];
	};

	EXPECT_NORMAL_DISTRIB(mean_boundary_exponent, std_boundary_exponent, generator);
	
}

TEST (CellDrift, Cubic) {
	auto file2 = ImportFile("Test_CellDrift_3D_cubic.xml");
	auto model = TestModel(file2.getDataAsString());
	
	model.init();
	auto drift = SIM::findGlobalSymbol<double>("drift.abs");
	EXPECT_TRUE(drift != nullptr);
	auto drift_tol = SIM::findGlobalSymbol<double>("drift_tol");
	EXPECT_TRUE(drift_tol != nullptr);
	model.run();
	EXPECT_LE(drift->get(SymbolFocus::global), drift_tol->get(SymbolFocus::global));
}

TEST (CellSorting, Cubic) {
	auto file3 = ImportFile("Test_CellSorting_3D_cubic.xml");
	auto model = TestModel(file3.getDataAsString());

	const double mean_boundary_exponent = -0.408;
	const double std_boundary_exponent  =  0.082;
	
	double init_time = 20;
	int data_points = 20;
	double data_interval = 10;
	
	auto generator = [&]() -> double {
		VectorXd xd(data_points);
		VectorXd yd(data_points);
		try {
			model.init();
			auto boundary = SIM::findGlobalSymbol<double>("boundary");
			EXPECT_TRUE(boundary != nullptr);
			model.run(init_time);
			xd[0] = init_time;
			yd[0] = boundary->get(SymbolFocus::global);
			for (int i=1; i<data_points; i++ ) {
				model.run(data_interval);
				xd[i] = init_time + i*data_interval;
				yd[i] = boundary->get(SymbolFocus::global);
			}
		}
		catch (const string &e) {
			cout << e;
		}
		catch (const std::exception& e) {
			cout << e.what();
		}
		
		// Fitting sorting boundary decay dynamics
		VectorXd p(3);
		p << 4000, -0.2, 300;
		sortingDecay_functor f_fitting(xd,yd);
		NumericalDiff<sortingDecay_functor> fitting_diff(f_fitting);
		LevenbergMarquardt<NumericalDiff<sortingDecay_functor>> fit(fitting_diff);
		auto info = fit.minimize(p);
// 		std::cout << "Fitting result " << p << std::endl;
		return p[1];
	};

	EXPECT_NORMAL_DISTRIB(mean_boundary_exponent, std_boundary_exponent, generator);
}
