<?xml version='1.0' encoding='UTF-8'?>
<MorpheusModel version="4">
    <Description>
        <Details>Full title:	Viral dynamics in monkey and bat cell lines
Date:	15.06.2022
Authors:	Cara E Brook, Mike Boots, Kartik Chandran, Andrew P Dobson, Christian Drosten, Andrea L Graham, Bryan T Grenfell, Marcel A Müller, Melinda Ng, Lin-Fa Wang, Anieke van Leeuwen
Contributors: Chetan Munegowda, Madina Japakhova, Shanya Singhal, Justin Bürger, Cara E Brook, Lutz Brusch
Software:	Morpheus (open-source), download from https://morpheus.gitlab.io
Time unit:      hours post infection
Space unit:    lattice interval corresponds to cell size
ModelID:	https://identifiers.org/morpheus/M7677
Reference:	This model reproduces the results in this publication which were originally obtained with a different simulator:
	Brook et al. (2020) Accelerated viral dynamics in bat cell lines, with implications for zoonotic emergence. eLife 9:e48401.
	https://doi.org/10.7554/eLife.48401</Details>
        <Title>Viral Dynamics</Title>
    </Description>
    <Space>
        <Lattice class="hexagonal">
            <Neighborhood>
                <Order>5</Order>
            </Neighborhood>
            <Size symbol="size" value="110, 127, 0"/>
            <BoundaryConditions>
                <Condition type="periodic" boundary="x"/>
                <Condition type="constant" boundary="y"/>
            </BoundaryConditions>
        </Lattice>
        <SpaceSymbol symbol="space"/>
    </Space>
    <Time>
        <StartTime value="0"/>
        <StopTime value="200"/>
        <TimeSymbol symbol="time" name="Time [hours post infection]"/>
        <RandomSeed value="0"/>
    </Time>
    <CellTypes>
        <CellType name="infectious" class="biological">
            <ChangeCellType time-step="dt" newCellType="dead" name="Natural death of infectious cells">
                <Condition>time > (t_birth+tau_mu)</Condition>
                <Triggers>
                    <Rule symbol-ref="t_start">
                        <Expression>time</Expression>
                    </Rule>
                </Triggers>
            </ChangeCellType>
            <ChangeCellType time-step="dt" newCellType="dead" name="Death of infectious cells due to lysis">
                <Condition>time > (t_start+tau_alpha)</Condition>
                <Triggers>
                    <Rule symbol-ref="t_start">
                        <Expression>time</Expression>
                    </Rule>
                </Triggers>
            </ChangeCellType>
            <Property symbol="tau_mu" value="rand_norm(1/mu,1/mu/2)"/>
            <Property symbol="tau_alpha" value="rand_norm(1/alpha,1/alpha/4)"/>
            <Property symbol="t_start" value="0.0"/>
            <Property symbol="t_birth" value="0.0"/>
        </CellType>
        <CellType name="dead" class="biological">
            <Property symbol="cDS" name="cDS" value="0.0"/>
            <Property symbol="cDA" name="cDA" value="0.0"/>
            <Property symbol="cSS" name="cSS" value="0.0"/>
            <NeighborhoodReporter time-step="dt" name="cDS">
                <Input noflux-cell-medium="false" scaling="cell" value="(sqrt((cell.center.x-local.cell.center.x)^2+(cell.center.y-local.cell.center.y)^2)&lt;1.5)*(cell.type == celltype.susceptible.id)/6"/>
                <Output symbol-ref="cDS" mapping="sum"/>
            </NeighborhoodReporter>
            <NeighborhoodReporter time-step="dt" name="cDA">
                <Input noflux-cell-medium="false" scaling="cell" value="(sqrt((cell.center.x-local.cell.center.x)^2+(cell.center.y-local.cell.center.y)^2)&lt;1.5)*(cell.type == celltype.antiviral.id)/6"/>
                <Output symbol-ref="cDA" mapping="sum"/>
            </NeighborhoodReporter>
            <NeighborhoodReporter time-step="dt" name="cSS">
                <Input noflux-cell-medium="false" scaling="cell" value="cell.type == celltype.susceptible.id"/>
                <Output symbol-ref="cSS" mapping="average"/>
            </NeighborhoodReporter>
            <ChangeCellType time-step="dt" newCellType="susceptible" name="Regeneration of dead cells because of antiviral neighbour cells">
                <Condition>rand_uni(0,1) &lt; (b*cDA*dt)</Condition>
                <Triggers>
                    <Rule symbol-ref="t_start">
                        <Expression>time</Expression>
                    </Rule>
                    <Rule symbol-ref="tau_mu">
                        <Expression>rand_norm(1/mu,1/mu/2)</Expression>
                    </Rule>
                    <Rule symbol-ref="tau_epsilon">
                        <Expression>if((epsilon*cSS)>0,rand_norm(1/(epsilon*cSS),1/(epsilon*cSS)/4),100000000000)</Expression>
                    </Rule>
                    <Rule symbol-ref="t_birth">
                        <Expression>time</Expression>
                    </Rule>
                </Triggers>
            </ChangeCellType>
            <ChangeCellType time-step="dt" newCellType="susceptible" name="Regeneration of dead cells because of susceptible neighbour cells">
                <Condition>rand_uni(0,1) &lt; (b*cDS*dt)</Condition>
                <Triggers>
                    <Rule symbol-ref="t_start">
                        <Expression>time</Expression>
                    </Rule>
                    <Rule symbol-ref="tau_mu">
                        <Expression>rand_norm(1/mu,1/mu/2)</Expression>
                    </Rule>
                    <Rule symbol-ref="tau_epsilon">
                        <Expression>if((epsilon*cSS)>0,rand_norm(1/(epsilon*cSS),1/(epsilon*cSS)/4),100000000000)</Expression>
                    </Rule>
                    <Rule symbol-ref="t_birth">
                        <Expression>time</Expression>
                    </Rule>
                </Triggers>
            </ChangeCellType>
            <Property symbol="t_start" value="0.0"/>
        </CellType>
        <CellType name="exposed" class="biological">
            <ChangeCellType time-step="dt" newCellType="dead" name="Natural death of exposed cells">
                <Condition>time > (t_birth+tau_mu)</Condition>
                <Triggers>
                    <Rule symbol-ref="t_start">
                        <Expression>time</Expression>
                    </Rule>
                </Triggers>
            </ChangeCellType>
            <ChangeCellType time-step="dt" newCellType="infectious" name="Exposed cells become infectious">
                <Condition>time > (t_start+tau_sigma)</Condition>
                <Triggers>
                    <Rule symbol-ref="t_start">
                        <Expression>time</Expression>
                    </Rule>
                    <Rule symbol-ref="tau_mu">
                        <Expression>rand_norm(1/mu,1/mu/2)</Expression>
                    </Rule>
                    <Rule symbol-ref="tau_alpha">
                        <Expression>rand_norm(1/alpha,1/alpha/4)</Expression>
                    </Rule>
                </Triggers>
            </ChangeCellType>
            <Property symbol="tau_sigma" value="rand_norm(1/sigma,1/sigma/4)"/>
            <Property symbol="tau_mu" value="rand_norm(1/mu,1/mu/2)"/>
            <Property symbol="t_start" value="0.0"/>
            <Property symbol="t_birth" value="0.0"/>
        </CellType>
        <CellType name="susceptible" class="biological">
            <Property symbol="cSI" name="cSI" value="0.0"/>
            <Property symbol="cSS" name="cSS" value="0.0"/>
            <Variable symbol="cSE" name="cSE" value="0.0"/>
            <NeighborhoodReporter time-step="dt" name="cSI">
                <Input noflux-cell-medium="false" scaling="cell" value="cell.type == celltype.infectious.id"/>
                <Output symbol-ref="cSI" mapping="average"/>
            </NeighborhoodReporter>
            <NeighborhoodReporter time-step="dt" name="cSE">
                <Input noflux-cell-medium="false" scaling="cell" value="cell.type == celltype.exposed.id"/>
                <Output symbol-ref="cSE" mapping="average"/>
            </NeighborhoodReporter>
            <NeighborhoodReporter time-step="dt" name="cSS">
                <Input noflux-cell-medium="false" scaling="cell" value="cell.type == celltype.susceptible.id"/>
                <Output symbol-ref="cSS" mapping="average"/>
            </NeighborhoodReporter>
            <ChangeCellType time-step="dt" newCellType="dead" name="Natural death of susceptible cells">
                <Condition>time > (t_birth+tau_mu)</Condition>
                <Triggers>
                    <Rule symbol-ref="t_start">
                        <Expression>time</Expression>
                    </Rule>
                </Triggers>
            </ChangeCellType>
            <ChangeCellType time-step="dt" newCellType="antiviral" name="Susceptible cells acquire induced immunity">
                <Condition>rand_uni(0,1) &lt; (rho*cSE*dt)</Condition>
                <Triggers>
                    <Rule symbol-ref="t_start">
                        <Expression>time</Expression>
                    </Rule>
                    <Rule symbol-ref="tau_mu">
                        <Expression>rand_norm(1/mu,1/mu/2)</Expression>
                    </Rule>
                </Triggers>
            </ChangeCellType>
            <ChangeCellType time-step="dt" newCellType="antiviral" name="Susceptible cells acquire constitutive immunity">
                <Condition>time > (t_start+tau_epsilon)</Condition>
                <Triggers>
                    <Rule symbol-ref="t_start">
                        <Expression>time</Expression>
                    </Rule>
                    <Rule symbol-ref="tau_mu">
                        <Expression>rand_norm(1/mu,1/mu/2)</Expression>
                    </Rule>
                </Triggers>
            </ChangeCellType>
            <ChangeCellType time-step="dt" newCellType="exposed" name="Susceptible cells gets exposed to the pathogen">
                <Condition>rand_uni(0,1) &lt; (beta*cSI*dt)</Condition>
                <Triggers>
                    <Rule symbol-ref="t_start">
                        <Expression>time</Expression>
                    </Rule>
                    <Rule symbol-ref="tau_mu">
                        <Expression>rand_norm(1/mu,1/mu/2)</Expression>
                    </Rule>
                    <Rule symbol-ref="tau_sigma">
                        <Expression>rand_norm(1/sigma,1/sigma/4)</Expression>
                    </Rule>
                </Triggers>
            </ChangeCellType>
            <Property symbol="tau_mu" value="rand_norm(1/mu,1/mu/2)"/>
            <Property symbol="tau_epsilon" value="if((epsilon*cSS)>0,rand_norm(1/(epsilon*cSS),1/(epsilon*cSS*4)),100000000000)"/>
            <Property symbol="t_start" value="0.0"/>
            <Property symbol="t_birth" value="0.0"/>
        </CellType>
        <CellType name="antiviral" class="biological">
            <ChangeCellType time-step="dt" newCellType="dead" name="Natural death of antiviral cells">
                <Condition>time > (t_birth+tau_mu)</Condition>
                <Triggers>
                    <Rule symbol-ref="t_start">
                        <Expression>time</Expression>
                    </Rule>
                </Triggers>
            </ChangeCellType>
            <ChangeCellType time-step="dt" newCellType="susceptible" name="Antiviral cells become susceptible">
                <Condition>rand_uni(0,1) &lt; c*dt</Condition>
                <Triggers>
                    <Rule symbol-ref="t_start">
                        <Expression>time</Expression>
                    </Rule>
                    <Rule symbol-ref="tau_mu">
                        <Expression>rand_norm(1/mu,1/mu/2)</Expression>
                    </Rule>
                    <Rule symbol-ref="tau_epsilon">
                        <Expression>if(epsilon>0,rand_norm(1/epsilon,1/epsilon/4),100000000000)</Expression>
                    </Rule>
                </Triggers>
            </ChangeCellType>
            <Property symbol="tau_mu" value="rand_norm(1/mu,1/mu/2)"/>
            <Property symbol="t_start" value="0.0"/>
            <Property symbol="t_birth" value="0.0"/>
        </CellType>
        <CellType name="medium" class="medium"/>
    </CellTypes>
    <CellPopulations>
        <Population type="infectious" name="infectious" size="0">
            <InitCircle mode="random" number-of-cells="0">
                <Dimensions center="75.0, 75.0, 0.0" radius="52.0"/>
            </InitCircle>
        </Population>
        <Population type="exposed" name="exposed" size="10">
            <InitCircle mode="random" number-of-cells="2">
                <Dimensions center="40.0, 40.0, 0.0" radius="3.0"/>
            </InitCircle>
            <InitCircle mode="random" number-of-cells="2">
                <Dimensions center="40.0, 80.0, 0.0" radius="3.0"/>
            </InitCircle>
            <InitCircle mode="random" number-of-cells="2">
                <Dimensions center="80.0, 40.0, 0.0" radius="3.0"/>
            </InitCircle>
            <InitCircle mode="random" number-of-cells="2">
                <Dimensions center="80.0, 80.0, 0.0" radius="3.0"/>
            </InitCircle>
            <InitCircle mode="random" number-of-cells="2">
                <Dimensions center="60.0, 60.0, 0.0" radius="3.0"/>
            </InitCircle>
        </Population>
        <!--    <Disabled>
        <Population type="exposed" name="exposed" size="0">
            <InitCircle mode="random" number-of-cells="(MOI==0.001)*10">
                <Dimensions center="55.0, 55.0, 0.0" radius="52.0"/>
            </InitCircle>
        </Population>
    </Disabled>
-->
        <Population type="dead" name="dead" size="0">
            <InitCircle mode="random" number-of-cells="(MOI==0.001)*953">
                <Dimensions center="55.0, 55.0, 0.0" radius="52.0"/>
            </InitCircle>
        </Population>
        <Population type="susceptible" name="susceptible" size="0">
            <InitCircle mode="random" number-of-cells="(MOI==0.001)*(9037+(select.celltype==3)*(select.virus==1)*(select.immunity==3)*(-4786)+(select.celltype==3)*(select.virus==2)*(select.immunity==3)*(-5351))">
                <Dimensions center="55.0, 55.0, 0.0" radius="52.0"/>
            </InitCircle>
        </Population>
        <Population type="antiviral" name="antiviral" size="0">
            <InitCircle mode="random" number-of-cells="(MOI==0.001)*(0+(select.celltype==3)*(select.virus==1)*(select.immunity==3)*4786+(select.celltype==3)*(select.virus==2)*(select.immunity==3)*5351)">
                <Dimensions center="55.0, 55.0, 0.0" radius="52.0"/>
            </InitCircle>
        </Population>
        <Population type="susceptible" name="susceptible" size="0">
            <InitCircle name="filling up space with susceptible cells for above conditions" mode="random" number-of-cells="(MOI==0.001)*((select.celltype!=3)*(select.virus!=2)*(select.immunity!=3)*100000)">
                <Dimensions center="55.0, 55.0, 0.0" radius="52.0"/>
            </InitCircle>
        </Population>
        <Population type="antiviral" name="antiviral" size="0">
            <InitCircle name="filling up space with antiviral cells for above conditions" mode="random" number-of-cells="(MOI==0.001)*((select.celltype==3)*(select.virus==2)*(select.immunity==3)*100000)">
                <Dimensions center="55.0, 55.0, 0.0" radius="52.0"/>
            </InitCircle>
        </Population>
    </CellPopulations>
    <Global>
        <Constant symbol="select.celltype" name="1=Vero, 2=RoNi7.1, 3=PaKiT01" value="3"/>
        <Constant symbol="select.virus" name="1=rVSV-G, 2=rVSV-EBOV, 3=rVSV-MARV" value="2"/>
        <Constant symbol="select.immunity" name="1=absent, 2=induced, 3=constitutive" value="3"/>
        <Constant symbol="MOI" name="Initial infectious dose" value="0.001"/>
        <Constant symbol="epsilon" value="(select.celltype==3)*(select.virus==1)*(select.immunity==3)*0.006017893 + (select.celltype==3)*(select.virus==2)*(select.immunity==3)*0.04775666"/>
        <Constant symbol="rho" value="(select.celltype==2)*(select.virus==1)*(select.immunity==2)*0.088973822 + (select.celltype==2)*(select.virus==2)*(select.immunity==2)*0.036294543 + (select.celltype==2)*(select.virus==3)*(select.immunity==2)*0.017664985 + (select.celltype==3)*(select.virus==1)*(select.immunity==3)*8.25891E-08 + (select.celltype==3)*(select.virus==2)*(select.immunity==3)*4.46435E-08 + (select.celltype==3)*(select.virus==3)*(select.immunity==2)*13.14416159"/>
        <Constant symbol="mu" value="(select.celltype==1)*(1/121) + (select.celltype==2)*(1/191) + (select.celltype==3)*(1/84)">
            <Annotation>To reproduce the originally published Fig. 5 Suppl. 3, use mu=
(select.celltype==1)*(1/83) + (select.celltype==2)*(1/191) + (select.celltype==3)*(1/120)
as in the originally published simulation code.</Annotation>
        </Constant>
        <Constant symbol="mu_MF" value="(select.celltype==1)*(1/121) + (select.celltype==2)*(1/191) + (select.celltype==3)*(1/84)"/>
        <Constant symbol="alpha" value="1/6"/>
        <Constant symbol="beta_MF" value="(MOI==0.001)*((select.celltype==1)*(select.virus==1)*(select.immunity==1)*2.441842792 + (select.celltype==1)*(select.virus==2)*(select.immunity==1)*1.499584278 + (select.celltype==1)*(select.virus==3)*(select.immunity==1)*0.975158825 + (select.celltype==2)*(select.virus==1)*(select.immunity==2)*2.470464175 + (select.celltype==2)*(select.virus==2)*(select.immunity==2)*0.684901882 + (select.celltype==2)*(select.virus==3)*(select.immunity==2)*1.233417847 + (select.celltype==3)*(select.virus==1)*(select.immunity==3)*3.451642437 + (select.celltype==3)*(select.virus==2)*(select.immunity==3)*34.48214995 + (select.celltype==3)*(select.virus==3)*(select.immunity==2)*3.24521928)"/>
        <Constant symbol="beta" value="beta_MF*10"/>
        <Constant symbol="b_MF" value="0.025"/>
        <Constant symbol="b" value="b_MF*6"/>
        <Constant symbol="c" value="0"/>
        <Constant symbol="sigma" value="(select.celltype==1)*(select.virus==1)*(select.immunity==1)*0.119284294 + (select.celltype==1)*(select.virus==2)*(select.immunity==1)*0.141509434 + (select.celltype==1)*(select.virus==3)*(select.immunity==1)*0.104347826 + (select.celltype==2)*(select.virus==1)*(select.immunity==2)*0.125 + (select.celltype==2)*(select.virus==2)*(select.immunity==2)*0.148883375 + (select.celltype==2)*(select.virus==3)*(select.immunity==2)*0.147420147 + (select.celltype==3)*(select.virus==1)*(select.immunity==3)*0.139534884 + (select.celltype==3)*(select.virus==2)*(select.immunity==3)*0.165745856 + (select.celltype==3)*(select.virus==3)*(select.immunity==2)*0.155844156"/>
        <Constant symbol="N_total" name="total number of cells required" value="10000"/>
        <Function symbol="N" name="total number of cells">
            <Expression>celltype.antiviral.size+celltype.dead.size+celltype.exposed.size+celltype.infectious.size+celltype.susceptible.size</Expression>
        </Function>
        <Function symbol="N_SAE" name="total number of live, uninfectious cells">
            <Expression>celltype.antiviral.size+celltype.exposed.size+celltype.susceptible.size</Expression>
        </Function>
        <Constant symbol="dt" name="dt" value="1/6"/>
        <System time-step="1/6" solver="Dormand-Prince [adaptive, O(5)]">
            <DiffEqn symbol-ref="P_S">
                <Expression>b_MF*P_D*(P_S+P_A)-beta_MF*P_S*P_I-mu_MF*P_S-rho*P_E*P_S-epsilon*P_S+c*P_A</Expression>
            </DiffEqn>
            <DiffEqn symbol-ref="P_A">
                <Expression>rho*P_E*P_S+epsilon*P_S-c*P_A-mu_MF*P_A</Expression>
            </DiffEqn>
            <DiffEqn symbol-ref="P_E">
                <Expression>beta_MF*P_S*P_I-sigma*P_E-mu_MF*P_E</Expression>
            </DiffEqn>
            <DiffEqn symbol-ref="P_I">
                <Expression>sigma*P_E-alpha*P_I-mu_MF*P_I</Expression>
            </DiffEqn>
            <DiffEqn symbol-ref="P_D">
                <Expression>mu_MF*(P_S+P_E+P_I+P_A)+alpha*P_I-b_MF*P_D*(P_S+P_A)</Expression>
            </DiffEqn>
        </System>
        <Variable symbol="P_I" name="P_I" value="0"/>
        <Variable symbol="P_E" name="P_E" value="(1-exp(-MOI))*0.9"/>
        <Variable symbol="P_D" name="P_D" value="0.1"/>
        <Variable symbol="P_S_DFE" name="P_S_DFE" value="((b_MF-mu_MF)*(c+mu_MF))/(b_MF*(c+mu_MF+epsilon))"/>
        <Variable symbol="P_A_DFE" name="P_A_DFE" value="epsilon*P_S_DFE/(c+mu_MF)"/>
        <Variable symbol="P_S" name="P_S" value="(epsilon==0)*(1-P_E-P_D)+(epsilon>0)*(P_S_DFE &lt; P_A_DFE)*((0.9-(1-exp(-MOI)))/(1+P_A_DFE/P_S_DFE))+(epsilon>0)*(P_S_DFE >= P_A_DFE)*(1-P_E-P_A_DFE-P_D)"/>
        <Variable symbol="P_A" name="P_A" value="(epsilon==0)*(0)+(epsilon>0)*(P_S_DFE &lt; P_A_DFE)*((0.9-(1-exp(-MOI))) - P_S)+(epsilon>0)*(P_S_DFE >= P_A_DFE)*(P_A_DFE)"/>
        <Function symbol="P_total">
            <Expression>P_S+P_A+P_E+P_I+P_D</Expression>
        </Function>
        <Function symbol="P_SAE">
            <Expression>P_S+P_A+P_E</Expression>
        </Function>
        <Function symbol="R0_MF" name="reproduction number">
            <Expression>(beta_MF*sigma*(b_MF-mu_MF)*(c+mu_MF))/(b_MF*(sigma+mu_MF)*(alpha+mu_MF)*(c+mu_MF+epsilon)) </Expression>
        </Function>
        <Function symbol="R_eff_MF" name="effective reproduction number">
            <Expression>R0_MF * P_S</Expression>
        </Function>
        <Constant symbol="antiviral_rate" value="rho*P_E*P_S-epsilon*P_S"/>
        <Constant symbol="plot_helper" name="plot_helper" value="-1"/>
        <Constant symbol="t_report" value="100"/>
        <!--    <Disabled>
        <Function symbol="test_MF">
            <Expression>(time==t_report)*(
(abs(P_D-0.6990170419)&lt;1e-10)+
(abs(P_S-0.005168814058)&lt;1e-10)+
(abs(P_A-0.2484433693)&lt;1e-10)+
(abs(P_E-0.02411102891)&lt;1e-10)+
(abs(P_I-0.02315979577)&lt;1e-10)
)/5 +
(time==2*t_report)*(
(abs(P_D-0.8842619523)&lt;1e-10)+
(abs(P_S-0.005010625795)&lt;1e-10)+
(abs(P_A-0.08962451566)&lt;1e-10)+
(abs(P_E-0.01062489864)&lt;1e-10)+
(abs(P_I-0.01037805759)&lt;1e-10)
)/5</Expression>
        </Function>
    </Disabled>
-->
        <!--    <Disabled>
        <Function symbol="test_spatial">
            <Expression>(time==t_report)*(
(abs(celltype.dead.size-5983)&lt;300)+
(abs(celltype.susceptible.size-6)&lt;50)+
(abs(celltype.antiviral.size-1937)&lt;200)+
(abs(celltype.exposed.size-954)&lt;100)+
(abs(celltype.infectious.size-924)&lt;100)
)/5 +
(time==2*t_report)*(
(abs(celltype.dead.size-8962)&lt;300)+
(abs(celltype.susceptible.size-754)&lt;200)+
(abs(celltype.antiviral.size-21)&lt;50)+
(abs(celltype.exposed.size-33)&lt;50)+
(abs(celltype.infectious.size-34)&lt;50)
)/5</Expression>
        </Function>
    </Disabled>
-->
    </Global>
    <Analysis>
        <!--    <Disabled>
        <Gnuplotter time-step="1">
            <Plot title="A. Cell space">
                <Cells value="cell.type">
                    <ColorMap>
                        <Color color="red" value="0"/>
                        <Color color="dark-violet" value="1"/>
                        <Color color="orange" value="2"/>
                        <Color color="dark-green" value="3"/>
                        <Color color="skyblue" value="4"/>
                        <Color color="white" value="5"/>
                    </ColorMap>
                </Cells>
            </Plot>
            <Terminal name="png"/>
        </Gnuplotter>
    </Disabled>
-->
        <!--    <Disabled>
        <Logger time-step="10">
            <Input>
                <Symbol symbol-ref="antiviral_rate"/>
                <Symbol symbol-ref="R0_MF"/>
            </Input>
            <Output>
                <TextOutput/>
            </Output>
            <Plots>
                <Plot title="B. Time series">
                    <Style style="points" decorate="false"/>
                    <Terminal terminal="png"/>
                    <X-axis minimum="0" maximum="200">
                        <Symbol symbol-ref="time"/>
                    </X-axis>
                    <Y-axis minimum="0" maximum="10000">
                        <Symbol symbol-ref="celltype.dead.size"/>
                        <Symbol symbol-ref="celltype.susceptible.size"/>
                        <Symbol symbol-ref="celltype.antiviral.size"/>
                        <Symbol symbol-ref="celltype.exposed.size"/>
                        <Symbol symbol-ref="N_SAE"/>
                        <Symbol symbol-ref="plot_helper"/>
                        <Symbol symbol-ref="celltype.infectious.size"/>
                    </Y-axis>
                </Plot>
                <Plot>
                    <Style style="points" decorate="true"/>
                    <Terminal terminal="png"/>
                    <X-axis>
                        <Symbol symbol-ref="time"/>
                    </X-axis>
                    <Y-axis>
                        <Symbol symbol-ref="P_D"/>
                        <Symbol symbol-ref="P_S"/>
                        <Symbol symbol-ref="P_A"/>
                        <Symbol symbol-ref="P_E"/>
                        <Symbol symbol-ref="P_SAE"/>
                        <Symbol symbol-ref="R_eff_MF"/>
                        <Symbol symbol-ref="P_I"/>
                    </Y-axis>
                </Plot>
            </Plots>
        </Logger>
    </Disabled>
-->
        <!--    <Disabled>
        <ModelGraph include-tags="#untagged" format="svg" reduced="false"/>
    </Disabled>
-->
        <!--    <Disabled>
        <Logger time-step="t_report" name="Test">
            <Input>
                <Symbol symbol-ref="test_MF"/>
                <Symbol symbol-ref="test_spatial"/>
            </Input>
            <Output>
                <TextOutput/>
            </Output>
        </Logger>
    </Disabled>
-->
    </Analysis>
</MorpheusModel>
