
IF(NOT BUILD_TESTING)
	return()
ENDIF()

add_library(ModelTesting SHARED model_test.cpp test_operators.cpp)
SET_PROPERTY(TARGET ModelTesting PROPERTY POSITION_INDEPENDENT_CODE TRUE)

target_include_directories(ModelTesting PUBLIC ${CMAKE_CURRENT_SOURCE_DIR} ${CMAKE_CURRENT_BINARY_DIR})
target_link_libraries(ModelTesting PUBLIC MorpheusCore gtest)

FUNCTION(InjectModels target)
	
	SET(RESSOURCE_INCLUDES "")
	SET(RESSOURCE_FILE_DESCRIPTORS "")
	
	get_target_property(sources ${target} SOURCES)
	FOREACH(source ${sources})
		file(STRINGS ${source} entries REGEX "ImportFile")
		IF(CMAKE_VERSION VERSION_LESS 3.20)
			get_filename_component(FILE_REL_PATH "${source}" DIRECTORY)
		ELSE()
			cmake_path(GET source PARENT_PATH FILE_REL_PATH)
		ENDIF()
		IF(FILE_REL_PATH STREQUAL "")
			SET(FILE_REL_PATH ".")
		ENDIF()
# 		file(READ ${source} data HEX)
		FOREACH(entry ${entries})
			IF (entry MATCHES "RessourceData ImportFile")
				### Skip the Implementation of the method ...
# 				message(STATUS "${entry} is the method implementation" )
				continue()
			ENDIF()
			string(REGEX MATCH "^[^/#]*ImportFile\\((.*)\\)" match ${entry} )
			IF(match)
				SET(MODEL_FILE ${CMAKE_MATCH_1})
				## Remove white space
				string(REGEX REPLACE "[ \t\"]" "" TRIMMED_FILE ${MODEL_FILE})
				SET(MODEL_FILE ${TRIMMED_FILE})
				
				# USE THE SOURCE FILE PATH AS THE BASE !!!
				IF(NOT EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/${FILE_REL_PATH}/${MODEL_FILE})
					message(FATAL_ERROR "Ressource file ${MODEL_FILE} does not exit")
				ENDIF()
				string(REGEX REPLACE "[./]" "_" variable ${TRIMMED_FILE} )
				message(STATUS "Injecting file ${FILE_REL_PATH}/${MODEL_FILE} as variable ${variable} " )
				
				SET(MODEL_RES ${variable}.h)
				add_custom_command(OUTPUT ${CMAKE_CURRENT_BINARY_DIR}/${MODEL_RES} 
								   COMMAND xxd ARGS -i ${MODEL_FILE} ${CMAKE_CURRENT_BINARY_DIR}/${MODEL_RES} 
								   MAIN_DEPENDENCY ${FILE_REL_PATH}/${MODEL_FILE} 
								   DEPENDS ${FILE_REL_PATH}/${MODEL_FILE} ${source}
								   WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/${FILE_REL_PATH}
								   COMMENT "Moving ${FILE_REL_PATH}/${MODEL_FILE} to ressources"
								   )
				target_sources(${target} PRIVATE ${CMAKE_CURRENT_BINARY_DIR}/${MODEL_RES})
				LIST(APPEND RESSOURCE_INCLUDES "#include \"${MODEL_RES}\"\n")
				LIST(APPEND RESSOURCE_FILE_DESCRIPTORS "\t{\"${TRIMMED_FILE}\", ${variable},  ${variable}_len},\n")
			endif()
		ENDFOREACH()
# 		ENDIF()
	ENDFOREACH()

	## Convert lists to string
	STRING(CONCAT  RESSOURCE_INCLUDES ${RESSOURCE_INCLUDES} )
	STRING(CONCAT  RESSOURCE_FILE_DESCRIPTORS ${RESSOURCE_FILE_DESCRIPTORS} )
	## Remove trailing comma
	STRING(REGEX REPLACE "[ \n,]+$" "" RESSOURCE_FILE_DESCRIPTORS "${RESSOURCE_FILE_DESCRIPTORS}")
	# Configure the ressource source file
	configure_file(${CMAKE_SOURCE_DIR}/morpheus/testing/ressources.cpp.in ${target}_ressources.cpp @ONLY)
	SET(ressources_file  ${CMAKE_CURRENT_BINARY_DIR}/${target}_ressources.cpp)
	
	# Add ressources to the target
	target_sources(${target} PRIVATE ${ressources_file})
ENDFUNCTION()

include(GoogleTest)
add_subdirectory(components)
add_subdirectory(core)
add_subdirectory(models)
