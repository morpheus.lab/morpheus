#include "neighborhood_reporter.h"


using namespace SIM;

REGISTER_PLUGIN(NeighborhoodReporter);

NeighborhoodReporter::NeighborhoodReporter() : ReporterPlugin(TimeStepListener::XMLSpec::XML_OPTIONAL) {
	scaling.setXMLPath("Input/scaling");
	map<string, InputModes> value_map;
	value_map["cell"] = CELLS;
	value_map["length"] = INTERFACES;
	scaling.setConversionMap(value_map);
	registerPluginParameter(scaling);
	
	input.setXMLPath("Input/value");
	// Input is required to be valid on global scope
	input.setGlobalScope();
	registerPluginParameter(input);
	
	   exclude_medium.setXMLPath("Input/noflux-cell-medium");
	   exclude_medium.setDefault("false");
	registerPluginParameter(exclude_medium);
	
}

void NeighborhoodReporter::loadFromXML(const XMLNode xNode, Scope* scope)
{    
	map<string, DataMapper::Mode> output_mode_map = DataMapper::getModeNames();
	// Define PluginParameters for all defined Output tags
	for (uint i=0; i<xNode.nChildNode("Output"); i++) {
		shared_ptr<OutputSpec> out(new OutputSpec());
		out->mapping.setXMLPath("Output["+to_str(i)+"]/mapping");
		out->mapping.setConversionMap(output_mode_map);
		out->symbol.setXMLPath("Output["+to_str(i)+"]/symbol-ref");
		registerPluginParameter(out->mapping);
		registerPluginParameter(out->symbol);
		output.push_back(out);
	}
	
	// Load all the defined PluginParameters
	ReporterPlugin::loadFromXML(xNode, scope);
}


void NeighborhoodReporter::init(const Scope* scope)
{
	input.addNameSpaceScope("local",scope);

    ReporterPlugin::init(scope);
	
	local_ns_id = input.getNameSpaceId("local");
	
	local_ns_granularity = Granularity::Global;
	using_local_ns = false;
	for (auto & local : input.getNameSpaceUsedSymbols(local_ns_id)) {
		local_ns_granularity += local->granularity();
		using_local_ns = true;
	}
	
    celltype = scope->getCellType();
	if (celltype) {
		
		// Reporter output value depends on cell position
		registerCellPositionDependency();
		
		bool input_is_halo = input.granularity() == Granularity::MembraneNode || input.granularity() == Granularity::Node;
		if (scaling.isDefined() && input_is_halo) {
			cout << "NeighborhoodReporter: Input has (membrane) node granularity. Ignoring defined input mode." << endl;
		}
		
		for ( auto & out : output) {
			switch (out->symbol.granularity()) {
				case Granularity::MembraneNode:
					halo_output.push_back(out);
					out->mapper = DataMapper::create(out->mapping());		// There might also be boolean input, that we cannot easily handle this way. But works for 
					out->membrane_symbol = dynamic_pointer_cast<const MembranePropertySymbol>(out->symbol.accessor());
					if (out->membrane_symbol) {
						if ( !out->membrane_mapper ) {
							switch (out->mapping()) {
								case DataMapper::MAXIMUM: 
									out->membrane_mapper = make_unique<MembraneMapper> (MembraneMapper::MAP_CONTINUOUS, MembraneMapper::InterpolationMode::MAXIMUM);
									break;
								case DataMapper::MINIMUM: 
									out->membrane_mapper = make_unique<MembraneMapper> (MembraneMapper::MAP_CONTINUOUS, MembraneMapper::InterpolationMode::MINIMUM);
									break;
								case DataMapper::DISCRETE: 
									out->membrane_mapper = make_unique<MembraneMapper> (MembraneMapper::MAP_DISCRETE, MembraneMapper::InterpolationMode::MAJORITY);
									break;
								case DataMapper::AVERAGE: 
									out->membrane_mapper = make_unique<MembraneMapper> (MembraneMapper::MAP_CONTINUOUS, MembraneMapper::InterpolationMode::AVERAGE);
									break;
								default:
									break;
							}
						}
					}
					break;
				case Granularity::Global :
				case Granularity::Cell :
					if ( ! out->mapping.isDefined())
						throw MorpheusException(string("NeighborhoodReporter requires a mapping for reporting into Symbol ") + out->symbol.name(), stored_node);
					out->mapper = DataMapper::create(out->mapping());
					out->mapper->setWeightsAreBuckets(true);
					
					if (input_is_halo) {
						halo_output.push_back(out);
					}
					else {
						interf_output.push_back(out);
					}
					break;
					
				case Granularity::Node: 
				default:
					throw XMLName() + " can not write to symbol " + out->symbol.name() + " with node granularity.";
					break;
			}
		}
		
		if (!halo_output.empty()) {
			CPM::enableEgdeTracking();
		}
		
		if (exclude_medium.isDefined() ){
			cout << "noflux_cell_medium_interface.isDefined()" << endl;
			if (exclude_medium() == true){
				cout << "noflux_cell_medium_interface() == true!" << endl;
			}
		}
	}
	else {
		// global scope case
		if (!CPM::getCellTypes().empty()) registerCellPositionDependency();
		for ( auto & out : output) {
			switch (out->symbol.granularity()) {
				case Granularity::Node:
					out->mapper = DataMapper::create(out->mapping());
					break;
				default:
					throw MorpheusException( XMLName() + " can not write to symbol " + out->symbol.name() + " without node granularity.", stored_node);
					break;
			}
		}
	}
}


void NeighborhoodReporter::report() {
	if (celltype) {
		reportCelltype(celltype);
	}
	else {
		reportGlobal();
	}
}

void NeighborhoodReporter::reportGlobal() {
	FocusRange range(Granularity::Node, SIM::getGlobalScope());
	const auto& def_neighborhood = SIM::lattice().getDefaultNeighborhood();
#pragma omp parallel
	{
		int thread = omp_get_thread_num();
#pragma omp for schedule(static)
		for (auto i_node = range.begin(); i_node<range.end(); ++i_node) {
	// 	for (auto node : range) { // syntax cannot be used with openMP
			const auto& node = *i_node;
			
			// Expose local symbols to input
			if (using_local_ns) {
				input.setNameSpaceFocus(local_ns_id, node);
			}
			
			// loop through its neighbors
			const auto& neighbors = def_neighborhood.neighbors(node.pos());
			for ( int i_nei = 0; i_nei < neighbors.size(); ++i_nei ) {
				VINT nb_pos = node.pos() + neighbors[i_nei];
				// get value at neighbor node
				double value = input(nb_pos);
				// add value to data mapper
				for (auto const&  out: output){
					out->mapper->addVal( value , thread);
				}
			}
			// write mapped values to output symbol at node
			for ( auto const&  out: output ) {
				out->symbol.accessor()->setBuffer(node.pos(), out->mapper->get(thread));
				out->mapper->reset(thread);
			}
		}
	}
	for ( auto const&  out: output ) {
		out->symbol.accessor()->applyBuffer();
	}
}

void NeighborhoodReporter::reportCelltype(CellType* celltype) {
    vector<CPM::CELL_ID> cells = celltype->getCellIDs();
	if (cells.empty()) return;

	// if the input has membrane node granularity, we need to create and iterate over the cell halo
	// the same is true, if one of the output symbols requires node granularity
	if ( !halo_output.empty()) {
		//  draw in the membrane mapper ...
		const auto& neighborhood = CPM::getSurfaceNeighborhood();
		ExceptionCatcher exception_catcher;
#pragma omp parallel
		{
		auto thread = omp_get_thread_num();
		for ( const auto& out : halo_output) {
			out->mapper->reset(thread);
		}
#pragma omp for schedule(static)
		for ( int i=0; i<cells.size(); i++) {
			exception_catcher.Run([&](){
				// Create halo of nodes surrounding the cell
				SymbolFocus cell_focus(cells[i]);
				if ( using_local_ns && local_ns_granularity != Granularity::MembraneNode) {
					// Expose local symbols to input
					input.setNameSpaceFocus(local_ns_id, cell_focus);
				}
				
				// NodeStorage_deque halo_nodes;
				Cell::Nodes halo_nodes; // holds nodes of neighbors of membrane nodes. Used for <concentration>
				const bool ignore_duplicates=true;
				const Cell::Nodes& surface_nodes = cell_focus.cell().getSurface();
				for ( Cell::Nodes::const_iterator m = surface_nodes.begin(); m != surface_nodes.end(); ++m ) {
					// check the current surface neighborhood
					const auto& neighbors = neighborhood.neighbors(*m);
					for ( int i_nei = 0; i_nei < neighbors.size(); ++i_nei ) {
						VINT neighbor_position = ( *m ) + neighbors[i_nei];
						auto nb_spin = CPM::getNode( neighbor_position );

						if ( cell_focus.cellID() != nb_spin ) { // if neighbor is different from me
							// HACK: NOFLUX BOUNDARY CONDITIONS when halo is in MEDIUM
							//cout << CPM::getCellIndex( nb_spin.cell_id ).celltype << " != " << CPM::getEmptyCelltypeID() << endl;
							if ( exclude_medium() && SymbolFocus(nb_spin).celltype() == CPM::getEmptyCelltypeID() ) {
								// if neighbor is medium, add own node in halo 
								// if (CPM::getCellIndex( nb_spin ).celltype == CPM::getEmptyCelltypeID() )
									halo_nodes.insert ( *m, ignore_duplicates );
							}
							else
								halo_nodes.insert ( neighbor_position, ignore_duplicates );
						}
					}
				}
				if (halo_nodes.empty() ) {
					cout << "NeighborhoodReporter refuses to report on cell " << cell_focus.cellID() << " because it has no surface" << endl;
					cout << "Cell "<< cell_focus.cellID() << " Cell size was " << cell_focus.cell().nNodes() << endl;
					return;
				}
				
				// Raw data retrieval
				vector<double> raw_data;
				raw_data.resize(halo_nodes.size());
				std::size_t i=0;
				if ( using_local_ns && local_ns_granularity == Granularity::MembraneNode) {
					for ( auto const & node :halo_nodes) {
						// Expose local symbols to input
						cell_focus.setCell(cell_focus.cellID(),node);
						input.setNameSpaceFocus(local_ns_id, cell_focus);
					raw_data[i++] = input(SymbolFocus(node));
					}
				}
				else {
					for ( auto const & node :halo_nodes) {
						raw_data[i++] = input(SymbolFocus(node));
					}
				}
				
				if (scaling() == INTERFACES) {
					for (const auto & out : halo_output) {
						if (out->membrane_symbol) {
							out->membrane_mapper->attachToCell(cell_focus.cellID(),thread);
							std::size_t i =0;
							for ( auto const & node :halo_nodes) {
								out->membrane_mapper->map(node, std::isnan(raw_data[i]) ? 0.0 : raw_data[i],thread);
								i++;
							}
							out->membrane_mapper->fillGaps(thread);
							out->membrane_mapper->copyData(out->membrane_symbol->getField(cell_focus.cellID()),thread);
						}
						else {
							for (const auto& val : raw_data) {
								if (!std::isnan(val))
									out->mapper->addVal(val, thread);
							}
							// Apply if symbol has node granularity
							if (out->symbol.granularity() == Granularity::Cell) {
								if (out->mapper->getMode() == DataMapper::SUM) {
									// Rescale to Interface length
									out->symbol.set(cell_focus, out->mapper->get(thread) / raw_data.size() *  cell_focus.cell().getInterfaceLength() );
								}
								else {
									out->symbol.set(cell_focus, out->mapper->get(thread));
								}
								out->mapper->reset(thread);
							}
						}
					}
				}
				else if (scaling() == CELLS) {
					for ( const auto & out : halo_output) {
						// TODO this piece of code always computes averages. one should rather use one of the data mappers 
						struct count_data { CPM::CELL_ID id; shared_ptr<DataMapper> value; };
						vector<count_data> cell_mapper;
						std::size_t i=0;
						for ( const auto & node :halo_nodes) {
							SymbolFocus f(node);
							double value = raw_data[i];
							std::size_t k=0;
							while (true) {
								if (std::isnan(value))
									break;
								if (cell_mapper.size() == k ) {
									cell_mapper.push_back({f.cellID(), DataMapper::create(out->mapping())}); 
								}
								if (cell_mapper[k].id == f.cellID()) {
									cell_mapper[k].value->addVal(raw_data[i]);
									break;
								}
								k++;
							}
						}
					
						if (out->membrane_symbol) {
							out->membrane_mapper->attachToCell(cell_focus.cellID(), thread);
							for ( const auto & i :halo_nodes) {
								SymbolFocus focus(i);
								double value=0;
								for ( const auto& val : cell_mapper ) {
									if (val.id == focus.cellID()) {
										value=val.value->get();
										break;
									}
								}
								out->membrane_mapper->map(i,value, thread);
							}
							out->membrane_mapper->fillGaps(thread);
							out->membrane_mapper->copyData(out->membrane_symbol->getField(cell_focus.cellID()), thread);
						}
						else {
							for ( const auto & cell_stat : cell_mapper ) {
								out->mapper->addVal(cell_stat.value->get(), thread);
							}
							if (out->symbol.granularity() == Granularity::Cell) {
								out->symbol.set(cell_focus, out->mapper->get(thread));
								out->mapper->reset(thread);
							}
						}
					}
				}
			
			});
		}}
		exception_catcher.Rethrow();
		// Post hoc writing of global output 
		for (auto const& out : halo_output) {
			if (out->symbol.granularity() == Granularity::Global) {
				out->symbol.set(SymbolFocus::global, out->mapper->getCollapsed());
			}
		}
	}
	
	if ( !interf_output.empty()) {
		// Assume cell granularity
		ExceptionCatcher exception_catcher;
#pragma omp parallel
		{
			int thread = omp_get_thread_num();
			for (auto const& out : interf_output) {
				out->mapper->reset(thread);
			}
#pragma omp for schedule(static)
			for (int c=0; c<cells.size(); c++) {
				exception_catcher.Run([&](){
				SymbolFocus cell_focus(cells[c]);
				
				// Expose local symbols to input
				if (using_local_ns) {
					// Expose local symbols to input
					input.setNameSpaceFocus(local_ns_id, cell_focus);
				}
				
				const auto& interfaces = CPM::getCell(cells[c]).getInterfaceLengths();
				
				// Special case cell has no interfaces ...
				if (interfaces.size() == 0) {
					for (auto const& out : interf_output) {
						out->symbol.set(cell_focus, 0.0);
					}
					return;
				}
				
				uint i=0;

				for (auto nb = interfaces.begin(); nb != interfaces.end(); nb++, i++) {
					CPM::CELL_ID cell_id = nb->first;
					if ( CellType::storage.cell(cell_id)==nullptr ) {
						cout << "Cell tracks dead cell! " << nb->first << " -> " << nb->second << endl;
						continue;
					}
					SymbolFocus cell_focus(cell_id);
					
					double value = 0.0;
					if ( exclude_medium() && cell_focus.cell().getCellType()->isMedium() ) { 
						// if neighbor is medium, skip value
						continue;
					}

					value = input.get(cell_focus); // value of neighboring cell
					double interfacelength = (scaling() == INTERFACES) ? nb->second : 1;
					
					for (auto & out : interf_output) {
						out->mapper->addValW(value, interfacelength, thread);
					}
				}

				for (auto& out : interf_output) {
					if (out->symbol.granularity() == Granularity::Cell) {
						out->symbol.set(cell_focus, out->mapper->get(thread));
						out->mapper->reset(thread);
					}
				}
			});
			}
		}
		exception_catcher.Rethrow();
		// Post hoc writing of global output 
		for (auto const& out : interf_output) {
			if (out->symbol.granularity() == Granularity::Global) {
				out->symbol.set(SymbolFocus::global, out->mapper->getCollapsed());
			}
		}
	}
}




