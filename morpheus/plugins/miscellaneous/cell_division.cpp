#include "cell_division.h"

REGISTER_PLUGIN(CellDivision);
map<string, shared_ptr<ofstream> >  CellDivision::log_files;

// TODO: is "InstantaneousProcessPlugin" correct as a MCSStepper?
CellDivision::CellDivision() : InstantaneousProcessPlugin( TimeStepListener::XMLSpec::XML_NONE ), logging("CellDivision") {
	
	condition.setXMLPath("Condition/text");
	registerPluginParameter(condition);

	division_plane.setXMLPath("division-plane");
	map<string,CellType::division> conversion_map;
	conversion_map[string("random")] 	= CellType::RANDOM;
	conversion_map[string("minor")] 	= CellType::MINOR;
	conversion_map[string("major")] 	= CellType::MAJOR;
	// TODO: it would be useful to determine specific division plane
	conversion_map[string("oriented")] 	= CellType::ORIENTED; 
	division_plane.setConversionMap( conversion_map );
	registerPluginParameter(division_plane);

	orientation.setXMLPath("orientation");
	registerPluginParameter(orientation);
	
	write_log.setXMLPath("write-log");
	write_log.setDefault("none");
	write_log.setConversionMap({
		{"none", CellDivision::NONE},
		{"dot", CellDivision::DOT},
		{"celltype-dot", CellDivision::CT_DOT},
		{"csv", CellDivision::CSV},
		{"celltype-csv", CellDivision::CT_CSV},
		{"newick", CellDivision::NEWICK}
	});
	registerPluginParameter(write_log);
	
	trigger_on_change.setDefault("on-change");
	trigger_on_change.setXMLPath("trigger");
	trigger_on_change.setConversionMap({{"when-true",false},{"on-change",true}});
	registerPluginParameter(trigger_on_change);
}

void CellDivision::loadFromXML(const XMLNode xNode, Scope* scope)
{
	InstantaneousProcessPlugin::loadFromXML( xNode, scope );
	
	if ( xNode.nChildNode("Triggers") ) {
		XMLNode xTrigger = xNode.getChildNode("Triggers");
		daughterID_symbol = "__daughterID";
		getXMLAttribute(xNode,"daughterID", daughterID_symbol);
		trigger_system = shared_ptr<TriggeredSystem>(new TriggeredSystem);
		trigger_system->loadFromXML(xTrigger, scope);
		trigger_system->addLocalSymbol(daughterID_symbol,0);
	}
	logging.loadFromXML(xNode, scope);
}

void CellDivision::init(const Scope* scope){
	InstantaneousProcessPlugin::init( scope );
	setTimeStep( CPM::getMCSDuration() );
	is_adjustable = false;
	celltype = scope->getCellType();
	
	registerCellPositionOutput();
	
	if(trigger_on_change()) {
		try {
			auto history = scope->findRWSymbol<double>("division_condition_history");
			condition_history = dynamic_pointer_cast< const PrimitivePropertySymbol<double> >(history);
		}
		catch (...) {
			condition_history = celltype->addProperty<double>("division_condition_history", 0.0);
		}
	}
	
	if (trigger_system) {
		trigger_system->init();
		// the time-step does not depend on the trigger input symbols registerInputSymbols(trigger_system->getDependSymbols());
		registerOutputSymbols(trigger_system->getOutputSymbols());
		daughterID = trigger_system->getLocalScope()->findRWSymbol<double>(daughterID_symbol);
	}
	
	if (write_log.isDefined()){
		log_file_name = "cell_division";
		switch ( write_log() ) {
			case CellDivision::CT_CSV :
				log_file_name += string("_") + celltype->getName();
			case CellDivision::CSV : 
				log_file_name += ".txt";
				break;
			case CellDivision::CT_DOT :
				log_file_name += string("_") + celltype->getName();
			case CellDivision::DOT :
				log_file_name += ".dot";
				break;
			default :
				break;
		}
		
	}
	
	if( division_plane() == CellType::ORIENTED && !orientation.isDefined()  ){
		throw MorpheusException("CellDivision: Orientation of cell division plane must be specified.", stored_node);
	}
	
	logging.setEventSymbols({"condition", "daughterId_1", "daughterId_2"});
	logging.init(scope, Granularity::Cell);
}

void CellDivision::executeTimeStep() {
	
	vector <CPM::CELL_ID> cells = celltype->getCellIDs();
	for (int i=0; i < cells.size(); i++ ) {
		
		SymbolFocus mother(cells[i]);

		if( mother.cell().nNodes() < 2.0 )
			continue;
		
		bool divide = false;
		
		double cond = condition( mother );
		if (cond >= 1.0) {
			if (trigger_on_change() ) {
				if ( condition_history->get(mother) < 1.0)  {
					divide = true;
				}
				condition_history->set(mother, cond);  // daughter cells will inherit this property value
			}
			else {
				divide = true;
			}
		}
		else {
			if (trigger_on_change()) 
				condition_history->set(mother, cond);
		}
		
		if( divide ){ // if condition for proliferation is fulfilled
			
			// const Cell& mother = CPM::getCell(cells[i]);
			//CPM::CELL_ID daughter_id = celltype->divideCell( mother_id, divisionPlane ); //CPM::getCellIndex(mother_id).cell );
			
			pair<CPM::CELL_ID,CPM::CELL_ID> daughter_ids { celltype->createCell(), celltype->createCell() };
			logging.trigger(mother, { cond, double(daughter_ids.first), double(daughter_ids.second) });
			
			if ( division_plane() == CellType::ORIENTED  ){
				daughter_ids = celltype->divideCell2( mother.cellID(), division_plane(), orientation( mother ), daughter_ids );
			}
			else
				daughter_ids = celltype->divideCell2( mother.cellID(), division_plane(), VINT(0,0,0), daughter_ids);
				
			SymbolFocus daughter1(daughter_ids.first);
			SymbolFocus daughter2(daughter_ids.second);
			

			// Prevent downstream triggers if something went wrong
			bool found_empty = false;
			if( daughter1.cell().nNodes() == 0){
				CPM::setCellType( daughter1.cellID(), CPM::getEmptyCelltypeID() );
				cerr << "CellDivision error: Daughter1 cell is empty! Daughter2 has " << daughter2.cell().nNodes() << " nodes." <<  endl ;
				found_empty = true;
			}
			if( daughter2.cell().nNodes() == 0){
				CPM::setCellType( daughter2.cellID(), CPM::getEmptyCelltypeID() );
				cerr << "CellDivision error: Daughter2 cell is empty! Daughter1 has " << daughter1.cell().nNodes() << " nodes." << endl;
				found_empty = true;
			}
			if (!found_empty) {
				cell_division_log(mother.cellID(), daughter1.cellID(), daughter2.cellID());
				
				if (trigger_system) {
					daughterID->set(SymbolFocus::global,1);
					trigger_system->trigger(daughter1);

					daughterID->set(SymbolFocus::global,2);
					trigger_system->trigger(daughter2);
				}
			}
		}
	}
}

void CellDivision::cell_division_log(CPM::CELL_ID mother, CPM::CELL_ID daughter1, CPM::CELL_ID daughter2) {
	switch ( write_log() ) {
		case CellDivision::CSV :
		case CellDivision::CT_CSV : {
			// if logfile was not created yet, create it with a custom deleter, write the header and put it in the static log_files map
			auto log_file = log_files.find(log_file_name);
			if ( log_file == log_files.end() ) {
				auto file = shared_ptr<ofstream>(new ofstream(log_file_name,ios::out), [=] (ofstream* f) {
					f->close();
					delete f;
				});
				cout << "CellDivision: Log cell IDs to file: " << log_file_name << "\n";
				*file << "Time\tMotherID\tDaughter1ID\tDaughter2ID" << endl;
				log_file = log_files.insert({log_file_name, file}).first;
			}
			*log_file->second << SIM::getTime() << "\t" << mother << "\t" << daughter1 << "\t" << daughter2 << endl;
			break;
		}
		case CellDivision::DOT :
		case CellDivision::CT_DOT : {
			auto log_file = log_files.find(log_file_name);
			// if logfile was not created yet, create it with a custom deleter, write the header and put it in the static log_files map
			if ( log_file == log_files.end() ) {
				auto file = shared_ptr<ofstream>(new ofstream(log_file_name,ios::out), [=] (ofstream* f) {
					*f << "\n}\n";
					f->close();
					delete f;
				});
				cout << "CellDivision: Log cell IDs to file: cell_division.dot\n";
				*file << "digraph{" << endl;
				log_file = log_files.insert( {log_file_name, file} ).first;
			}
			*log_file->second  << mother << " -> " << daughter1 << "\n"; 
			*log_file->second  << mother << " -> " << daughter2 << endl;
			break;
		}
		case CellDivision::NEWICK : 
		{
			// Generate NEWICK tree
			string mstr = "\""+to_string(mother)+"\"";
			bool mother_found=false;
			for(int i=0; i < newicks.size(); i++){
				std::size_t found = newicks[i].find(mstr);
				if (found!=std::string::npos){ // if found, replace mother with mother and daughter sub-tree
					mother_found = true;
					string mstr2 = string("(\"") + to_string(daughter1) + "\",\"" + to_string(daughter2)+"\")";
					newicks[i].insert(found,mstr2);
				}
			}
			if( !mother_found ){ // add a new newick tree and file
				string mstr2 = string("(\"")+to_string(daughter1)+"\",\""+to_string(daughter2)+"\")"+mstr;

				newicks.push_back( mstr2 );
			}
			int num=0;
			for(string newick : newicks){
				ofstream fout_newick;
				fout_newick.open("cell_division_newick_"+celltype->getName()+"_"+to_string(num++)+".txt",ios::out);
				fout_newick << newick << ";" << endl;
				fout_newick.close();
			}
		}
		break;
		default:
			break;
	}
}

CellDivision::~CellDivision()
{
	if( write_log() == CellDivision::NEWICK){
		int num=0;
		for(string newick : newicks){
			ofstream fout_newick;
			fout_newick.open("cell_division_newick_"+celltype->getName()+"_"+to_string(num++)+".txt",ios::out);
			fout_newick << newick << ";" << endl;
			fout_newick.close();
		}
	}

}






