#include "change_celltype.h"

REGISTER_PLUGIN(ChangeCelltype);

// TODO: What is the proper interface for MCSListeners?
ChangeCelltype::ChangeCelltype() : InstantaneousProcessPlugin( TimeStepListener::XMLSpec::XML_OPTIONAL), logging("ChangeCelltype") {

	condition.setXMLPath("Condition/text");
	registerPluginParameter(condition);

	celltype_new.setXMLPath("newCellType");
	registerPluginParameter(celltype_new);
	
};

void ChangeCelltype::loadFromXML(const XMLNode xNode, Scope* scope)
{
	InstantaneousProcessPlugin::loadFromXML(xNode, scope);

	celltype_new.init();

	if (xNode.nChildNode("Triggers")) {
		triggers = shared_ptr<TriggeredSystem>(new TriggeredSystem);
		triggers->loadFromXML(xNode.getChildNode("Triggers"), const_cast<Scope*>(celltype_new()->getScope()));
	}
	logging.loadFromXML(xNode, scope);
}

void ChangeCelltype::init(const Scope* scope)
{
	if (CPM::getMCSDuration()>0)
		setTimeStep( CPM::getMCSDuration() );
	
	InstantaneousProcessPlugin::init(scope);
	celltype = scope->getCellType();
	registerInputSymbols(condition.getDependSymbols());

	if (triggers) triggers->init();
	
	logging.setEventSymbols({"condition", "target_celltype"});
	logging.init(scope, Granularity::Cell);
}


void ChangeCelltype::executeTimeStep() {

	int celltype_new_ID = celltype_new()->getID();
	vector <CPM::CELL_ID> cells = celltype->getCellIDs();
	for (int i=0; i < cells.size(); i++ ) {
		auto focus = SymbolFocus(cells[i]);
		auto cond  = condition(focus );
		if ( celltype_new_ID != celltype->getID() && cond ) {
			logging.trigger(focus, { cond, double(celltype_new_ID) });
			auto changed_cell = CPM::setCellType( cells[i], celltype_new_ID);
			if (triggers) triggers->trigger(SymbolFocus(changed_cell));
		
		}
	}
}



