//////
//
// This file is part of the modelling and simulation framework 'Morpheus',
// and is made available under the terms of the BSD 3-clause license (see LICENSE
// file that comes with the distribution or https://opensource.org/licenses/BSD-3-Clause).
//
// Authors:  Joern Starruss
// Copyright 2009-2023, Technische Universität Dresden, Germany
//
//////

#ifndef CPM_TYPES_H
#define CPM_TYPES_H
#include "vec.h"

class Cell;
class CellType;

namespace CPM {

	typedef unsigned int CELL_ID;
	
	enum CELL_INDEX_STATE {
		REGULAR_CELL, SUPER_CELL, SUB_CELL, NO_CELL=99999, VIRTUAL_CELL=100000
	};
	
	/**
	 * @brief CPM::STATE represents the information hold in the cpm lattice and identifies a cell that occupies the node.
	 *
	 * In addition, @p pos stores the position of a node in the coordinate system of 
	 * the cell. Calculating the cell center depends on that coordinate system.
	 */
	struct STATE { 
		CELL_ID cell_id;
// 		CELL_ID super_cell_id;
		VINT pos;
	};
	
	/// @brief Stores the association of a cell, which might change over time
	struct INDEX {
		uint celltype;
		CELL_INDEX_STATE status;
		CELL_ID super_cell_id;
		uint super_celltype;
		uint sub_cell_id;
	};
	
	/** 
	 * Comparison operator that returns true in the case that both states denote the same cpm cell, false otherwise. 
	 * The given position does not matter.
	 */
	inline bool operator ==(const CPM::STATE &a, const CPM::STATE &b)
	{
		return ( a.cell_id == b.cell_id /*&& a.super_cell_id == b.super_cell_id*/);
	}
	


	/**
	 * Comparison operator that returns true in the case that both states do not denote the same cpm cell, false otherwise.
	 * The given position does not matter.
	 */
	inline bool operator !=(const CPM::STATE &a,const CPM::STATE &b)  
	{
		return ( a.cell_id != b.cell_id ); 
	}

	inline bool operator <(const CPM::STATE &a,const CPM::STATE &b)
	{
		return (a.cell_id < b.cell_id);
	}
}


#endif // CPM_TYPES_H
