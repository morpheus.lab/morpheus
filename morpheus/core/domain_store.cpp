#include "domain_store.h"

void domain_store::insert(const VINT& n, bool ignore_duplicates) {
	// find the right starting patch
	auto pos = lower_bound(data.begin(), data.end(),n, 
		[](const StrideData& a, const VINT& b){ 
		return a.start.z<b.z || (a.start.z==b.z && a.start.y<b.y) || (a.start.z==b.z && a.start.y==b.y && a.start.x+a.length < b.x);
	});
	// empty
	bool insert_before = false;
	if (pos == data.end()) {
		insert_before = true;
	}
	// ? append
	else if (pos->start.z==n.z && pos->start.y == n.y)  {
		if (pos->start.x-1 == n.x) {
			pos->start.x-=1; pos->length++;
			_size++;
		}
		else if (pos->start.x+pos->length == n.x) {
			pos->length++;
			_size++;
			auto npos = pos;
			npos++;
			if (npos != data.end() && npos->start == n + VINT(1,0,0)) {
				pos->length += npos->length;
				data.erase(npos);
			}
		}
		else if (pos->start.x -1 > n.x) {
			insert_before = true;
		}
		else if (!ignore_duplicates) {
			stringstream dump;
			dump << " dump ";
			for (const auto& s : data) {
				dump << s.start << "+" << s.length << " | ";  
			}
			cout << "Domain store already contains " << to_str(n) << "\n";
			cout << dump.str() << endl;
			throw string("Domain store already contains ") + to_str(n);
		}
	}
	else {
		insert_before = true;
	}
	
	if (insert_before) {
		data.insert(pos,{n,1});
		_size++;
	}
}

void domain_store::push_back(const VINT& n) {
	if (data.empty()) { 
		data.push_back({n,1}); ++_size;
	}
	else { 
		auto& b =data.back(); VINT last = b.start; last.x+= b.length;
		if (n==last) { ++b.length; ++_size; }
		else if (less_VINT()(last,n)) { data.push_back({n,1}); ++_size; }
		else { insert(n); }
	}
};

bool domain_store::remove(const VINT& n) {
	auto pos = lower_bound(data.begin(), data.end(),n, [](const StrideData& a, const VINT& b){ 
		return a.start.z<b.z || (a.start.z==b.z && a.start.y<b.y) || (a.start.z==b.z && a.start.y==b.y && a.start.x + a.length < b.x) ;
	});
	if (pos == data.end() ) {
		// cout << "Position not in domain store " << n << "\n";
		return false;
	}
	auto d = n - pos->start;
	// cout << "Remove " << n << " picked stride " << pos->start << "+" << pos->length << endl;
	if ( d.z!=0 || d.y != 0 || (d.x <0 || d.x>=pos->length) ) {
		// cout << "Position not in domain store " << n << "\n";
		return false;
	}
	
	return erase_impl( pos, d.x); 
}


bool domain_store::erase_impl(std::deque<StrideData>::iterator pos, int i)
{
	if (pos != data.end() && i < pos->length)  {
		if (i == 0) {
			pos->start.x++; pos->length--;
			if ( pos->length == 0) {
				data.erase(pos);
			}
		}
		else if(i+1 == pos->length) {
			pos->length--;
		}
		else {
			StrideData s1 = { pos->start, i };
			pos->start.x += i+1;
			pos->length -= s1.length+1;
			data.insert(pos,s1);
		}
		
		--_size;
		return true;
	}
	else {
		cout << "Ooops: Domain erase " << pos-data.begin() << ","<<i << " out of snippet  " << pos->start << "+" << pos->length;
		return false;
	}
}

void domain_store::iterator::increment() {
	if (state.section == d->size())
		return;
	++state.idx;
	if (state.idx>=(*d)[state.section].length) {
		state.section++;
		state.idx=0;
		if (state.section == d->size()) {
			value = {0,0,0};
		}
		else 
			set_value();
	}
	else {
		++value.x;
	}
}
		
void domain_store::iterator::decrement() {
	if (state.idx==0) {
		if (state.section>0) {
			--state.section;
			state.idx=(*d)[state.section].length-1;
			set_value();
		}
	}
	else {
		--state.idx;
		--value.x;
	}
}

domain_store::iterator::difference_type domain_store::iterator::distance_to(const iterator& z) const
{
	if (z.state.section==state.section) { return z.state.idx-state.idx; }
	else if (z.state.idx>state.idx) {
		difference_type dist=(*d)[state.section].length-state.idx;
		auto i=state.section+1;
		while (i<z.state.section) { dist+=(*d)[i].length; ++i; }
		dist+=z.state.idx;
		return -dist;
	}
	else {
		difference_type dist=(*d)[z.state.section].length-z.state.idx;
		auto i=z.state.section+1;
		while (i<state.section) { dist+=(*d)[i].length; ++i; }
		dist+=state.idx;
		return dist;
	}
}

void domain_store::iterator::advance(difference_type n) {
	if ( n >0 ) {
		while (n>0) {
			if (state.idx+n>=(*d)[state.section].length) {
				n-=(*d)[state.section].length - state.idx;
				// end check
				if (state.section+1>=d->size()) {
					state.idx = (*d)[state.section].length;
					value = {0,0,0};
					return;
				}
				state.idx=0;
				state.section++;
			}
			else {
				state.idx+=n;
				n=0;
			}
		}
	}
	else {
		n=-n;
		while (n>0) {
			if (state.idx<=n) {
				n-=state.idx;
				// end check
				if (state.section==0) {
					state.section = 0;
					value = {0,0,0};
					return;
				}
				state.idx=(*d)[--state.section].length;
			}
			else {
				state.idx-=n;
				n=0;
			}
		}
	}
	set_value();
}

void domain_store::iterator::set_value() {
	value = (*d)[state.section].start;
	value.x += state.idx;
}

domain_store::iterator::iterator(const deque<domain_store::StrideData>* data, int pos)
	: state({0,0}), d(data)
{
	if (d->empty()) {
		state = {0,0};
		value = {0,0,0};
	}
	else {
		if (pos==0)  {
			value = (*d)[0].start;
		}
		else if (pos>0) {
			advance(pos);
		}
		else {
			// state.idx = d->size()-1;
			// state.i = (*d)[state.idx].length;
			state.section = d->size();
			state.idx = 0;
			value = {0,0,0};
		}
	}
}

