#ifndef CELL_UPDATE_H
#define CELL_UPDATE_H

#include "cpm_layer.h"
#include "symbolfocus.h"


namespace CPM {
	/** @brief Stores details of an update in the cellular potts model.
	 */
	
	
	struct UpdateData {
		/// Neighborhood stencil used for selecting Updates of the spatial configuration, in particular for the Monte Carlo Sampler of the CPM.
		shared_ptr<LatticeStencil> update;
		/// Neighborhood stencil used to select surface nodes. Any node having a different state in the surface neigborhood is a surface node.
		shared_ptr<LatticeStencil> surface;
		/// Neighborhood stencil used to estimate the boundary length and the interaction length with neighboring cells
		shared_ptr<StatisticalLatticeStencil> boundary;
	};
	
	struct UpdateSelection {
		SymbolFocus source;
		SymbolFocus focus;
	};
	
	/// sequential counter of all individual updates
	long long int update_counter();
	
	/** @brief Enumerator type used to traverse update tasks through the cell population interfaces.
	 */
	class Update {
	public:
		Update(const UpdateData& data, shared_ptr<CPM::LAYER> layer);
		Update(const Update& other) : d(other.d), operation(other.operation) { d->ref_count++; };
		Update& operator=(const Update& other);
		Update clone() const;
		~Update() { if (d && --(d->ref_count) == 0) delete d; }
		

		/** @brief Enumerator type for update operations. */
		enum class Operation { Extend, Move };
		/** @brief Enumerator type for atomic update tasks to traverse through the cell population interfaces. */
		enum AtomicOperation { NONE=0x0, ADD=0x1, REMOVE=0x2,  ADD_AND_REMOVE=0x3, MOVE=0x4, NEIGHBORHOOD_UPDATE=0x8 };
		bool valid() const { return operation!=NONE; };
		const class SymbolFocus& source() const { return d->_source; };
		const class SymbolFocus& focus() const { return d->_focus; };
		const class SymbolFocus& focusUpdated() const{ return d->_focus_updated; };
		long long int counter() const { return d->counter; }
		
		// NeighborhoodStencils
		
// 		/// Neighborhood used for the update operation selection, order is angualar
// 		shared_ptr< const LatticeStencil> updateStencil() const { return d.update; };
// 		/// Neighborhood used to determine surface nodes, as used for the connectivity constraint, order is angular
// 		shared_ptr< const LatticeStencil> surfaceStencil() const { return d.surface; };
// 		/// Neighborhood used to compute anything derived from shape boundary length.
// 		/// Most prominently the Interactions, Perimeter, Perimeter constraints, 
// 		shared_ptr< const StatisticalLatticeStencil> boundaryStencil() const { return d.boundary; };
		
		/// Neighborhood used for the update operation selection, order is angualar
		const LatticeStencil* updateStencil() const { return &d->update; };
		/// Neighborhood used to determine surface nodes, as used for the connectivity constraint, order is angular
		const LatticeStencil* surfaceStencil() const { return &d->surface; };
		/// Neighborhood used to compute anything derived from shape boundary length.
		/// Most prominently the Interactions, Perimeter, Perimeter constraints, 
		const StatisticalLatticeStencil* boundaryStencil() const { return &d->boundary; };
		
		int full_op() const { return operation; };
		/// Operation includes addition of Node focus to Cell of focusStateAfter()
		constexpr bool opAdd() const { return operation & ADD; };
		/// Operation includes removal of Node focus from focusStateBefore()
		constexpr bool opRemove() const { return operation & REMOVE; };
		/// Operation includes move of Node 
		constexpr bool opMove() const { return operation & MOVE; };
		/// Operation just notifies about the update in the Neighborhood
		constexpr bool opNeighborhood() const { return operation & NEIGHBORHOOD_UPDATE; };
		/// Select an atomic suboperation of the update.
		Update selectOp(AtomicOperation op) const;
// 		const Update& resetOpSelection() const;
// 		const Update& removeOp(AtomicOperation op) const;
		
		
		/// Set the update to perform operation @p opx from @p source to @p dest
		void set(VINT source, VINT direction, Update::Operation opx);
		/// Set the update to place a certain @p cell at position @p dest
		void set(VINT dest, CELL_ID cell);
		/// Set the update to the UpdateSelection
		void set(const UpdateSelection& selection, Update::Operation opx);
		
		
		void unset() { operation=NONE; d->_source.unset();  d->_focus.unset();  d->_focus_updated.unset(); };
		
	private:
		struct UpdateData_Private {
			UpdateData_Private(const UpdateData& ud) : update(*ud.update), surface(*ud.surface), boundary(*ud.boundary), ref_count(0) {};
			LatticeStencil update;
			/// Neighborhood stencil used to select surface nodes. Any node having a different state in the surface neigborhood is a surface node.
			LatticeStencil surface;
			/// Neighborhood stencil used to estimate the boundary length and the interaction length with neighboring cells
			StatisticalLatticeStencil boundary;
			class SymbolFocus _focus, _source, _focus_updated;
			shared_ptr<CPM::LAYER> layer;
			long long int counter;
			unsigned ref_count;
		};
		UpdateData_Private* d;
		int operation;
	};
	
	inline bool operator == (const CPM::Update &a,const CPM::Update &b ) {
		return (a.source() == b.source() &&  a.focus() == b.focus());
	}
	
	inline ostream& operator <<(ostream& os, const CPM::Update& n) { os << n.source() << " -> " << n.focus() << " = " << n.focusUpdated() << endl; return os;}

	
}


#endif
