#include "cpm_shape.h"
// #include <map>

CPMShape::BoundaryScalingMode CPMShape::scalingMode = BoundaryScalingMode::Magno;
Neighborhood CPMShape::boundary_neighborhood = Neighborhood();

double CPMShape::BoundaryLengthScaling(const Neighborhood& neighborhood, CPMShape::BoundaryScalingMode mode)
{
	map<Lattice::Structure, vector<double> > magno_scaling_by_order =
	{ { Lattice::Structure::linear,    {0,1,2,3,4,5} },
	  { Lattice::Structure::square,    {0, 1.273, 3.074, 5.620, 11.31, 14.92, 18.74, 26.79, 35.97} },
	  { Lattice::Structure::hexagonal, {0,2.206,6.025,10.44,22.11,28.72,36.36,52.26,61.09} },
	  { Lattice::Structure::hexagonal_new, {0,2.206,6.025,10.44,22.11,28.72,36.36,52.26,61.09} },
	  { Lattice::Structure::cubic,     {0,1.500,5.742,9.206,12.21,25.62,40.32,48.80,71.30} }
	};

	double scaling = 1;
// 	BoundaryScalingMode mode = BoundaryScalingMode::Magno;
	switch (scalingMode) {
		case BoundaryScalingMode::Magno :
		{
			const auto& scalings = magno_scaling_by_order[neighborhood.lattice().getStructure()];
			if (neighborhood.order() >= scalings.size()) {
				throw string("No neighborhood normalization scaling available for order "+to_str(neighborhood.order()));
			}
			scaling = scalings[neighborhood.order()];
			break;
		}
		case BoundaryScalingMode::NeighborNumber :
			scaling = neighborhood.size();
			break;
		case BoundaryScalingMode::None:
		case BoundaryScalingMode::Classic:
			scaling = 1.0;
			break;
	}
	return scaling;
}
