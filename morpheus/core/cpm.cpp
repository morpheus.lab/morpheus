#define CPM_CPP

#include "cpm_p.h"


namespace CPM {
	
	bool enabled = false;
	
	LAYER::ValType InitialState, EmptyState; // get overridden during load process;
	uint EmptyCellType;
	
// 	Time_Scale time_per_mcs("MCSDuration",1);
	UpdateData global_update_data;
	unique_ptr<CPM::Update> global_update;
	long long int _update_counter = 0;
	long long int update_counter() { return _update_counter; }
	
	shared_ptr<LAYER> layer;
	shared_ptr<CPMSampler> cpm_sampler;
	Neighborhood update_neighborhood;
	Neighborhood surface_neighborhood;
	
	shared_ptr<EdgeTrackerBase> edgeTracker;
	
	vector< shared_ptr<CellType> > celltypes;
	map< std::string, uint > celltype_names;
	Scope* scope;
	XMLNode xCellPop,xCellTypes,xCPM;
	
	class BoundaryReader : public CPM::LAYER::ValueReader {
	public:
		void set(string input) override {
				auto ct = celltype_names.find(input);
				if ( ct== celltype_names.end()) {
					throw MorpheusException(string("Unknown celltype '") + input + "' at the boundary", xCellPop);
				}
				if ( ! dynamic_pointer_cast<MediumCellType>( celltypes[ct->second] ) ) {
					throw MorpheusException(string("Unable to set celltype '")+input+"' at the boundary. " +
							+ "Medium-like celltype required! ", xCellPop);
				}
				state = celltypes[ct->second]->createCell();
			}
			bool isSpaceConst() const override { return false; }
			bool isTimeConst() const override { return true; }
			CPM::LAYER::ValType get(const VINT& /*pos*/) const override { return state; }
			shared_ptr<CPM::LAYER::ValueReader> clone() const override { return make_shared<BoundaryReader>(*this); }
		private:
			CPM::LAYER::ValType state;
	};
	
bool isEnabled() { return enabled; }

const Cell& getCell(CELL_ID cell_id) {
	return *CellType::storage.cell(cell_id);
}

bool cellExists(CELL_ID cell_id) {
	return (CellType::storage.isFree(cell_id) ? false : true);
}

const CPM::INDEX& getCellIndex(const CELL_ID cell_id) {
	return CellType::storage.index(cell_id);
}


const CPM::LAYER::ValType& getEmptyState() {
	return EmptyState;
};

uint getEmptyCelltypeID() { return EmptyCellType; }

weak_ptr<const CellType> getEmptyCelltype() {
	return celltypes[ EmptyCellType ];
}

weak_ptr<const CellType> findCellType(string name) {
	if (celltype_names.find(name) != celltype_names.end())
		return celltypes[celltype_names[name]];
	else
		return weak_ptr<const CellType>();
}

vector< weak_ptr<const CellType> > getCellTypes() {
	return vector< weak_ptr<const CellType> > (celltypes.begin(),celltypes.end());
}

map<string, weak_ptr<const CellType> > getCellTypesMap() {
	
	map<string, weak_ptr<const CellType> > m;
	for (auto ct : celltypes ) {
		m[ct->getName()] = weak_ptr<const CellType>(ct);
	}
	return m;
}

double getMCSDuration() {
	if (cpm_sampler)
		return cpm_sampler->MCSDuration();
	else 
		return 0;
};

ostream& operator <<(ostream& os, const CPM::STATE& n) {
	os << n.cell_id << " ["<< n.pos << "]";
	// celltypes[getCellIndex(n.cell_id).celltype] -> getName()
	return os;
}

void enableEgdeTracking()
{
	// Don't enable the edge tracker when just creating a dependency graph
	if (SIM::dependencyGraphMode()) return ;
	
	if (!dynamic_pointer_cast<EdgeListTracker>(edgeTracker)) {
		if ( ! update_neighborhood.empty() ) {
			edgeTracker = shared_ptr<EdgeTrackerBase>(new EdgeListTracker(layer, update_neighborhood, surface_neighborhood));
		}
		else {
			edgeTracker = shared_ptr<EdgeTrackerBase>(new EdgeListTracker(layer, surface_neighborhood, surface_neighborhood));
		}
	}
}

shared_ptr<const EdgeTrackerBase> cellEdgeTracker() {                                                      
	return edgeTracker;
}

const Neighborhood& getBoundaryNeighborhood()
{
	return CPMShape::boundaryNeighborhood();
}

const Neighborhood& getSurfaceNeighborhood()
{
	return surface_neighborhood;
}

const Neighborhood& getUpdateNeighborhood()
{
	return update_neighborhood;
}

bool isSurface(const VINT& pos) {
	return edgeTracker->has_surface(pos);
}

uint nSurfaces(const VINT& pos) {
	return edgeTracker->n_surfaces(pos);
};


void loadFromXML(XMLNode xMorph, Scope* local_scope) {
	
	scope = local_scope;
	
	if ( ! xMorph.getChildNode("CellTypes").isEmpty() ) 
		loadCellTypes(xMorph.getChildNode("CellTypes"));
	
	string EmptyCT_string;
	if (getXMLAttribute(xMorph,"CellPopulations/InitialValue/@text", EmptyCT_string)) {
		if (!celltype_names.count(EmptyCT_string)) throw MorpheusException("Invalid CellType", getXMLNode(xMorph,"CellPopulations/InitialValue"));
		EmptyCellType = celltype_names[EmptyCT_string];
	}
	else if ( ! celltypes.empty()) {
		// look for a medium type in predefined celltypes, or create one ...
		for (uint i=0;; i++) {
			if (i == celltypes.size()) {
				// create a default medium celltype, in case no medium was defined
				shared_ptr<CellType> ct =  shared_ptr<CellType>( new MediumCellType( i ) );
				XMLNode medium_node = XMLNode::createXMLTopNode("CellType");
				medium_node.addAttribute("name","Medium");
				medium_node.addAttribute("class","medium");
				ct->loadFromXML(medium_node, scope);
				celltype_names[ct->getName()] = i;
				EmptyCellType = i;
				celltypes.push_back(ct);
				break;
			}
			if (celltypes[i]->isMedium()) {
				EmptyCellType = i;
				break;
			}
		}
	}
	
	if ( ! xMorph.getChildNode("CPM").isEmpty() ) {
		xCPM = xMorph.getChildNode("CPM");
		// CPM time evolution is defined by a MonteCarlo simulation based on the a Hamiltionian and the metropolis kintics
		if ( ! xCPM.getChildNode("MonteCarloSampler").isEmpty() ) {
			cpm_sampler =  shared_ptr<CPMSampler>(new CPMSampler());
			cpm_sampler->loadFromXML(xCPM, scope);
		}
		
		enabled = true;
		
	}
	
	if ( ! xMorph.getChildNode("CellPopulations").isEmpty()) {
		xCellPop = xMorph.getChildNode("CellPopulations");
	}
	
}


void loadCellTypes(XMLNode xCellTypesNode) {
	xCellTypes = xCellTypesNode;
	for (int i=0; i<xCellTypesNode.nChildNode("CellType");i++) {
		string classname;
		XMLNode xCTNode = xCellTypesNode.getChildNode("CellType",i);
		try {
			if ( ! getXMLAttribute(xCTNode,"class", classname))
				throw string("No classname provided for celltype ")+to_str(i);
			shared_ptr<CellType> ct = CellTypeFactory::CreateInstance( classname, celltypes.size() );
			if (ct.get() == NULL)
				throw string("No celltype class ")+classname+" available";
			ct->loadFromXML( xCTNode, scope );
			string name=ct->getName();
			if (name.empty())
				throw string("No name for provided for celltype ")+to_str(i);
			if (name.find_first_of(" \t\n\r") != string::npos)
				throw string("Celltype names may not contain whitespace characters. Invalid name \"")+name+" \"";
			if (celltype_names.find(name) != celltype_names.end()) 
				throw string("Redefinition of celltype ")+name;
			
			celltype_names[name] = celltypes.size();
			celltypes.push_back(ct);
			
			auto celltype_name = SymbolAccessorBase<double>::createConstant(string("celltype.") + name+ ".id", "CellType ID", double(celltype_names[name]));
			SIM::getGlobalScope()->registerSymbol(celltype_name);
 			auto celltype_size = make_shared<CellPopulationSizeSymbol>(string("celltype.") + name + ".size", celltypes.back().get());
			celltype_size->setXMLPath(getXMLPath(xCTNode));
			SIM::getGlobalScope()->registerSymbol(celltype_size);
		}
		catch (string e) {
			throw MorpheusException(string("Unable to create CellType\n") + e, xCTNode);
		}
	}
	
	for (uint i=0; i<celltypes.size(); i++) {
		celltypes[i]->loadPlugins();
#ifdef HAVE_SUPERCELLS
		if (dynamic_pointer_cast<SuperCT>(celltypes[i]) )
			dynamic_pointer_cast<SuperCT>(celltypes[i])->bindSubCelltype();
#endif
	}
	
	if (!celltype_names.empty()) {
		cout << "CellTypes defined: ";
		for (map<string,uint>::iterator ct1=celltype_names.begin(); ct1 != celltype_names.end();ct1++)
			cout << "\'" << ct1->first << "\' ";
		cout << endl;
	}
}

void init() {
	CPMShape::setBoundaryNeighborhood(SIM::lattice().getDefaultNeighborhood());
	if (cpm_sampler)
		update_neighborhood = cpm_sampler->getUpdateNeighborhood();
	
	std::map<Lattice::Structure, int> surface_neighborhood_size {
		{ Lattice::linear, 1 },
		{ Lattice::square, 2 },
		{ Lattice::hexagonal, 1 },
		{ Lattice::hexagonal_new, 1 },
		{ Lattice::cubic, 3 }
	};
	
	if ( ! surface_neighborhood_size.count(SIM::lattice().getStructure()) ) {
		throw string("Missing surface neighborhood size for lattice ") + SIM::lattice().getXMLName();
	}
	surface_neighborhood = SIM::lattice().getNeighborhoodByOrder(surface_neighborhood_size[SIM::lattice().getStructure()]);
	
	if ( enabled ) {
		try {
			// CPM Cell representation requires the definition of the CPM ShapeSurface for shape length estimations
			std::map<Lattice::Structure, int> default_neigbhborhoods {
				{Lattice::linear, 1},
				{Lattice::square, 4},
				{Lattice::hexagonal, 4},
				{Lattice::hexagonal_new, 4},
				{Lattice::cubic, 6}
			};
			auto xNeighborhood = xCPM.getChildNode("ShapeSurface").getChildNode("Neighborhood");
			Neighborhood boundary_neighborhood = SIM::lattice().getNeighborhood( xNeighborhood, { {"optimal", default_neigbhborhoods[SIM::lattice().getStructure()]} });
			int max_offset =0;
			for (const auto& n : boundary_neighborhood.neighbors()) {
				if (abs(n.x) > max_offset) max_offset=abs(n.x);
				if (abs(n.y) > max_offset) max_offset=abs(n.y);
				if (abs(n.z) > max_offset) max_offset=abs(n.z);
			}
			if ( max_offset > CPM::shadow_size ) {
				throw string("Shape neighborhood is too large");
			}
			cout << "Assigning Shape Neighborhood of size " << boundary_neighborhood.distance() <<  " -> " << boundary_neighborhood.neighbors().size() << " neighbors." << endl;
			CPMShape::setBoundaryNeighborhood(boundary_neighborhood);
		} 
		catch (string e) { 
			throw MorpheusException(e, xCPM.getChildNode("ShapeSurface").getChildNode("Neighborhood"));
		}
		string boundary_scaling;
		if (getXMLAttribute(xCPM,"ShapeSurface/scaling",boundary_scaling)) {
			if (boundary_scaling == "none") {
				CPMShape::scalingMode = CPMShape::BoundaryScalingMode::None;
			}
			else if (boundary_scaling == "norm") {
				CPMShape::scalingMode = CPMShape::BoundaryScalingMode::Magno;
			}
			else if (boundary_scaling == "size") {
				CPMShape::scalingMode = CPMShape::BoundaryScalingMode::NeighborNumber;
			}
			else if (boundary_scaling == "magno") {
				CPMShape::scalingMode = CPMShape::BoundaryScalingMode::Magno;
			}
			else if (boundary_scaling == "classic") {
				CPMShape::scalingMode = CPMShape::BoundaryScalingMode::Classic;
			}
		}
	}
	
	if ( ! celltypes.empty()) {
		auto l = SIM::lattice().size() + 6;
		CPM::LAYER::ValType x[16];
		cout << "Creating cell layer of size " << l.x*l.y*l.z*sizeof(x)/16/1024/1024.0<< " MB" << endl;
   
		EmptyState = celltypes[EmptyCellType]->createCell(); 
		InitialState = EmptyState;
		
		cout << "with initial state set to CellType \'" << celltypes[EmptyCellType]->getName() << "\'" << endl;

		layer = make_shared<LAYER>(SIM::getLattice(), InitialState, "cpm");
		if (SIM::lattice().getDomain().domainType() != Domain::none) {
			CellType::storage.cell(InitialState)->setSize(SIM::lattice().getDomain().enumeration().size());
		}
		else {
			auto s = SIM::lattice().size();
			CellType::storage.cell(InitialState)->setSize(s.x*s.y*s.z);
		}
		assert(layer);
		
		// Setting up lattice boundaries
		if (! xCellPop.isEmpty()) {
			layer->loadFromXML( xCellPop, make_shared<BoundaryReader>());
		}
		
		// Setting the initial state
// 		VINT size = SIM::lattice().size();
// 		for (InitialState.pos.z=0; InitialState.pos.z<size.z; InitialState.pos.z++)
// 			for (InitialState.pos.y=0; InitialState.pos.y<size.y; InitialState.pos.y++)
// 				for (InitialState.pos.x=0; InitialState.pos.x<size.x; InitialState.pos.x++)
// // 					if (cpm_layer->writable(InitialState.pos))
// 						layer->set(InitialState.pos, InitialState.);

		// Creating a default global update template
		global_update_data.boundary = unique_ptr<StatisticalLatticeStencil>(new StatisticalLatticeStencil(layer, CPMShape::boundaryNeighborhood()));
		global_update_data.surface = unique_ptr<LatticeStencil>(new LatticeStencil(layer, surface_neighborhood));
		if ( ! update_neighborhood.empty() ) {
			if (update_neighborhood.neighbors() == surface_neighborhood.neighbors()) {
				global_update_data.update = global_update_data.surface;
			}
			else {
				global_update_data.update = make_unique<LatticeStencil>(layer, update_neighborhood);
			}
			// Setting up the EdgeTracker
			edgeTracker = shared_ptr<EdgeTrackerBase>(new NoEdgeTracker(layer, update_neighborhood, surface_neighborhood));
		}
		else {
			global_update_data.update = global_update_data.surface;
			edgeTracker = shared_ptr<EdgeTrackerBase>(new NoEdgeTracker(layer, surface_neighborhood, surface_neighborhood));
		}
		
	}
	else {
		global_update_data.boundary = 0;
		global_update_data.update = 0;
		global_update_data.surface = 0;
	}
	
	loadCellPopulations();
	
	// Init the CellTypes and their (CPM) Plugins
	for (uint i=0; i<celltypes.size(); i++) {
		cout << "Initializing celltype \'" << celltypes[i]->getName() << "\'" <<endl;
		celltypes[i]->init();
	}
	
	// Init the sampler
	if ( cpm_sampler) {
		cpm_sampler->init(SIM::getGlobalScope());
	}

}

void loadCellPopulations()
{
	// Don't load the CellPopulations when we just want to create the symbol graph.
	if (SIM::dependencyGraphMode())
		return;
	
	if ( ! layer &&  ! xCellPop.isEmpty()) {
		// We need at least a cpm Layer and have to specify the neighborhood ...
		throw MorpheusException(string("Unable to create cell populations with no CPM layer available."), xCellPop);
	}
// 	if ( celltypes.size() > 1 && populations.isEmpty()) {
// 		cerr << "No CellPopulations specified." << endl;
// 	}
	
	
	vector<XMLNode> defered_poulations;
	for (int i=0; i<xCellPop.nChildNode("Population"); i++) {
		XMLNode population = xCellPop.getChildNode("Population",i);
		string type;
		getXMLAttribute( population,"type",type);
		auto ct= celltype_names.find(type);
		if (ct == celltype_names.end()) {
			throw MorpheusException(string("Unable to create cell populations for celltype \"")+type+"\"", xCellPop);
		}

#ifdef HAVE_SUPERCELLS
		if (dynamic_pointer_cast< SuperCT >(celltypes[ct->second]) ) {
			defered_poulations.push_back(population);
			continue;
		}
#endif

		celltypes[ct->second] -> loadPopulationFromXML(population);
	}

	for (uint i=0; i<defered_poulations.size(); i++) {
		// we already know that the celltype exists !!
		string type;
		getXMLAttribute( defered_poulations[i],"type",type);
		celltypes[celltype_names[type]] -> loadPopulationFromXML(defered_poulations[i]);
	}
}

XMLNode saveCPM() { return xCPM; };

XMLNode saveCellTypes() { return xCellTypes; }

XMLNode saveCellPopulations() {
	if ( ! CPM::celltypes.empty() ) {
		
		XMLNode xCP = layer->saveToXML().deepCopy();
		for (int i=xCP.nChildNode("Population"); i>0; i--) {
			xCP.getChildNode("Population",i-1).deleteNodeContent();
		}
		for (uint ct=0; ct<celltypes.size(); ++ct ) {
			xCP.addChild(celltypes[ct] -> savePopulationToXML());
		}
// 		auto xLayer = layer->saveToXML();
// 		for (int i=0; i<xLayer.nChildNode("BoundaryValue"); i++) {
// 			xCP.addChild(xLayer.getChildNode("BoundaryValue",i).deepCopy());
// 		}
// 		if (xLayer.nChildNode("InititalValue")) {
// 		}
		return xCP; 
	}
	else 
		return XMLNode();
}

shared_ptr<const CPM::LAYER> getLayer() {
	if (!layer) {
		throw string("Cell layer is undefined. Probably no CellTypes have been defined.");
	}
	return layer;
};

const CPM::LAYER::ValType& getNode(const VINT& pos) {
	assert(layer);
	return layer->get(pos);
};

bool executeCPMUpdate(const CPM::Update& update) {
//      cout << cpm_layer->get(update.focus) << endl;
//      cout << update.focus.pos() << " - "<< update.remove_state.cell_id << " - " << update.add_state.cell_id << endl;
	assert(SIM::lattice().equal_pos(update.focus().pos(), update.focusUpdated().pos()));
	
	
	if (layer->get(update.focus().pos()) != update.focus().cellID()) {
		cout << "Faulty update at " << update.focus() << endl;
		return false;
	}
	try {
		if (update.opAdd() && update.opRemove()) {
			
			const_cast<Cell&> (update.focus().cell()) . applyUpdate(update.selectOp(Update::REMOVE));
			const_cast<Cell&> (update.focusUpdated().cell()) . applyUpdate(update.selectOp(Update::ADD));
			
			// TODO : These updates could be stacked or do we rather need another cell-based lock ?? 
			// Notify all the cells that are adajcent to the focal node wrt. the boundary neighborhood
			if (update.opNeighborhood()) {
				const vector<StatisticalLatticeStencil::STATS>& neighbor_stats = update.boundaryStencil()->getStatistics();
				for (uint i=0; i<neighbor_stats.size(); i++) {
					if ( (neighbor_stats[i].cell != update.focus().cellID()) && (neighbor_stats[i].cell != update.focusUpdated().cellID())) {
						CELL_INDEX_STATE state = getCellIndex(neighbor_stats[i].cell).status;
						if ( state != NO_CELL && state != VIRTUAL_CELL)
							CellType::storage.cell(neighbor_stats[i].cell) -> applyUpdate(update.selectOp(Update::NEIGHBORHOOD_UPDATE));
					}
				}
			}
			if  (! layer->set(update.focus().pos(), update.focusUpdated().cellID()))
				throw string ("CPM::executeCPMUpdate: Trying to write read-only position ") + to_str( update.focus().pos() );
			
			assert(edgeTracker);
			edgeTracker->update_notifier(update.focus().pos(), *update.updateStencil());
		}
		else if (update.opMove()) {
			
		}
		_update_counter++;
	} catch ( string e ) {
		stringstream s;
		s << "error while applying executeCPMUpdate()" << endl;
		s << "[" << update.focus().cellID() << "] -> [" << update.focusUpdated().cellID() << "]" << endl;
		s << e << endl;
		throw s.str();
	}
	return true;
}

CPM::Update& getGlobalUpdate() { if (! global_update) global_update = make_unique<Update>(global_update_data, layer); return *global_update; }

const CPM::Update& createUpdate(VINT source, VINT direction, CPM::Update::Operation opx) {
	
	VINT latt_pos = source + direction;
	Update& global_update = getGlobalUpdate();
	global_update.unset();
	if ( ! layer->writable_resolve(latt_pos) ) {
		cout << "Cannot write to constant node " << latt_pos << ". Rejecting update." << endl;
	}
	else {
		global_update.set(source,direction, opx);
	}
	
	setUpdate(global_update);

	return global_update;
	
}

bool setNode(VINT position, CPM::CELL_ID cell_id) {

	VINT latt_pos = position;
	if ( ! layer->writable_resolve(latt_pos) ) {
		cout << "setNode(): Rejecting write to constant node at " << latt_pos << "." << endl;
		return false;
	}
	
	Update& global_update = getGlobalUpdate();
	global_update.set(position, cell_id); 
	
	if (global_update.valid()) {
		setUpdate(global_update);
		return executeCPMUpdate(global_update);
	}
	else 
		return false;

};

VINT findEmptyNode(VINT min , VINT max) {
	// default behaviour -- any point in the lattice
	if (max == VINT(0,0,0)) max = SIM::lattice().size() - VINT(1,1,1);
	VINT pos;
	for ( int i=0; ; i++) {
		VINT a(0,0,0);
		pos.x= min.x + int(getRandomUint(uint(max.x - min.x)));
		if ( SIM::lattice().getDimensions() > 1) pos.y = min.y +  int(getRandomUint(uint(max.y - min.y)));
		if ( SIM::lattice().getDimensions() > 2) pos.z = min.z +  int(getRandomUint(uint(max.z - min.z)));
		if ( layer->get(pos) == EmptyState  && layer->writable(pos)  ) break;
		if (i==10000) throw string("findEmptyCPMNode: Unable to find empty node for random cell");
	}
	return pos;
}


CELL_ID setCellType(CPM::CELL_ID cell_id, uint celltype)
{
	assert (celltype < celltypes.size());
	uint old_celltype = getCellIndex(cell_id).celltype;
	return celltypes[celltype]->addCell(cell_id);
}

void setUpdate(CPM::Update& update) {
	// we assume that focus source, add_state and remove_state are already set properly
// 	if (update.interaction)
// 		update.interaction->setPosition(update.focus.pos());
// 	update.boundary->setPosition(update.focus.pos());

	// Fwd adding nodes to a supercell to the first subcell
// 	if (update.focusUpdated().cell_index().status == SUPER_CELL ) {
// 		update.add_state.super_cell_id = update.add_state.cell_id;
// 		update.add_state.cell_id = static_cast<const SuperCell&>(getCell(update.add_state.super_cell_id)).getSubCells().front();
// 		update.source = SymbolFocus(update.add_state.cell_id, update.add_state.pos);
// 	}
	
	// Find the proper celltype to notify
// 	update.source_top_ct = update.source.celltype();
// 	update.focus_top_ct =  update.focus.celltype();
	
// 	if ( update.source.cell_index().status == SUB_CELL ) {
// 		// notify the supercell containing the cell
// 		update.source_top_ct = getCellIndex(update.add_state.super_cell_id).celltype;
// 	}
// 
// 	if ( update.focus.cell_index().status == SUB_CELL ) {
// 		// notify the supercell containing the cell
// 		update.focus_top_ct = getCellIndex(update.remove_state.super_cell_id).celltype;
// 	}
	
	if (update.opAdd() && update.opRemove()) {
		const_cast<Cell&>(update.focus().cell()) . setUpdate(update.selectOp(Update::REMOVE));
		const_cast<Cell&>(update.focusUpdated().cell()) . setUpdate(update.selectOp(Update::ADD));
	}

// 	if (update.opNeighborhood()) {
// 		const vector<StatisticalLatticeStencil::STATS>& neighbor_stats = update.boundaryStencil()->getStatistics();
// 		for (uint i=0; i<neighbor_stats.size(); i++) {
// 			if ( (neighbor_stats[i].cell != update.focusUpdated().cellID()) && (neighbor_stats[i].cell != update.focus().cellID())) {
// 				CELL_INDEX_STATE state = getCellIndex(neighbor_stats[i].cell).status;
// 				if ( state != NO_CELL && state != VIRTUAL_CELL)
// 					CellType::storage.cell(neighbor_stats[i].cell) . setUpdate(update.selectOp(CPM::Update::NEIGHBORHOOD_UPDATE));
// 			}
// 		}
// 	}

	
}

void wipe() {
	cout << "sampler " << cpm_sampler.use_count() << " layer " << layer.use_count()<< endl;
	edgeTracker = nullptr;
	cpm_sampler = nullptr;
	celltype_names.clear();
	celltypes.clear();
	
	CellType::storage.wipe();
	global_update = nullptr;
	_update_counter = 0;
	layer = nullptr;
}

}
