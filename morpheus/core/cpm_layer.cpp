#include "cpm_layer.h"
#include "lattice_data_layer.cpp"

// Explicit template instantiation
// template class Lattice_Data_Layer<CPM::LAYER::ValType, CPM::shadow_size>;


StatisticalLatticeStencil::StatisticalLatticeStencil(std::shared_ptr< const CPM::LAYER > data_layer, const Neighborhood& neighbors)
{
	this->data_layer = data_layer;
	setStencil( neighbors );
	
}

void StatisticalLatticeStencil::setStencil(const Neighborhood& neighborhood)
{
	stencil = neighborhood;
	
	auto stencil_neighbors = data_layer->optimizeNeighborhood(neighborhood.neighbors());
	auto alternating_neighbors = data_layer->optimizeNeighborhood(neighborhood.alternating_neighbors());
	is_alternating = ! alternating_neighbors.empty();
	
	stencil_states.resize(stencil_neighbors.size());
	stencil_offsets.resize(stencil_neighbors.size());
	auto center = &data_layer->unsafe_getRef(VINT(0,0,0));
// 	int last_offset = -10000000;
	for (uint i=0; i<stencil_neighbors.size(); i++) {
		int mem_offset = &data_layer->unsafe_getRef(stencil_neighbors[i]) - center;
		stencil_offsets[i] = mem_offset;
// 		if ( abs(mem_offset - last_offset) > 64/sizeof(CPM::STATE) ) {
// 			stencil_row_offsets.push_back(mem_offset);
// 			last_offset = mem_offset;
// 		}
	}
	
	if (is_alternating) {
		alternating_stencil_offsets.resize(alternating_neighbors.size());
		auto center = &data_layer->unsafe_getRef(VINT(0,0,0));
		for (uint i=0; i<alternating_neighbors.size(); i++) {
			int mem_offset = &data_layer->unsafe_getRef(alternating_neighbors[i]) - center;
			alternating_stencil_offsets[i] = mem_offset;
		}
	}
}

void StatisticalLatticeStencil::setPosition(const VINT& pos)
{
	this->pos = pos;
	valid_data = false;
}

void StatisticalLatticeStencil::applyPos() const
{
	stencil_statistics.resize(0);
	assert(data_layer->lattice().inside(pos));
	auto center = &data_layer->unsafe_getRef(pos);
// #ifdef __GNUC__
// 		for (uint k=0; k<stencil_row_offsets.size(); ++k ) {
// 			__builtin_prefetch(&data_layer->data[ center_index + stencil_row_offsets[k]],0,1);
// 		}
// #endif
	const vector<int>& offsets = (is_alternating && (pos.y & 1)) ? alternating_stencil_offsets : stencil_offsets;
	const uint stencil_offsets_size = offsets.size();
	uint stencil_statistics_size = 0;
	
	for (uint k=0; k<stencil_offsets_size; ++k ) {
		stencil_states[k] = *(center + offsets[k]);
		uint stack_id = 0;
		
		while(1) {
			if (stack_id == stencil_statistics_size) {
				stencil_statistics.push_back( {stencil_states[k], 1} );
				stencil_statistics_size++;
				break;
			}
			if (stencil_statistics[stack_id].cell == stencil_states[k]) {
				stencil_statistics[stack_id].count++;
				break;
			}
			stack_id++;
		}
	}
	valid_data = true;
}

LatticeStencil::LatticeStencil(shared_ptr< const CPM::LAYER >  data_layer, const Neighborhood& neighbors )
{
	this->data_layer = data_layer;
	setStencil(neighbors);
	
}

void LatticeStencil::setPosition(const VINT& pos)
{
	// whenever set is called invalidate, because the underlying state may has changed meanwhile
	this->pos = pos;
	valid_data = false;
}

void LatticeStencil::applyPos() const {
	auto center = &data_layer->unsafe_getRef(pos);
	
	if (is_alternating && pos.y & 1) {
		for (uint i=0; i< stencil_offsets.size(); i++) {
			stencil_states[i] = *(center + alternating_stencil_offsets[i]);
		}
	}
	else {
		for (uint i=0; i< stencil_offsets.size(); i++) {
			stencil_states[i] = *(center + stencil_offsets[i]);
		}
	}
	valid_data = true;
}


void LatticeStencil::setStencil(const Neighborhood& neighborhood)
{
	stencil = neighborhood;
	
	auto stencil_neighbors = neighborhood.neighbors();
	auto alternating_neighbors = neighborhood.alternating_neighbors();
	is_alternating = ! alternating_neighbors.empty();
	
	stencil_states.resize(stencil_neighbors.size());
	stencil_offsets.resize(stencil_neighbors.size());
	auto center = &data_layer->unsafe_getRef(VINT(0,0,0));
	for (uint i=0; i<stencil_neighbors.size(); i++) {
		int mem_offset = &data_layer->unsafe_getRef(stencil_neighbors[i]) - center;
		stencil_offsets[i] = mem_offset;
	}
	
	if (is_alternating) {
		alternating_stencil_offsets.resize(alternating_neighbors.size());
		auto center = &data_layer->unsafe_getRef(VINT(0,0,0));
		for (uint i=0; i<alternating_neighbors.size(); i++) {
			int mem_offset = &data_layer->unsafe_getRef(alternating_neighbors[i]) - center;
			alternating_stencil_offsets[i] = mem_offset;
		}
	}
}

