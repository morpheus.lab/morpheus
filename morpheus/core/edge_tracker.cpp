#include "edge_tracker.h"
#include "lattice_data_layer.cpp"

template class Lattice_Data_Layer<EdgeListTracker::Edge_Id_List,0>;

const unsigned int EdgeListTracker::no_edge = numeric_limits<unsigned int>::max()-1;
const unsigned int EdgeListTracker::no_flux_edge = numeric_limits<unsigned int>::max();
thread_local EdgeListTracker::WorkerData EdgeListTracker::worker_data;

void EdgeListTracker::wipe()
{
// 	std::cout << "wiping EdgeListTracker" << std::endl;
	if (edge_lattice) {
		auto data = edge_lattice->getData();
		for(auto list : data) {
			if (list) delete[] list;
		}
	}

	// cleanup of thread_local variables
	auto clear_thread =
		[this](int){ 
			edgeStash().clear();
			auto& list_stash = edgeListStash();
			for (auto& list : list_stash ) {
				delete[] list;
			}
			list_stash.clear();
		};
		
#ifdef CPM_OMP_LOCKS
#pragma omp parallel num_threads(TP::CPMteamSize())
	clear_thread(omp_get_thread_num());
#else
	vector<int> params; params.resize(TP::CPMteamSize()-1);
	auto result = TP::getThreadPool().parallel_submit(clear_thread, params );
	clear_thread(0);
	for (auto& res : result) { res.wait(); }
#endif
}

void EdgeListTracker::reset() {
	wipe();
	init_edge_list();
}


EdgeListTracker::EdgeListTracker(shared_ptr< const CPM::LAYER >p, const Neighborhood &opx_nei, const Neighborhood& surface_nei) :
	EdgeTrackerBase(p, opx_nei, surface_nei),
	edge_list_size(opx_nei.size())
{
	Edge_Id_List emptyList = nullptr;
	cout << "creating Edge tracker " << endl;
	edge_lattice = make_unique< Lattice_Data_Layer<Edge_Id_List,0> >(this->lattice, emptyList, "edge_list");
	// creating a list of the indices of the neighbors in the opposite direction
	auto opx_neighbors = this->opx_neighbors.neighbors();
	auto surface_neighbors = this->surface_neighbors.neighbors();
	for (vector<VINT>::iterator nei = opx_neighbors.begin(); nei != opx_neighbors.end(); nei++) {
		auto other_neighbors = this->opx_neighbors.neighbors(*nei);
		for (uint o_nei = 0; ; o_nei++) {
			if (o_nei == other_neighbors.size()) {
				cerr << "Edge_List_Stepper:: unsymmetric opx_neighbors !" << endl; exit(-1);
			}
			if (other_neighbors[o_nei] + (*nei) == VINT(0,0,0) ) {
				inverse_neighbor.push_back(o_nei); 
				// remove the neighbor while not changing the indices of the others
				// this is essential when neighbors show up multiple times as in FCHC
				other_neighbors[o_nei]=VINT(0,0,0); 
				break;
			}
		}
		
		opx_is_surface.push_back(find(surface_neighbors.begin(),surface_neighbors.end(),*nei) != surface_neighbors.end());
	}
	is_alternate = !this->opx_neighbors.alternating_neighbors().empty();
	if (is_alternate) {
		auto opx_neighbors = this->opx_neighbors.alternating_neighbors();
// 		auto surface_neighbors = this->surface_neighbors.alternating_neighbors();
		for (vector<VINT>::iterator nei = opx_neighbors.begin(); nei != opx_neighbors.end(); nei++) {
			auto other_neighbors = this->opx_neighbors.neighbors(VINT(0,1,0) + *nei);
			for (uint o_nei = 0; ; o_nei++) {
				if (o_nei == other_neighbors.size()) {
					cerr << "Edge_List_Stepper:: unsymmetric opx_neighbors !" << endl; exit(-1);
				}
				if (other_neighbors[o_nei] + (*nei) == VINT(0,0,0) ) {
					inverse_alternate_neighbor.push_back(o_nei); 
					// remove the neighbor while not changing the indices of the others
					// this is essential when neighbors show up multiple times as in FCHC
					other_neighbors[o_nei]=VINT(0,0,0); 
					break;
				}
			}
		}
	}
	
	// Register the thread local stashes to 
	thread_edge_stash.resize(TP::CPMteamSize(), nullptr);
	
	
#ifdef CPM_OMP_LOCKS
#pragma omp parallel num_threads(TP::CPMteamSize())
	{
		thread_edge_stash[omp_get_thread_num()] = &edgeStash();
		edges_shared_mtx.register_thread();
	}
#else 
	thread_edge_stash[TP::teamNum()] = &edgeStash();
	edges_shared_mtx.register_thread();
	vector<int> params; params.resize(TP::CPMteamSize()-1);
// 	cout << "Assigning stash " << TP::teamNum() << endl;

	auto result = TP::getThreadPool().parallel_submit(
		[this](int){ 
			thread_edge_stash[TP::teamNum()] = &edgeStash();
			edges_shared_mtx.register_thread();
// 			cout << "Assigning stash " << TP::teamNum() << endl;
		},
		params
	);
	
	for (auto& res : result) { res.wait(); }
#endif
	keeps_since_defrag =0;
	init_edge_list();
};

EdgeListTracker::~EdgeListTracker()
{ 
	wipe();
}

std::vector< EdgeListTracker::Edge_Id_List >& EdgeListTracker::edgeListStash() {
// 	static thread_local std::vector< EdgeListTracker::Edge_Id_List > list;
// 	return list;
	return worker_data.edge_list_stash;
}

std::vector<EdgeListTracker::Edge_Id>& EdgeListTracker::edgeStash() {
// 	static thread_local std::vector< Edge_Id > list;
// 	return list;
	return worker_data.edge_id_stash;
}

void EdgeListTracker::init_edge_list() {
	// Crawl through the lattice and extract all existing boundaries
	unsigned long int initial_edges = 0;
	unsigned long int update_edges = 0;
	VINT l_size = this->lattice->size();
	uint l_dim = this->lattice->getDimensions();
	VINT pos;
	for (pos.z = 0; pos.z<l_size.z; pos.z++) {
		for (pos.y = 0; pos.y<l_size.y; pos.y++) {
			for (pos.x = 0; pos.x<l_size.x; pos.x++) {
				
				if ( ! edge_lattice->writable_resolve(pos) )
					continue;
				
				// Find out if there are boundaries that can recieve updates
				bool has_boundaries = false;
				const CPM::CELL_ID cpm_cell_a = this->cell_layer->unsafe_get(pos);
				bool use_alternate = is_alternate && (pos.y & 1) == 1;
				const auto& opx_neighbors =  use_alternate ? this->opx_neighbors.alternating_neighbors() : this->opx_neighbors.neighbors();
				
				for (unsigned short neighbor = 0; neighbor < opx_neighbors.size(); neighbor++) {
					// get_writable uses the  topological boundaries to wrap the point into the accessible space, which is saved in edge.pos_b
					VINT nei_pos = pos + opx_neighbors[neighbor];
					Boundary::Type bt;
					this->cell_layer->writable_resolve(nei_pos,bt);
					const CPM::CELL_ID cpm_cell_b = this->cell_layer->unsafe_get(nei_pos);
					// this->cpm_lattice->get(edge.pos_b);
					if ( cpm_cell_a != cpm_cell_b ) {
						
						has_boundaries = true;
						break;
					}
				}
				
				if (!has_boundaries) {
					continue;
				}
				
				// There are boundaries that can recieve updates
				Edge edge;
				edge.pos = pos;
				auto& eid_list_a = edge_lattice->unsafe_getRef(edge.pos);
				if (! eid_list_a) {
					eid_list_a = new Edge_Id[edge_list_size+1];
					std::fill(eid_list_a, eid_list_a + edge_list_size, no_edge);
					eid_list_a[edge_list_size] = 0;
				}
				edge.valid = true;
				Boundary::Type bt;
				
				for (unsigned short neighbor = 0; neighbor < opx_neighbors.size(); neighbor++) {
					
					if ((eid_list_a)[neighbor] != no_edge){
						// a neighbor already registered the boundary !
						continue;
					}
					// get_writable uses the  topological boundaries to wrap the point into the accessible space, which is saved in edge.pos_b
					auto pos_b = edge.pos + opx_neighbors[neighbor];
					const CPM::CELL_ID cpm_cell_b = this->cell_layer->get(pos + opx_neighbors[neighbor]);
					if ( edge_lattice->writable_resolve(pos_b, bt) ) 
						// that neighboring node is writable !!
					{
						if ( cpm_cell_a != cpm_cell_b ) {	// there will be an edge
// 							if (! eid_list_a) *edge.eid_list_a = freshList();
							
							auto& eid_list_b = edge_lattice->unsafe_getRef(pos_b);
							if (! eid_list_b) {
								eid_list_b  = new Edge_Id[edge_list_size+1];
								std::fill(eid_list_b, eid_list_b + edge_list_size, no_edge);
								eid_list_b[edge_list_size] = 0;
							}
							edge.direction = neighbor; //index of the opx_neighborhood
							uint new_eid = edges.size();
							edges.push_back(edge);
							eid_list_a[neighbor] = new_eid;
							eid_list_a[edge_list_size]++;
							eid_list_b[use_alternate ? inverse_alternate_neighbor[neighbor] : inverse_neighbor[neighbor]] = new_eid;
							eid_list_b[edge_list_size]++;
							initial_edges++;
							update_edges++;
						}
						
					}
					else if ( bt == Boundary::constant) 
						// that neighboring node is not writable, but can source new nodes 
					{
						if ( cpm_cell_a != cpm_cell_b ) {
							
							// not registered in the boundary neighbor
							edge.direction = neighbor; //index of the opx_neighborhood

							uint new_eid = edges.size();
							edges.push_back(edge);
							eid_list_a[neighbor] = new_eid;
							eid_list_a[edge_list_size]++;
							initial_edges++;
							update_edges++;
						}
					}
					else if ( bt == Boundary::noflux) 
						// that neighboring node is not writable and can not source new nodes 
					{
						if ( cpm_cell_a != cpm_cell_b ) {
							eid_list_a[neighbor] = no_flux_edge;
							eid_list_a[edge_list_size]++;
// 							initial_edges++;
						}
						else {
							eid_list_a[neighbor] = no_edge;
						}
					}
				}
			}
		}
	}
	
	edges_size = edges.size();
	
	l_size=l_size+2;
	cout << "EdgeListTracker::init() : Created Tracker with Neighborhood size " << surface_neighbors.size() << " of size " <<l_size.x*l_size.y*l_size.z * sizeof(Edge_Id_List) / 1024/1024.0 << " MB"<<    endl;
	cout << "EdgeListTracker::init() : Found " << initial_edges << " initial edges, wherof " << update_edges << " can be modified." << endl;
}


uint EdgeListTracker::updates_per_mcs() const {
	double factor = 2.0 / this->opx_neighbors.size();
	uint stashed_edges = std::accumulate(thread_edge_stash.begin(),
										 thread_edge_stash.end(),
										 0,
										 [](uint sum, const vector<Edge_Id>* stash) -> uint { 
											if (stash) {
												return sum + stash->size();
											}
											else { 
												return sum;
											}
										 });
	return uint( (edges_size - stashed_edges) * factor);
};

void EdgeListTracker::get_update(VINT& origin, VINT& direction) const noexcept
{
	uint n_try=0;
	bool spin = getRandomBool();
	
	edges_shared_mtx.lock_shared();	
	do {
		assert(++n_try < 500);
		const auto edge = edges[getRandomUint( edges_size-1 )];
		if ( ! edge.valid ) continue;
		edges_shared_mtx.unlock_shared();
		
		const auto& neighbors = this->opx_neighbors.neighbors(edge.pos);
		auto pos_b = edge.pos + neighbors[edge.direction];
		if ( spin ) {
			if ( ! edge_lattice->writable_resolve(pos_b) ) continue; // node b, the focus, is not writable ...
			origin = edge.pos;
			direction = neighbors[edge.direction];
			break;
		} else {
			if ( ! edge_lattice->writable(edge.pos) ) continue;
			edge_lattice->resolve(pos_b);
			origin = pos_b;
			direction = -1 * neighbors[edge.direction];
			break;
		}
		
	} while (1) ;
}

void EdgeListTracker::update_notifier(const VINT& pos, const LatticeStencil& neighborhood) {
	Edge edge;
	edge.pos=pos;
	edge.valid = false;

	// pos is in cpm_lattice space. we have to map it before we can use it ...
	this->lattice->resolve(edge.pos);
	const CPM::CELL_ID cpm_cell_a = this->cell_layer->unsafe_get(edge.pos);
	
// 	int t = TP::teamNum();
	auto& edge_stash = edgeStash();
	auto& edge_list_stash = edgeListStash();
	
	// Assert sufficient empty edges on the stash 
	if (edge_stash.size() < opx_neighbors.size()) {
		std::unique_lock guard(edges_shared_mtx);
		while (edge_stash.size()<opx_neighbors.size()*2) {
			edges.push_back({VINT(0,0,0), 0, false});
			edge_stash.push_back(edges_size);
			edges_size++;
		}
	}
	
	if (edge_list_stash.size() < opx_neighbors.size()+1) {
		for (uint i=0; i<opx_neighbors.size()+1; i++ ) {
			auto eid_list = new Edge_Id[edge_list_size+1];
			std::fill(eid_list, eid_list + edge_list_size, no_edge);
			eid_list[edge_list_size] = 0;
			edge_list_stash.push_back(eid_list);
		}
	}

	auto& eid_list_a = edge_lattice->unsafe_getRef(edge.pos);
	if ( ! eid_list_a ) {
		eid_list_a = edge_list_stash.back();
		edge_list_stash.pop_back();
	}
	
	Boundary::Type bt;
	// appending new meaningful edges and deleting meaningless
	bool using_alternate = is_alternate && (edge.pos.y & 1);
	auto& opx_neighbors = using_alternate ? this->opx_neighbors.alternating_neighbors() : this->opx_neighbors.neighbors();
	auto& inverse_opx_neighbors = using_alternate ? this->inverse_alternate_neighbor : this->inverse_neighbor;
	sf::shared_lock_guard guard(edges_shared_mtx);
// 	std::shared_lock guard(edges_shared_mtx);
	for (unsigned int neighbor = 0; neighbor < opx_neighbors.size(); neighbor++) {
		// get_writable uses the  topological boundaries to wrap the point into the accessible space, which is saved in edge.pos_b
		auto pos_b = edge.pos + opx_neighbors[neighbor];
		const CPM::CELL_ID cpm_cell_b = neighborhood.getStates()[neighbor];
		if ( edge_lattice->writable_resolve(pos_b, bt) ) 
			// that neighboring node is writable !!
		{
			if ( cpm_cell_a != cpm_cell_b ) {	// there will be an edge
				if (eid_list_a[neighbor] == no_edge) {  // there was no edge before
					auto& eid_list_b = edge_lattice->unsafe_getRef(pos_b);
					if ( ! eid_list_b) {
						assert(!edge_list_stash.empty());
						eid_list_b = edge_list_stash.back();
						edge_list_stash.pop_back();
					}
					edge.direction = neighbor; //index of the opx_neighborhood
					uint new_eid = edge_stash.back();
					edge_stash.pop_back();
// 					edges_shared_mtx.lock_shared();
					edges[new_eid] = edge;
// 					edges_shared_mtx.unlock_shared();
					
					eid_list_a[neighbor] = new_eid;
					eid_list_a[edge_list_size]++;
					eid_list_b[ inverse_opx_neighbors[neighbor] ] = new_eid;
					eid_list_b[edge_list_size]++;
					
					edges[new_eid].valid  = true;
					
				}
				else {
					// else the boundary is just conserved ...
				}
				
			}
			else {		// there will be no edge
				unsigned int remove_edge_id  = eid_list_a[neighbor];
				if ( remove_edge_id != no_edge ) {
					auto& eid_list_b = edge_lattice->unsafe_getRef(pos_b);
					if (!eid_list_b) {
						throw string("Invalid eid_list at ") + to_str(pos_b) + " next to " + to_str(edge.pos);
						assert(eid_list_b);
					}
					
					eid_list_a[ neighbor ] = no_edge;
					eid_list_a[edge_list_size]--;
					eid_list_b[ inverse_opx_neighbors[neighbor] ] = no_edge;
					eid_list_b[edge_list_size]--;
					
					if (eid_list_b[edge_list_size] ==0) {
						edge_list_stash.push_back(eid_list_b);
						eid_list_b = nullptr;
					}
					if ( remove_edge_id != no_flux_edge) {
// 						edges_shared_mtx.lock_shared();
						edges[remove_edge_id].valid = false;
// 						edges_shared_mtx.unlock_shared();
						edge_stash.push_back(remove_edge_id);
					}
				} 
				else {
					stringstream error;
					error <<  "EdgeListTracker:: Error while removing a boundary at " << edge.pos << " ; " << pos_b << " which is already 'no_edge'."<< endl; assert(0); throw error.str();
				}
			}
		} 
		else if ( bt == Boundary::constant) 
			// that neighboring node is not writable, but can source new nodes 
		{
			if ( cpm_cell_a != cpm_cell_b ) {
				if (eid_list_a[neighbor] == no_edge) {  // there was no edge before

					edge.direction = neighbor; //index of the opx_neighborhood

					uint new_eid = edge_stash.back();
					edge_stash.pop_back();
// 					edges_shared_mtx.lock_shared();
					edges[new_eid] = edge;
// 					edges_shared_mtx.unlock_shared();
					
					eid_list_a[neighbor] = new_eid;
					eid_list_a[edge_list_size]++;
					edges[new_eid].valid = true;
				}
				else {
					// else the boundary is just conserved ...
				}
				
			}
			else {		// there will be no edge
				unsigned int remove_edge_id  = eid_list_a[neighbor];
				if ( remove_edge_id != no_edge) {
					eid_list_a[ neighbor ] = no_edge;
					eid_list_a[edge_list_size]--;

// 					edges_shared_mtx.lock_shared();
					edges[remove_edge_id].valid = false;
// 					edges_shared_mtx.unlock_shared();
					edge_stash.push_back(remove_edge_id);
				} 
				else {
					stringstream error;
					error << "EdgeListTracker::Boundary::constant Error while removing a boundary at " << edge.pos << " which is already 'no_edge'."<< endl;
					throw error.str();
				}
			}
		}
		else // ( bt == Boundary::noflux) 
		// that neighboring node is not writable and cannot source new nodes  (i.e. noflux)
		{
			if ( cpm_cell_a != cpm_cell_b ) {
				if (eid_list_a[neighbor] == no_edge) {  // there was no edge before
					eid_list_a[neighbor] = no_flux_edge;
					eid_list_a[edge_list_size]++;
				}
				else {
					// else the boundary is just conserved ...
				}
				
			}
			else {		// there will be no edge
				unsigned int remove_edge_id  = eid_list_a[neighbor];
				if ( remove_edge_id == no_flux_edge) {
					eid_list_a[ neighbor ] = no_edge;
					eid_list_a[edge_list_size]--;
				} 
				else { 
					stringstream error;
					error << "EdgeListTracker::Boundary::noflux Error while removing a boundary at " << edge.pos << "->" << neighbor << " which is already '";
					if (remove_edge_id==no_flux_edge)
						error << "no_flux_edge";
					else if (remove_edge_id==no_edge) 
						error << "no_edge";
					else 
						error << remove_edge_id;
					error << "'."<< endl;
					throw error.str();
				}
			}
		}
	}
	
	if (eid_list_a[edge_list_size] == 0) {
		edge_list_stash.push_back(eid_list_a);
		eid_list_a = nullptr;
	}

};

void EdgeListTracker::house_keeping() const {
	auto that = const_cast<EdgeListTracker*>(this);
	uint edge_stash_size = that->equi_workers();
	that->keeps_since_defrag++;
	if ( (keeps_since_defrag>1000 && double (edge_stash_size - thread_edge_stash.size() * opx_neighbors.size() * 2) / (edges.size()) > 0.1) || keeps_since_defrag>5000 ) {
		// cout << "EdgeListTracker: Running defragmentation " << endl;
		// cout << getStatInfo();
		const_cast<EdgeListTracker*>(this)->defragment();
		that->keeps_since_defrag=0;
	}
}


uint EdgeListTracker::equi_workers() {
	uint stashed_edges=0; uint n_stashes=0;
	for (auto& stash : thread_edge_stash) {
		stashed_edges += stash ? stash->size() : 0;
		n_stashes += stash != nullptr;
	}
	
	auto edges_per_stash = stashed_edges / n_stashes;
	bool first = true;
	vector< EdgeListTracker::Edge_Id > tmp;
	for (auto& stash : thread_edge_stash) {
		auto n_optimal = edges_per_stash;
		if (first) {
			n_optimal = stashed_edges - (n_stashes-1) * edges_per_stash;
			first = false;
		}
		while (stash->size()>n_optimal) {
			tmp.push_back(stash->back());
			stash->pop_back();
		}
		while (!tmp.empty() && stash->size()<n_optimal) {
			stash->push_back(tmp.back());
			tmp.pop_back();
		}
	}
	first = true;
	for (auto& stash : thread_edge_stash) {
		auto n_optimal = edges_per_stash;
		if (first) {
			n_optimal = stashed_edges - (n_stashes-1) * edges_per_stash;
			first = false;
		}
		while (stash->size()>n_optimal) {
			tmp.push_back(stash->back());
			stash->pop_back();
		}
		while (!tmp.empty() && stash->size()<n_optimal) {
			stash->push_back(tmp.back());
			tmp.pop_back();
		}
	}
	return stashed_edges;
}

void EdgeListTracker::defragment() 
{
	set<uint> dangling_ids;
	vector<Edge_Id> edge_stash = 
		std::accumulate(
			thread_edge_stash.begin(),
			thread_edge_stash.end(),
			vector<Edge_Id>() , 
			[]( vector<Edge_Id>& partial_sum, vector<Edge_Id>* stash) { 
				if (stash) {
					partial_sum.insert(partial_sum.end(), stash->begin(), stash->end());
					stash->clear();
				}
				return partial_sum;
			}
		);

	// cout << "running defrag"<< endl;
	while ( ! edge_stash.empty()) {
		if (dangling_ids.count( edge_stash.back())) {
			dangling_ids.erase(edge_stash.back());
			         edge_stash.pop_back();
		}
		else if (edges.back().valid) {
			auto new_id = edge_stash.back();
			edge_stash.pop_back();
			
			edges[new_id] = edges.back();
			edges.pop_back();
			
			auto& edge = edges[new_id];
			auto& eid_list_a = edge_lattice->unsafe_getRef(edge.pos);
			if (!eid_list_a)
				throw string("EdgeListTracker::defragment() unexpected empty edge_list at ") + to_str(edge.pos);
			
			eid_list_a[edge.direction] = new_id;
			
			bool using_alternate = is_alternate && (edge.pos.y & 1);
			const auto& opx_neighbors = using_alternate ? this->opx_neighbors.alternating_neighbors() : this->opx_neighbors.neighbors();
			const auto& inverse_opx_neighbors = using_alternate ? this->inverse_alternate_neighbor : this->inverse_neighbor;
			VINT pos_b = edge.pos + opx_neighbors[edge.direction];
			if (edge_lattice->writable_resolve(pos_b))
				edge_lattice->unsafe_getRef(pos_b)[inverse_opx_neighbors[edge.direction]] = new_id;
			
		}
		else {
			auto invalid_id = edges.size()-1;
			edges.pop_back();
// 			invalid_edge_ids.erase(find(invalid_edge_ids.begin(),invalid_edge_ids.end(), invalid_id));
			dangling_ids.insert(invalid_id);
		}
	}
	assert(dangling_ids.empty());
	edges_size = edges.size();
}


bool EdgeListTracker::has_surface(const VINT& pos) const{
	VINT k(pos);
	if ( ! this->lattice->resolve(k) ) return true;
	const Edge_Id_List& edge_list = edge_lattice->get(k);
	if ( ! edge_list) return false;
	return edge_list[edge_list_size] > 0;
// 	for (uint i = 0; i< this->opx_neighbors.size(); i++) {
// 		if (opx_is_surface[i] &&  edge_list[i] != no_edge ) return true;
// 	}
// 	return false;
}

uint EdgeListTracker::n_surfaces(const VINT& pos) const{
	VINT k(pos);
	if ( ! this->lattice->resolve(k) ) return 1;
	const Edge_Id_List edge_list = edge_lattice->get(pos);
	if ( ! edge_list) return 0;
	return edge_list[edge_list_size];
// 	uint n_edges(0);
// 	for (uint i = 0; i< this->opx_neighbors.size(); i++) {
// 		n_edges += (opx_is_surface[i] &&  edge_list[i] != no_edge );
// 	}
// 	return n_edges;
}


string EdgeListTracker::getStatInfo() const {
	stringstream info;
	uint edge_stash_size = 
	std::accumulate(
		thread_edge_stash.begin(),
		thread_edge_stash.end(),
		0 , 
		[](uint partial_sum, const vector<Edge_Id>* stash) { 
			partial_sum += stash->size();
			return partial_sum;
		}
	);
	auto edge_size = edges.size();
	info << "EdgeListTracker: EdgeList size " << edge_size << ", invalid " << edge_stash_size << endl;
	info << "                 Select ratio is " <<  double (edge_size -  edge_stash_size) / edge_size << endl;
	info << "                 Opx Neighborhood is " << this->opx_neighbors.size() << endl;
	return info.str(); 
	
}


ostream& EdgeListTracker::dump(ostream& out) const {
	for (const auto& edge : edges) {
		if (edge.valid) {
			out << edge.pos << "->" << edge.direction << " " <<edge_lattice->get(edge.pos)[edge.direction] << "\n";
		}
	}
	return out;
}
