#pragma once
#include "vec.h"
class domain_store {
private:
	struct StrideData {
		VINT start;
		int length;
	};
	deque<StrideData> data ={};
	size_t _size =0;
public:
	
	class iterator 
	{
		public:
			using difference_type = long;
			using value_type = VINT;
			using pointer = const VINT*;
			using reference = const VINT;
			using iterator_category = std::random_access_iterator_tag;
		
		private:
			struct StrideState {
				int section; int idx;
				bool operator==(const StrideState& other) const { return section==other.section && idx==other.idx; };
				bool operator!=(const StrideState& other) const { return section!=other.section || idx!=other.idx; };
				bool operator<=(const StrideState& other) const { return idx<other.idx || (idx==other.idx && section<=other.section); };
				bool operator<(const StrideState& other)  const { return idx<other.idx || (idx==other.idx && section<other.section); };
				bool operator>=(const StrideState& other) const { return idx>other.idx || (idx==other.idx && section>=other.section); };
				bool operator>(const StrideState& other)  const { return idx>other.idx || (idx==other.idx && section>other.section); };
			} state;
			VINT value;
			const deque<StrideData>* d;
			
			void increment();
			void decrement();
			void advance(difference_type n);
			difference_type distance_to(const iterator& z) const;
			void set_value();
			
			explicit iterator(const deque<StrideData>* data, StrideState pos)  : state(pos), d(data) { set_value(); };
			friend domain_store;
			
		public:
			
			explicit iterator(const deque<StrideData>* data, int pos = 0) ;
			iterator(const domain_store::iterator&) = default;
			iterator(domain_store::iterator&&) = default;
			iterator() : d(nullptr) {};
			domain_store::iterator& operator=(const domain_store::iterator&) = default;
			
			reference  operator*() const { return value; }
			pointer operator->() const { return &value; }
			bool operator==(const iterator& other) const { return state==other.state; };
			bool operator!=(const iterator& other) const { return state!=other.state; };
			bool operator<=(const iterator& other) const { return state<=other.state; };
			bool operator<(const iterator& other) const { return state<other.state; };
			bool operator>=(const iterator& other) const { return state>=other.state; };
			bool operator>(const iterator& other) const { return state>other.state; };
			
			iterator& operator++() { increment();  return *this; };
			iterator operator++(int) { auto r = iterator(*this); increment(); return r; }
			iterator operator +(difference_type n) const { auto r = iterator(*this); r.advance(n); return r; };
			iterator& operator +=(difference_type n) { advance(n); return *this; };
			iterator& operator--() { decrement(); return *this; };
			iterator operator--(int) { auto r = iterator(*this); decrement(); return r; }
			iterator operator -(difference_type n) const { auto r = iterator(*this); r.advance(-n); return r; };
			iterator& operator -=(difference_type n) { advance(-n); return *this; };
			difference_type operator -(const iterator& z) const { return distance_to(z); };
			
	};
	using const_iterator = iterator;
	void insert(const VINT& n, bool ignore_duplicates = false);
	void push_back(const VINT& n);
	bool remove(const VINT& n);
	bool erase(const VINT& n) { return remove(n); }
	bool erase(iterator& pos) { return erase_impl(data.begin() + pos.state.section, pos.state.idx); };
	void clear() { data.clear(); _size=0; };
	
	domain_store::iterator begin() const { return iterator(&data); };
	domain_store::iterator end() const { return iterator(&data,-1); };
	domain_store::iterator cbegin() const { return begin(); };
	domain_store::iterator cend() const { return end(); };
	template <class T>
	domain_store::iterator find(const VINT& value, T&& predicate) const;
	bool empty() const { return _size==0; }
	size_t size() const { return _size; };
	void dump() {
		for (const auto& s : data) {
			cout << s.start << "+" << s.length << " | ";  
		}
	}
	
private:
	friend class iterator;
	bool erase_impl (deque<StrideData>::iterator pos, int i);
};

template <class T>
domain_store::iterator domain_store::find(const VINT& value, T&& predicate) const {
	auto row_value = value; row_value.x=0;
	iterator::StrideState pos;
	for (pos.section=0; pos.section<data.size(); pos.section++) {
		row_value.x = data[pos.section].start.x;
		if (predicate(row_value, data[pos.section].start)) {
			auto data_value = data[pos.section].start;
			for (pos.idx=0; pos.idx<data[pos.section].length; pos.idx++, data_value.x++) {
				if (predicate(value, data_value)) {
					return iterator(&data,pos);
				}
				
			}
		}
	}
	return end();
}
