#include "cpm_sampler.h"
#include "interaction_energy.h"
// #include <thread>
// #include <boost/thread.hpp>

REGISTER_PLUGIN(CPMSampler);

CPMSampler::CPMSampler() :
	ContinuousProcessPlugin(MCS,XMLSpec::XML_NONE)
{
	mcs_duration.setXMLPath("MonteCarloSampler/MCSDuration/value");
	registerPluginParameter(mcs_duration);
	// mcs_duration_symbol.setXMLPath("MonteCarloSampler/MCSDuration/symbol");
	// registerPluginParameter(mcs_duration_symbol);
	
	stepper_type.setXMLPath("MonteCarloSampler/stepper");
	map<string, StepperType> stepper_map;
	stepper_map["edgelist"] = StepperType::EDGELIST;
	stepper_map["random"] = StepperType::RANDOM;
	stepper_type.setConversionMap(stepper_map);
	registerPluginParameter(stepper_type);
	
	metropolis_temperature.setXMLPath("MonteCarloSampler/MetropolisKinetics/temperature");
	registerPluginParameter(metropolis_temperature);
	
	metropolis_yield.setXMLPath("MonteCarloSampler/MetropolisKinetics/yield");
	metropolis_yield.setDefault("0");
	metropolis_yield.setLocalsTable({{"dir",EvaluatorCache::LocalSymbolDesc::VECTOR}});
	registerPluginParameter(metropolis_yield);
};


void CPMSampler::loadFromXML(const XMLNode node, Scope* scope)
{
    ContinuousProcessPlugin::loadFromXML(node, scope);
	
	// if (mcs_duration_symbol.isDefined()) {
	// 	auto symbol = SymbolAccessorBase<double>::createConstant(mcs_duration_symbol.stringVal(),"Monte Carlo Step Duration", mcs_duration());
	// 	scope->registerSymbol(symbol);
	// }
	
	interaction_energy = shared_ptr<InteractionEnergy>(new InteractionEnergy());
	interaction_energy->loadFromXML(node.getChildNode("Interaction"), scope);
}

void CPMSampler::init(const Scope* scope)
{
	if (update_neighborhood.empty()) {
		initUpdateNeighborhood();
	}
	
	ContinuousProcessPlugin::init(scope);
	if ( ! mcs_duration.flags().time_const ||  !mcs_duration.flags().space_const) {
		throw MorpheusException("MonteCarloSampler/MCSDuration is required to be constant in time and space.", stored_node);
	}
	
	setTimeStep(mcs_duration(SymbolFocus::global));
	is_adjustable = false;
	
	cell_layer = CPM::getLayer();
	registerCellPositionOutput();
	registerOutputSymbol(scope->findSymbol<double>(SymbolBase::CellVolume_symbol,true));
	registerOutputSymbol(scope->findSymbol<double>(SymbolBase::CellSurface_symbol,true));
	registerOutputSymbol(scope->findSymbol<double>(SymbolBase::CellLength_symbol,true));
	registerOutputSymbol(scope->findSymbol<double>(SymbolBase::CellType_symbol,true));
	if (stepper_type() == StepperType::EDGELIST) {
		CPM::enableEgdeTracking();
	}
	else if (stepper_type() == StepperType::RANDOM) {
		// That's the default case;
	}
	
	edge_tracker = CPM::cellEdgeTracker();
	// get the size of the lock box around updates
	interaction_energy->init(scope);
	interaction_size=0;
	for (auto neighbor : getInteractionNeighborhood().neighbors()) {
		 interaction_size = abs(neighbor.x) > interaction_size ? abs(neighbor.x) : interaction_size;
		 interaction_size = abs(neighbor.y) > interaction_size ? abs(neighbor.y) : interaction_size;
		 interaction_size = abs(neighbor.z) > interaction_size ? abs(neighbor.z) : interaction_size;
	}
	int update_size=0;
	for (auto neighbor : getUpdateNeighborhood().neighbors()) {
		 update_size = abs(neighbor.x) > update_size ? abs(neighbor.x) : update_size;
		 update_size = abs(neighbor.y) > update_size ? abs(neighbor.y) : update_size;
		 update_size = abs(neighbor.z) > update_size ? abs(neighbor.z) : update_size;
	}
	
	interaction_size = max(interaction_size, 2*update_size);
	registerInputSymbols( interaction_energy->getDependSymbols() );
	
	if (metropolis_temperature.granularity() != Granularity::Global) {
		throw MorpheusException(string("Metropolis temperature is required to be homogeneous in space."),stored_node.getChildNode("MonteCarloSampler").getChildNode("MetropolisKinetics"));
	}
	

	auto weak_celltypes = CPM::getCellTypes();
	for (auto wct : weak_celltypes) {
		auto ct = wct.lock();
		celltypes.push_back(ct);
		auto dependencies =  ct->cpmDependSymbols();
		for (auto& dep : dependencies) {
			registerInputSymbol( dep.second );
		}
	}
}


void CPMSampler::initUpdateNeighborhood(){
	// In fact, the update neighborhood should include all neighbors up to sqrt(2), aka Moore for square, 1st order for hex and 2nd order in cubic.
	XMLNode xNeighborhood = stored_node.getChildNode("MonteCarloSampler").getChildNode("Neighborhood");
	if (!xNeighborhood.isEmpty()) {
		std::map<Lattice::Structure, int> default_neigbhborhoods {
			 {Lattice::linear, 1},
			 {Lattice::square, 2},
			 {Lattice::hexagonal, 1},
			 {Lattice::hexagonal_new, 1},
			 {Lattice::cubic, 2}
		};
		update_neighborhood = SIM::lattice().getNeighborhood(xNeighborhood, { {"optimal", default_neigbhborhoods[SIM::lattice().getStructure()]} } );
	}
	else throw string("Missing required element MonteCarloSampler/Neighborhood.");
	
	if (update_neighborhood.distance() > 3 || (SIM::lattice().getStructure()==Lattice::hexagonal && update_neighborhood.order()>5) ) {
		throw string("Update neighborhood is too large");
	}
}

vector<multimap< Plugin*, SymbolDependency > > CPMSampler::getCellTypeDependencies() const
{
	vector<multimap< Plugin*, SymbolDependency > > res;
	auto weak_celltypes = CPM::getCellTypes();
	for (auto wct : weak_celltypes) {
		auto ct = wct.lock();
		res.push_back(ct->cpmDependSymbols() );
	}
	return res;
}

set< SymbolDependency > CPMSampler::getInteractionDependencies() const 
{
	return interaction_energy->getDependSymbols();
}

const Neighborhood& CPMSampler::getInteractionNeighborhood()
{
	return interaction_energy->getNeighborhood();
}

const Neighborhood& CPMSampler::getUpdateNeighborhood()
{
	if (update_neighborhood.empty()) {
		initUpdateNeighborhood();
	}
	return update_neighborhood;
}

void CPMSampler::executeTimeStep()
{
	MonteCarloStep();
	if (stepper_type() == StepperType::EDGELIST) {
		static_pointer_cast<const EdgeListTracker>(edge_tracker)->house_keeping();
	}
}

bool CPMSampler::getUpdate(CPM::Update& update, update_stats& mcs_stats) {
	uint tries = 0;
	auto t = TP::teamNum();
	while (1) {
		VINT direction;
		VINT source_pos(0,0,0);
		const Cell* source_cell;
		const Cell* focus_cell;

		edge_tracker->get_update(source_pos, direction);
		
		// Assert update source selection is correct
		// Boundary::Type bt = Boundary::none;
		// if ( ! cell_layer->writable_resolve(source_pos,bt))
		// 	if (bt == Boundary::noflux) continue;
		
		// try to lock the update 
		SymbolFocus tmp(source_pos);
		source_cell = &tmp.cell();
		auto focus_pos = source_pos + direction;
		cell_layer->resolve(focus_pos);
		
		// Assert update focus selection is writable
		// if (!cell_layer->resolve(focus_pos) || !cell_layer->writable(focus_pos)) continue ;
		
		tmp.setPosition(focus_pos);
		focus_cell = &tmp.cell();
		// Assert that the boundary was not yet removed
		if (source_cell->getID() == focus_cell->getID()) return false;
		// Lock the cells
		while (1)
		{
			if (! source_cell->isNodeTracking()) {
				if (focus_cell->isNodeTracking() )
					focus_cell->lock();
			}
			else if (! focus_cell->isNodeTracking() ) {
				source_cell->lock();
			}
			else {
				source_cell->lock();
				if ( ! focus_cell->try_lock()) {
					// swap lock order
					source_cell->unlock(); 
					mcs_stats.cell_retries++;
					focus_cell->lock();
					if ( ! source_cell->try_lock()) { 
						focus_cell->unlock(); 
						mcs_stats.cell_retries++;
						std::this_thread::yield();
						continue;
					}
				}
			}
			break;
		}
			
		// try to lock the region
		while(1)
		{
			const auto& lattice = cell_layer->lattice();
			const auto& size = lattice.size();
			
			bool overlap = false;
			region_lock_mtx.lock();
			
			for (auto i=0; i< region_locks.size(); i++) {
				if (region_locks[i].active) {
					auto d = focus_pos - region_locks[i].pos;
					if (lattice.get_boundary_type(Boundary::px) == Boundary::periodic && (abs(2*d.x) > size.x)) {
						d.x= pmod(d.x, size.x);
						d.x -= (d.x > size.x - d.x) ? size.x : 0;
					}
					if (abs(d.x)>interaction_size) continue;
					
					if (lattice.get_boundary_type(Boundary::py) == Boundary::periodic && (abs(2*d.y) > size.y)) {
						d.y = pmod(d.y, size.y);
						d.y -= (d.y > size.y - d.y) ? size.y : 0;
					}
					if (abs(d.y)>interaction_size) continue;
					
					if (lattice.get_boundary_type(Boundary::pz) == Boundary::periodic && (abs(2*d.z) > size.z)) {
						d.z  = pmod(d.z, size.z);
						d.z -= (d.z > size.z - d.z) ? size.z : 0;
					}
					if (abs(d.z)>interaction_size) continue;
					overlap = true;
					break;
				}
			}
			if (!overlap) {
				region_locks[t].pos = focus_pos;
				region_locks[t].active = true;
				region_lock_mtx.unlock();
				break;
			}
			region_lock_mtx.unlock();
			mcs_stats.region_retries++;
			std::this_thread::yield();
		}

		// Assert that the cells still occupy the nodes 
		update.set(source_pos, direction, CPM::Update::Operation::Extend);
		if ( update.source().cellID() == source_cell->getID() && update.focus().cellID() == focus_cell->getID() ) {
			break;
		}
		else {
			if (source_cell->isNodeTracking())
				source_cell->unlock();
			if (focus_cell->isNodeTracking() )
				focus_cell->unlock();
			region_locks[t].active = false;
			
			if (++tries > 100) {
				cout << "discarded update ";
				return false;
			}
		}
	}
	return true;
}

void CPMSampler::MonteCarloStep() 
{
	assert(edge_tracker != NULL);
	const uint nupdates = edge_tracker->updates_per_mcs();
	update_stats mcs_stats;
	
	bool is_random = dynamic_pointer_cast<const NoEdgeTracker>(edge_tracker) != nullptr;
	cached_temp = metropolis_temperature.get(SymbolFocus::global);
	ExceptionCatcher exception_catcher;
	
	region_locks.resize( TP::CPMteamSize() );

#ifdef CPM_OMP_LOCKS
#pragma omp parallel shared(mcs_stats) num_threads(TP::CPMteamSize())
	{
	CPM::Update update(CPM::getGlobalUpdate().clone());
	update_stats mcs_stats_prv;
#pragma omp for schedule(static) nowait
	for (uint i=0; i < nupdates; ++i) {
		// an update is actually a copy operation of a value at source to position source + direction
		// in "cpm language" source + direction is the "focus" (W. de Back et. al.)
		if (exception_catcher.hasException()) continue;
		if ( ! this->getUpdate(update, mcs_stats_prv) )  continue;   // << this may also block you for a while
		
		if ( update.focus().cellID() != update.focusUpdated().cellID()) {
			exception_catcher.Run([&]() {
				mcs_stats_prv.count++;
				CPM::setUpdate(update);
				
				// and we check whether the update should take place
				if ( evalCPMUpdate(update) ) {
					mcs_stats_prv.success += CPM::executeCPMUpdate(update);
				}
			});
		}
		if (update.focusUpdated().cell().isNodeTracking())
			update.focusUpdated().cell().unlock();
		if (update.focus().cell().isNodeTracking() )
			update.focus().cell().unlock();
		region_locks[omp_get_thread_num()].active = false;
	}
#pragma omp critical(reduc)
	mcs_stats+= mcs_stats_prv;
	}
	
	if (exception_catcher.hasException()) {
		cerr << "Caught exception in CPMSampler::MonteCarloStep \n";
		cerr << "Region lock state \n";
		int t=0;
		for (const auto& lock : region_locks) {
			cerr << "T["<<t<<"] active: " << (lock.active ? "true" : "false") << " pos " << lock.pos << endl; 
			t++;
		}
		
		exception_catcher.Rethrow();
	}
	
#else //CPM_OMP_LOCKS
	auto job_count = min ( TP::getThreadPool().size(), CellType::storage.size()/4 + 1 );
	vector<int> update_counts;
	auto updates_left = nupdates;
	for (uint i=0; i<job_count; i++ ) {
		update_counts.push_back(updates_left/(job_count-i));
		updates_left-=update_counts.back();
	}
	int main_counts = update_counts.back();
	update_counts.pop_back();
	
	auto task = [&](const int count) -> update_stats {
			CPM::Update update(CPM::getGlobalUpdate().clone());
			update_stats mcs_stats_prv;

			auto team_id = TP::teamNum();
			try {
				for (uint i=0; i < count; ++i) {
					if ( ! this->getUpdate(update, mcs_stats_prv) )  continue;   // << may also block you for a while
					if ( update.focus().cellID() != update.focusUpdated().cellID()) {
						mcs_stats_prv.count++;
						CPM::setUpdate(update);
						
						// and we check whether the update should take place
						if ( evalCPMUpdate(update) ) {
							mcs_stats_prv.success += CPM::executeCPMUpdate(update);
						}
					
					}
					if (update.focusUpdated().cell().isNodeTracking())
						update.focusUpdated().cell().unlock();
					if (update.focus().cell().isNodeTracking() )
						update.focus().cell().unlock();
					region_locks[team_id].active = false;
				}
			}
			catch (...) { 
				if (update.focusUpdated().cell().isNodeTracking())
					update.focusUpdated().cell().unlock();
				if (update.focus().cell().isNodeTracking() )
					update.focus().cell().unlock();
				region_locks[team_id].active = false;
				throw ;
			}
			return mcs_stats_prv;
		};
	
	auto stats = TP::getThreadPool().parallel_submit( task, update_counts );
	
	try {
		// run on task in the main thread;
		mcs_stats = task(main_counts);
	
		for (auto& stat : stats) {
			stat.wait();
			mcs_stats += stat.get();
		}
	}
	catch (...) {
		cerr << "Caught exception in CPMSampler::MonteCarloStep \n";
		cerr << "Region lock state \n";
		int t=0;
		for (const auto& lock : region_locks) {
			cerr << "T["<<t<<"] active: " << (lock.active ? "true" : "false") << " pos " << lock.pos << endl; 
			t++;
		}
		
		std::rethrow_exception(std::current_exception());
	}
	

#endif
	MCS_counter++;
// 	cout << nupdates << " to run: " << mcs_stats.success << " succeeded " << mcs_stats.cell_retries << " cell retries " << mcs_stats.region_retries << "region retries." << endl;
}

bool CPMSampler::evalCPMUpdate(const CPM::Update& update)
{
	double dE=0, dInteraction=0, dCell=0;
	
	// Find the proper celltype to notify
	uint source_ct = update.source().celltype();
	uint focus_ct =  update.focus().celltype();
	
	if ( focus_ct == source_ct ) {
		if ( ! celltypes[focus_ct] -> check_update(update) )
			return false;
	}
	else {
		if ( ! celltypes[source_ct] -> check_update(update.selectOp(CPM::Update::ADD)) )
			return false;
		if ( ! celltypes[focus_ct] -> check_update( update.selectOp(CPM::Update::REMOVE)) )
			return false;
	}
	
	// InteractionEnergy
	dInteraction = interaction_energy -> delta(update);
	// CellType dependend energies
	if ( focus_ct == source_ct ) {
		dCell += celltypes[source_ct] -> delta(update);
	}
	else {
		dCell += celltypes[source_ct] -> delta( update.selectOp(CPM::Update::ADD));
		dCell += celltypes[focus_ct] -> delta( update.selectOp(CPM::Update::REMOVE));
		// TODO crawl through the neighborhood for CPM::Update::Neighborhood energy changes, if any CPMEnergy requires that
	}
	// the magic Metropolis Kinetics with Boltzmann probability ...
	VDOUBLE dir = update.focus().pos() - update.source().pos();
	metropolis_yield.setLocals(&dir.x);
	dE = dInteraction + dCell + metropolis_yield(update.focus()); //metropolis_yield(update.focus);

	if (dE <= 0)
		return true;
	
	double p = exp(-dE / cached_temp);
	double rnd = getRandom01();

	return rnd < p;
}

