#include "membranemapper.h"
#include "cell.h"

const double MembraneMapper::no_distance = 999999;

MembraneMapper::Data::Data() {
	const bool spherical = true;
	std::shared_ptr< const Lattice > membrane_lattice = MembraneProperty::lattice();
	data_map = make_shared<PDE_Layer>(membrane_lattice,1,spherical);
	accum_map = make_shared<PDE_Layer>(membrane_lattice,1,spherical);
	distance_map  = make_shared<PDE_Layer>(membrane_lattice,1,spherical);
	distance_map->set_boundary_value(Boundary::my,no_distance);
	distance_map->set_boundary_value(Boundary::py,no_distance);
	valid_cell = false;
	cell_id = CPM::NO_CELL;
	cell_shape = NULL;
}

MembraneMapper::MembraneMapper( MembraneMapper::MODE mode, InterpolationMode interpolation_mode, bool use_cell_shape ) :  use_shape(use_cell_shape), interpolation_mode(interpolation_mode) , mapping_mode(mode)
{
	membrane_lattice = MembraneProperty::lattice();
	global_lattice = SIM::getLattice();
	if (mode == MembraneMapper::MAP_DISCRETE || mode == MembraneMapper::MAP_BOOLEAN ) {
		if (interpolation_mode == InterpolationMode::AVERAGE)
			interpolation_mode = InterpolationMode::MAJORITY;
	}
	else if (mapping_mode == MAP_DISTANCE_TRANSFORM) {
		interpolation_mode = InterpolationMode::AVERAGE;
	}
	data.resize(omp_get_max_threads());
}

void MembraneMapper::attachToCell ( CPM::CELL_ID cell_id, int slot )
{
	auto& d = data[slot];
	d.cell_center = CPM::getCell(cell_id).getCenter();
	d.cell_id = cell_id;
	d.valid_cell = true;
	d.data_map->data.fill(0);
	d.accum_map->data.fill(0);
	d.distance_map->data.fill(no_distance);

	if (use_shape)
		d.cell_shape = & (CPM::getCell(cell_id).currentShape().sphericalApprox());
// 	if (mapping_mode == MAP_DISTANCE_TRANSFORM)
// 		shape_mapper->attachToCell(cell_id);
}

void MembraneMapper::attachToCenter ( VDOUBLE center, int slot )
{
	auto& d = data[slot];
	d.cell_id = CPM::NO_CELL;
	d.valid_cell = true;
	d.cell_center = center;

	d.data_map->data.fill(0);
	d.accum_map->data.fill(0);
	d.distance_map->data.fill(no_distance);
}


void MembraneMapper::map (const VINT& pos, double v, int slot)
{
	assert(data[slot].valid_cell);
	map(global_lattice->to_orth(pos),v, slot);
}

void MembraneMapper::map ( const VDOUBLE& pos, double v, int slot)
{
	const VINT membrane_pos = getMembranePosition( pos );
	set(membrane_pos,v,slot);
}

VINT MembraneMapper::getMembranePosition( const VDOUBLE& pos_3D, int slot)
{
	const auto& d = data[slot];
	assert(d.valid_cell);
	VDOUBLE orientation = global_lattice->orth_distance(pos_3D, d.cell_center);
	
	
	if ( !d.rotation_matrices.empty() ) {
		for (const auto& matrix : d.rotation_matrices) {
			//cout << "Rotation " << i << ", orientation: " <<  orientation << "\n";
			Eigen::Vector3f vector;
			vector << orientation.x, orientation.y, orientation.z;
			vector = matrix * vector;
			orientation = VDOUBLE( vector(0), vector(1), vector(2) );
		}
		//cout << "Rotation end, orientation: " <<  orientation << "\n";
	} 
	
	return MembraneProperty::orientationToMemPos(orientation);
}

void MembraneMapper::setRotationMatrix( Eigen::Matrix3f rot_matrix, int slot ){
	data[slot].rotation_matrices.push_back(rot_matrix);
}

void MembraneMapper::resetRotationMatrices(int slot) {
	data[slot].rotation_matrices.clear();
}

void MembraneMapper::set ( const VINT& membrane_pos, double v, int slot)
{
	assert( membrane_pos.x >= 0); 
	assert( membrane_pos.y >= 0 ); 
	assert( membrane_pos.x < data_map->l_size.x );
	assert( membrane_pos.y < data_map->l_size.y );
	
	const auto& d = data[slot];
	if (interpolation_mode == InterpolationMode::MAXIMUM) {
		if (d.accum_map->unsafe_getRef(membrane_pos)) {
			d.data_map->unsafe_getRef(membrane_pos) = d.data_map->unsafe_get(membrane_pos) > v ? d.data_map->unsafe_get(membrane_pos) : v;
		}
		else {
			d.accum_map->unsafe_getRef(membrane_pos) = 1;
			d.data_map->unsafe_getRef(membrane_pos) = v;
		}
	}
	else if (interpolation_mode == InterpolationMode::MINIMUM) {
		if (d.accum_map->unsafe_getRef(membrane_pos)) {
			d.data_map->unsafe_getRef(membrane_pos) = d.data_map->unsafe_get(membrane_pos) < v ? d.data_map->unsafe_get(membrane_pos) : v;
		}
		else {
			d.accum_map->unsafe_getRef(membrane_pos) = 1;
			d.data_map->unsafe_getRef(membrane_pos) = v;
		}
	}
	else if (mapping_mode == MAP_DISCRETE) {
		d.data_map->unsafe_getRef(membrane_pos) = v;
		d.accum_map->unsafe_getRef(membrane_pos) = 1;
	}
	else {
		d.data_map->unsafe_getRef(membrane_pos) += v;
		d.accum_map->unsafe_getRef(membrane_pos) += 1;
	}
	d.distance_map->unsafe_getRef(membrane_pos) = 0;
	
}

void MembraneMapper::fillGaps(int slot)
{
	const auto& d = data[slot];
	auto ii_data = xt::flatten( d.data_map->data );
	auto ii_distance = xt::flatten( d.distance_map->data );
	auto ii_accum = xt::flatten( d.accum_map->data );

	// Normalize the information in the lattice ...
	for (uint i=0; i<ii_data.size(); i++ ) {
		if ( (ii_accum[i] != 0.0) &&  (ii_accum[i] != 1.0) ) {
			if (mapping_mode == MAP_BOOLEAN || mapping_mode == MAP_DISTANCE_TRANSFORM) {
				if (ii_data[i] / ii_accum[i] != 0.5)
					ii_data[i] = ii_data[i] / ii_accum[i] > 0.5;
				else
					ii_data[i] =  getRandomBool();
			}
			else {
				ii_data[i] /= ii_accum[i];
			}
			ii_accum[i] = 1.0;
		}
	}
	
	d.data_map->reset_boundaries();
	d.distance_map->reset_boundaries();

	if (membrane_lattice->getDimensions() == 1) {
		// Spread the information ...
		bool done = false;
		bool fwd = true;
		int iterations = 0;
		while (! done ) {
			done = true;
			iterations++;
			int iter = fwd ? 1 : -1;
			int start_idx = fwd ? d.data_map->origin.x : d.data_map->top.x-1;
			int stop_idx = fwd ? d.data_map->top.x : d.data_map->origin.x-1;

			for ( int idx=start_idx; idx!=stop_idx; idx+=iter ) {
				// Evaluate the distance of the source of that pixel value
				if ((ii_distance[idx-1] + 1 < ii_distance[idx]) || (ii_distance[idx+1] + 1 < ii_distance[idx])) {
					if (ii_distance[idx-1] < ii_distance[idx+1]) {
						// select left
						ii_data[idx] = ii_data[idx-1];
						ii_distance[idx] = ii_distance[idx-1] + 1;
					}
					else if (ii_distance[idx-1] > ii_distance[idx+1]) {
						// select right
						ii_data[idx] = ii_data[idx+1];
						ii_distance[idx] = ii_distance[idx+1] + 1;
					}
					else {
						// interpolate
						if (mapping_mode == MAP_DISTANCE_TRANSFORM ) {
							if (ii_data[idx-1] + ii_data[idx+1] == 1.0)
								ii_data[idx] = getRandomBool();
							else
								ii_data[idx] = (ii_data[idx-1] + ii_data[idx+1]) > 1.0;
						}
						else if (interpolation_mode == InterpolationMode::MAXIMUM) {
							ii_data[idx] = ii_data[idx-1] > ii_data[idx+1] ? ii_data[idx-1] : ii_data[idx+1];
						}
						else if (interpolation_mode == InterpolationMode::MINIMUM) {
							ii_data[idx] = ii_data[idx-1] < ii_data[idx+1] ? ii_data[idx-1] : ii_data[idx+1];
						}
						else if (interpolation_mode == InterpolationMode::MAJORITY) {
							if (ii_data[idx-1] == ii_data[idx+1]) {
								ii_data[idx] =  ii_data[idx-1];
							}
							else {
								ii_data[idx] =  getRandomBool() ? ii_data[idx-1] : ii_data[idx+1];
							}
						}
						else { // interpolation_mode == InterpolationMode::AVERAGE
							ii_data[idx] = 0.5 * (ii_data[idx-1] + ii_data[idx+1]);
						}
						ii_distance[idx] = ii_distance[idx+1] + 1;
					}
					done = false;
				}
			}
			d.data_map->reset_boundaries();
			d.distance_map->reset_boundaries();
			fwd = ! fwd;
		}
	}
	else if (membrane_lattice->getDimensions() == 2) {
		int done = 1;
		bool empty = true;
		bool fwd = true;
		int iterations=0;
		vector<VINT> neighbors = membrane_lattice->getNeighborhoodByOrder(2).neighbors();
		vector<int> neighbor_offsets(neighbors.size());
		vector<double> neighbor_distance(neighbors.size());
		for (uint i=0; i< neighbors.size(); i++ ) {
			neighbor_offsets[i] = dot(neighbors[i], d.data_map->shadow_offset);
		}
		
		
		while ( done!=0 ) {
			done = 0;
			iterations++;

			VINT length(membrane_lattice->size());
			VINT start( fwd ? 0 :  length.x-1,  fwd ? 0 : length.y-1, 0);
			VINT iter( fwd ? +1 : -1, fwd ? +1 : -1, 0);
			VINT stop(fwd ? membrane_lattice->size() : VINT(-1,-1,0));
			VINT pos = start;
			
			for ( pos.y = start.y; pos.y != stop.y; pos.y+=iter.y) {

				VDOUBLE node_dist(1.0,1.0,0);
				if (pos.y==0 || pos.y==length.y-1)
					node_dist.x=0;  // set x_distance of polar volume elements to zero
				else {
					double theta_y = (double(pos.y)+0.5) / double(length.y) * M_PI;
					node_dist.x = sin(theta_y);
				}
				// precompute the distance of the neighbors within the spherical lattice
				for (uint i=0; i<neighbors.size(); i++) {
					neighbor_distance[i] = (neighbors[i]*node_dist).abs();
				}
				pos.x=start.x;
				int idx = d.data_map->get_data_index(pos);
				for ( ; pos.x != stop.x; pos.x+=iter.x, idx += iter.x) {

					double min_dist = ii_distance[idx];
					double acc_val=0;
					double acc=0;
					if (min_dist==0) {
						empty = false;
					}

					for (uint i=0; i<neighbors.size(); i++) {
						double dist = ii_distance[idx+neighbor_offsets[i]] + neighbor_distance[i];
						if (dist <= min_dist) {
							if (dist == min_dist) {
								acc++;
								acc_val += ii_data[idx+neighbor_offsets[i]];
							}
							else {
								acc = 1;
								acc_val = ii_data[idx+neighbor_offsets[i]];
								min_dist = dist;
							}

						}
					}

					if (min_dist < ii_distance[idx] - 10e-6) {
						if (mapping_mode == MAP_DISCRETE) {
							if (acc>1) {
								vector<double> buffer;
								for (uint i=0; i<neighbors.size(); i++) {
									if (min_dist == ii_distance[idx+neighbor_offsets[i]] + neighbor_distance[i]) {
										buffer.push_back(ii_data[idx+neighbor_offsets[i]]);
										acc +=1;
									}
								}
								ii_data[idx] = buffer[getRandomUint(buffer.size()-1)];
							}
							else
								ii_data[idx] = acc_val;
						}
						else if (mapping_mode == MAP_BOOLEAN || mapping_mode == MAP_DISTANCE_TRANSFORM ) {
							if (acc_val / acc == 0.5)
								ii_data[idx] = getRandomBool();
							else
								ii_data[idx] = (acc_val/acc > 0.5);
						}
						else { // mapping_mode == MAP_CONTINUOUS
								ii_data[idx] = acc_val / acc;
						}
						ii_distance[idx] = min_dist;
						done += 1;
					}
				}
			}
			fwd = ! fwd;
			d.data_map->reset_boundaries();
			d.distance_map->reset_boundaries();
			if (empty) {
				cout << "Cannot flood fill empty field" << endl;
				break;
			}
		}
// 		cout << iterations << " | " << endl;
	}

	if (mapping_mode == MAP_DISTANCE_TRANSFORM) {
		ComputeDistance();
	}
}


void MembraneMapper::ComputeDistance(int slot)
{
	const auto& d = data[slot];
	auto& ii_data = d.data_map->data;
	auto& ii_distance = d.distance_map->data;
	auto& ii_accum = d.accum_map->data;

// 	cout  << "ComputeDistance: sum " << ii_data.sum() << "\n";
	auto& ii_shape = d.cell_shape->data;
	
	double cell_spherical_radius = pow( CPM::getCell( d.cell_id ).nNodes() / ((4.0/3.0) * M_PI) , 1.0/3.0); // used to scale unit sphere to cell volume
	
	// Reset distances for no contact nodes and set 0 distance for contact nodes after imputation ...
	for (uint i=0; i<ii_data.size(); i++ ) {
		if ( ii_data[i] == 0.0 )
			ii_distance[i] = no_distance;
		else{
			ii_distance[i] = 0.0;
		}
	}
	
	d.distance_map->reset_boundaries();

	if (membrane_lattice->getDimensions() == 1) {
		bool done = false;
		bool fwd = true;
		int iterations = 0;
		while (! done ) {
			done = true;
			iterations++;
			int length = membrane_lattice->size().x;
			VINT start = fwd ? VINT(0,0,0) : VINT(length-1, 0,0);
			int iter = fwd ? 1 : -1;

			int idx = d.distance_map->get_data_index(start);
			for ( int step=0; step < length; step++, idx+=iter ) {
				double d_left = ii_distance[idx-1] + sqrt(sqr(1 / membrane_lattice->size().x * min(ii_shape[idx], ii_shape[idx-1]))  + sqr(ii_shape[idx]-ii_shape[idx-1]) );
				double d_right = ii_distance[idx+1] + sqrt(sqr(1 / membrane_lattice->size().x  * min(ii_shape[idx], ii_shape[idx+1]))+ sqr(ii_shape[idx]-ii_shape[idx+1]) );
				if (( d_left < ii_distance[idx]) || (d_right < ii_distance[idx])) {
					if (ii_distance[idx-1] < ii_distance[idx+1]) {
						ii_distance[idx] = d_left;
					}
					else {
						ii_distance[idx] = d_right;
					}
					done = false;
				}
			}
			d.distance_map->reset_boundaries();
			fwd = ! fwd;
		}
	}
	else if (membrane_lattice->getDimensions() == 2) {
		bool done = false;
// 		bool fwd = true;
		uint dir = 0;
		int iterations=0;
		vector<VINT> neighbors = membrane_lattice->getNeighborhoodByOrder(2).neighbors();
		vector<int> neighbor_offsets(neighbors.size());
		vector<double> neighbor_distance(neighbors.size());
		for (uint i=0; i< neighbors.size(); i++ ) {
			neighbor_offsets[i] = dot(neighbors[i], d.distance_map->shadow_offset);
		}
		while (! done ) {
			done = true;
			iterations++;
			bool x_fwd = (dir == 0 || dir == 3);
			bool y_fwd = (dir == 0 || dir == 2);
			VINT length(membrane_lattice->size());
			VINT pos( x_fwd ? 0 :  length.x-1,  y_fwd ? 0 : length.y-1, 0);
			VINT iter( x_fwd ? +1 : -1, y_fwd ? +1 : -1, 0);

			for ( int y_step = 0; y_step<length.y; y_step++, pos.y+=iter.y) {

				// precompute the node distance in a unit sphere
				VDOUBLE node_dist(1.0,1.0,0);
				if (pos.y==0 || pos.y==length.y-1)
					node_dist.x=0;  // set x_distance of polar volume elements to zero
				else {
					double theta_y = (double(pos.y)+0.5) / double(length.y) * M_PI;
					node_dist.x = sin(theta_y);
				}
				// precompute the distance of the neighbors within the spherical lattice
				for (uint i=0; i<neighbors.size(); i++) {
					neighbor_distance[i] = sqrt( sqr(neighbors[i].x * node_dist.x) + sqr(neighbors[i].y * node_dist.y) ) * (2.0 * M_PI / double(membrane_lattice->size().x)) ;
				}

				int idx = d.distance_map->get_data_index(pos);
				for ( int x_steps=0; x_steps < length.x; x_steps++ , idx+=iter.x ) {
					double min_dist = ii_distance[idx];
					for (uint i=0; i<neighbors.size(); i++) {
						double dist = ii_distance[idx+neighbor_offsets[i]];
						if( dist == no_distance)
						  continue;
						
						dist += (use_shape ? 
							  sqrt( sqr(neighbor_distance[i] * min(ii_shape[idx], ii_shape[idx+neighbor_offsets[i]]) ) + sqr(ii_shape[idx] - ii_shape[idx+neighbor_offsets[i]])) 
							  : neighbor_distance[i] * cell_spherical_radius );
						if (dist < min_dist) {
							min_dist = dist;
						}
					}

					if (min_dist - ii_distance[idx] < -10e-6) {
						ii_distance[idx] = min_dist;
						done = false;
					}
				}
			}
// 			if(cell_id == 39183 || cell_id == 33664){
// 				ofstream d;
// 				d.open(string("distance_") + to_str(iterations) + ".log", ios_base::trunc);
// 				distance_map->write_ascii(d);
// 				d.close();
// 			}
// 			fwd = ! fwd;
			dir = (dir + 1) % 4;
// 				distance_map;
// 			cout << endl << endl;
			d.distance_map->reset_boundaries();
// 			if (iterations == 8) break;
		}
// 		cout << iterations << " | " << "\t";

		
	}

	
}


const PDE_Layer& MembraneMapper::getData(int slot) {
	if (mapping_mode == MAP_DISTANCE_TRANSFORM)
		return getDistance();
	return *data[slot].data_map.get();
}

void MembraneMapper::copyData(PDE_Layer* membrane, int slot) {
	const auto& d = data[slot];
	assert(d.valid_cell);
	assert(d.membrane->data.size() == data_map->data.size());
	if (mapping_mode == MAP_DISTANCE_TRANSFORM)
		copyDistance(membrane, slot);
	else
		membrane->data = d.data_map->data;
}

const PDE_Layer& MembraneMapper::getAccum(int slot)
{
	return *data[slot].accum_map.get();
}


void MembraneMapper::copyAccum(PDE_Layer* membrane, int slot) {
	const auto& d = data[slot];
	assert(d.valid_cell);
	assert(d.membrane->data.size() == accum_map->data.size());
// 	if (mapping_mode == MAP_DISTANCE_TRANSFORM)
// 		shape_mapper->copyData(membrane);
// 	else
	membrane->data = d.accum_map->data;
}

const PDE_Layer& MembraneMapper::getDistance(int slot)
{
	return *data[slot].distance_map.get();
}


void MembraneMapper::copyDistance(PDE_Layer* membrane, int slot) {
	const auto& d = data[slot];
	assert(d.valid_cell);
	assert(d.membrane->data.size() == distance_map->data.size());
	if( xt::amin(d.distance_map->data)() == 999999)
		membrane->data.fill(0.0);
	else
		membrane->data = d.distance_map->data;
}


