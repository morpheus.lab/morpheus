//////
//
// This file is part of the modelling and simulation framework 'Morpheus',
// and is made available under the terms of the BSD 3-clause license (see LICENSE
// file that comes with the distribution or https://opensource.org/licenses/BSD-3-Clause).
//
// Authors:  Joern Starruss and Walter de Back
// Copyright 2009-2016, Technische Universität Dresden, Germany
//
//////

#ifndef CPM_SAMPLER_H
#define CPM_SAMPLER_H

#include <map>
#include <list>
#include "interfaces.h"
#include "simulation.h"
#include "plugin_parameter.h"
#include "edge_tracker.h"
#include "interaction_energy.h"

/**
\defgroup ML_CPM CPM
\ingroup MorpheusML ContinuousProcessPlugins

Specifies parameters for a cellular Potts model (CPM) representation of cells. Temporal simulation is provided by a MonteCarlo sampler that evolves the spatial cell configuration on the basis of statistical sampling of the defined Hamiltonian landscape and Metropolis kinetics acceptance rate.

Basic Hamiltonian using \ref ML_Interaction and \ref ML_VolumeConstraint :

\f$ H = \sum J_{\sigma, \sigma '} (1-\delta_{\sigma, \sigma '}) + \lambda_V \sum (v_\sigma-V_\sigma)^2 \f$

Additional contributions can be defined under \ref ML_CellType .

\ref ML_Interaction specifies interaction energies \f$ J_{\sigma, \sigma '} \f$ for different inter-cellular \ref ML_Contact. The interaction energy is given per length unit as defined in \ref ML_ShapeSurface. \ref ML_MonteCarloSampler specifies the temporal simulation parameters.

\section References

 -# Graner and Glazier, Phys Rev Lett, 1992
 -# Käfer, Hogeweg and Marée, PLoS Comp Biol, 2006
 -# Magno, Grieneisen and Marée, BMC Biophysics, 2015

\defgroup ML_ShapeSurface ShapeSurface
\ingroup ML_CPM

\b ShapeSurface specifies the Neighborhood used to estimate the boundary length of CPM Shapes, in particular cells. This estimate is used for computing interaction energies, cell perimeters and interface lengths.
  - \b scaling scaling of number of neighbors to length :
    - \b norm estimate the length in unit of node length (see Magno, Grieneisen and Marée, BMC Biophysics, 2015),
	- \b none estimate the length as the number of neighbor nodes occupied by other entities,
	- \b size estimate the length as the neigborhood fraction occupied by other entities,
	- \b classic estimate surface length by unscaled 1st order neighborhood - as used by CompuCell3D - and contact energies unscaled by the given neighborhood.
	
  - \b Neigborhood defines the stencil size to approximate the surface length. Wrt. shape isotropy some neighborhoods are favourable on a given lattice and are represented by the neighborhood named \b optimal : 
    - square  -- 4th order corresponding to a distance of \f$ \sqrt 5 \f$ 
    - hexagonal -- 4rd order, corresponding to a distance of \f$ \sqrt 7 \f$
    - cubic  -- 6th order  corresponding to a distance of \f$ \sqrt 6 \f$ 

\ref ML_Interaction specifies interaction energies \f$ J_{\sigma, \sigma '} \f$ for different inter-cellular \ref ML_Contact. The interaction energy is given per length unit as defined in ShapeSurface.

\defgroup ML_MonteCarloSampler MonteCarloSampler
\ingroup ML_CPM

\b MonteCarloSampler specifies all parameters for the the temporal simulation of the CPM.

The Metropolis kinetics acceptance probability is defined as

\f$ P (\Delta H) = \text{min}( exp(- (\Delta H + E_{yield}) / T) , 1 ) \f$

\f$ P (\Delta H) = \left\{ \begin{array}{lr}
	1, & if \; (\Delta H + E_{yield} < 0) \\
	exp(- (\Delta H + E_{yield}) / T) & otherwise \\
	\end{array} \right.
\f$

  - \b stepper: 
    - \b edgelist chooses updates from a tracked list of lattice sites that can potentially change configuration
    - \b random sampling chooses a lattice site with uniform random distribution over all lattice sites.
  - \b MetropolisKinetics:
    - \b temperature: specifies Boltzmann probability used to accept updates that increase energy. Required to be homogeneous in space.
    - \b yield: offset for Boltzmann probability distribution representing resistance to membrane deformations (see Kafer, Hogeweg and Maree, PLoS Comp Biol, 2006).
  - \b Neighborhood specifies the neighborhood size used for choosing updates in the modified Metropolis algorithm. The named neighborhood \b optimal represents 1st order for linear and hexagonal lattices, and 2nd order for square and cubic lattice.
  - \b MCSDuration scales the Monte Carlo Step (MCS) to global model time (a value of 10 will distribute one MCS over 10 model time units). One MCS is defined as a number of update attempts equal to the number of lattice sites.

  
**/


class CPMSampler : public ContinuousProcessPlugin {
public:
	DECLARE_PLUGIN("CPM");
	CPMSampler();
	
    virtual void loadFromXML(const XMLNode node, Scope* scope) override;
	double MCSDuration() { mcs_duration.init(); return mcs_duration(SymbolFocus::global); }
	virtual void prepareTimeStep(double /*step_size*/) override {};
	virtual void executeTimeStep() override ;
	
    virtual void init(const Scope* scope) override;
	void initUpdateNeighborhood();
	const Neighborhood& getInteractionNeighborhood();
	const Neighborhood& getUpdateNeighborhood();
	vector< multimap< Plugin*, SymbolDependency > > getCellTypeDependencies() const;
	set< SymbolDependency > getInteractionDependencies() const;
	
private:
	enum class StepperType { EDGELIST, RANDOM };
	PluginParameter2<double,XMLThreadsafeEvaluator,RequiredPolicy> mcs_duration;
	// PluginParameter2<string,XMLValueReader,OptionalPolicy> mcs_duration_symbol;
	PluginParameter2<StepperType,XMLNamedValueReader,RequiredPolicy> stepper_type;
	PluginParameter2<double,XMLThreadsafeEvaluator,RequiredPolicy> metropolis_temperature;
	PluginParameter2<double,XMLThreadsafeEvaluator,DefaultValPolicy> metropolis_yield;
	
// 	CPM::UPDATE current_update;
	mutable Neighborhood update_neighborhood;
	int interaction_size=0;
	shared_ptr<const EdgeTrackerBase> edge_tracker;
	shared_ptr<InteractionEnergy> interaction_energy;
	
	///  Run one MonteCarloStep, i.e. as many updates as determined by the mcs stepper
	void MonteCarloStep();
	uint MCS_counter = 0;
public:
	struct update_stats {
		uint count=0;
		uint success=0;
		uint cell_retries=0;
		uint region_retries=0;
		update_stats operator+ (const CPMSampler::update_stats& other ) { return CPMSampler::update_stats({count+other.count, success+other.success, cell_retries+other.cell_retries, region_retries+other.region_retries}); }
		update_stats& operator+= (const CPMSampler::update_stats& other ) { count+=other.count; success+=other.success; cell_retries+=other.cell_retries; region_retries+=other.region_retries; return *this; }
	};
private:
	bool getUpdate(CPM::Update& update, update_stats& mcs_stats);
	bool evalCPMUpdate(const CPM::Update& update);
	
	shared_ptr<const CPM::LAYER> cell_layer;
	vector <std::shared_ptr <const CellType > > celltypes;
	mutable double cached_temp;
	struct region_lock_data {
		VINT pos;
		bool active = false;
	};
	mutable vector<region_lock_data> region_locks;
	CPM::mutex region_lock_mtx;
	
// 	std::list<CPM::UpdateSelection> update_post_stack;
};

#endif
