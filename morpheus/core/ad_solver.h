#ifndef AD_SOLVER_H
#define AD_SOLVER_H

#include "interfaces.h"
#include "field.h"
#include "membrane_property.h"

enum class AD_Solver {
	WellMixed,
	FwdEuler,
	Implicit
};

/** Descriptor for spatial operators on a spatially continuous Symbol.
 *  \b field_type encodes the class of field beyond the \b field_symbol
 */

struct AD_Descriptor
{
	/// XML specified diffusion rate, may be constant in space and/or time
	PluginParameter_Shared<double, XMLThreadsafeEvaluator, OptionalPolicy> diffusion;
	/// XML specified advection velocity, may be constant in space and/or time, no assumptions are made on the quality of the velocity field
	PluginParameter_Shared<VDOUBLE, XMLThreadsafeEvaluator, OptionalPolicy> advection;
	
	/// Accessor to the field symbol
	shared_ptr<const FieldAccessor<double>> field_symbol;
	/// Type of field behind the field_symbol
	enum FieldType { FIELD, MEMBRANE, INTRACELL } field_type;
	AD_Solver ad_solver;
	
	// Data types of the field, retrieved from field_symbol->getField()  ...
	using scalar_tensor = PDE_Layer::value_tensor;
	using boundary_tensor = PDE_Layer::boundary_tensor;
	using ValueReader = PDE_Layer::ValueReader;
	
	/// extends of the data container underlying the field
	VINT origin, top, size;   /// \b origin is the data offset discarding shadow regions, \b top the index of the upper corner of the data, and \b size the extends of the data range (i.e. without the shadow)

	// Boundary definitions
	/** if a domain is used, the \b boundary_mask has all the boundary layout, 
	 *  else \b boundary_types and \b boundary_values specify the respective settings
	 */
	bool has_domain_and_mask; 
	vector<Boundary::Type> boundary_types; // boundary_type: const, const flux, periodic ; boundary={Boundary::mx,Boundary::px, ... }
	vector< shared_ptr<ValueReader> > boundary_values;
	const boundary_tensor *boundary_mask;
};  

class AD_Solver_I {
public:
	enum ErrorCode {
		NoError=0, TimeStepLimitExceeded
	};
	
	virtual ~AD_Solver_I() {};
	
	virtual void setNodeLength(double value) =0;
	virtual double maxTimeStepHint() const =0;
	virtual void setADBoundaries(AD_Descriptor::scalar_tensor *x0) const =0;
	/** Compute a time step for the field identified by the symbol of the descriptor and the given \p focus.
	 * 
	 *  The actual data is given in \p x0 and the result shall be written to \p x1. 
	 *  All required additional data is provided by the descriptor and the fields behind the field_symbol.
	 */ 
	virtual int computeTimeStep(SymbolFocus focus, double time_step, AD_Descriptor::scalar_tensor *x0, AD_Descriptor::scalar_tensor *x1) =0; // can be succesful (return 0) or unsuccessful (return error code)   // fragt sich ob setTimeStep noetig ist
	    
};


class AD_Solver_Base : public AD_Solver_I {
public:
	AD_Solver_Base(const AD_Descriptor &descr) : d(descr) {};
	void setADBoundaries(AD_Descriptor::scalar_tensor *x0) const final;
	static shared_ptr<AD_Solver_I> createInstance(const AD_Descriptor& descr);
protected:
	const AD_Descriptor d;
};

#endif
