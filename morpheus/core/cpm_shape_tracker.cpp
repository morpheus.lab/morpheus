#include "cpm_shape_tracker.h"
#include "eigen/Eigen/Eigenvalues"
#include "membranemapper.h"
#include "simulation.h"

/*

 * CPMShapeTracker is the coordinating wrapper to keep current shape and updated shape in synch, according to the 
 * the updates set/applied
 * AdaptiveCPMShapeTracker is the tracker backend that just tracks what is requested from the frontend
 * it needs functionality to initialize a tracker and to apply minimal updates
 */ 

/**
 * We require the current Lattice in order to provide everything in orthogonal coordinates
 */


AdaptiveCPMShapeTracker::AdaptiveCPMShapeTracker(CPM::CELL_ID cell, const CPMShape::Nodes& cell_nodes)
	: lattice(SIM::lattice()), cell_id(cell), _nodes(cell_nodes)
{
	// boundary_scaling = CPMShape::BoundaryLengthScaling( boundary_stencil, CPMShape::BoundaryScalingMode::Magno);
	boundary_scaling = CPMShape::BoundaryLengthScaling(
		CPMShape::scalingMode == CPMShape::BoundaryScalingMode::Classic ? 
			SIM::lattice().getNeighborhoodByOrder(1) : 
			CPMShape::boundaryNeighborhood(),
		CPMShape::scalingMode
	);
	if (lattice.getDimensions() == 1)
		Ell_I.resize(1,0.0);
	else if (lattice.getDimensions() == 2)
		Ell_I.resize(3,0.0);
	else if (lattice.getDimensions() == 3)
		Ell_I.resize(6,0.0);
	
	reset();
}

void AdaptiveCPMShapeTracker::reset() {
	tracking = { false, false, false, false, false, false };
	tracker_valid = { false, false, false, false, false, false };
	overlay_node.active = false;
	initNodes();
	updates_per_interface_request = 1e9;
	updates_per_ellipsoid_request = 1e9;
}

const CPMShape::Nodes& AdaptiveCPMShapeTracker::surfaceNodes() const
{
	if (tracker_valid.surface) {
		return surface_nodes;
	}
// 	else  if (n_updates - last_surface_update < 3 * (10 + _nodes.size()) ) { 
// 		tracking.surface = true;
// 	}
	CPM::guard lock(mutex);
	initSurfaceNodes();
	return surface_nodes;
}



const map<CPM::CELL_ID,double>&  AdaptiveCPMShapeTracker::interfaces() const 
{
	if (! tracker_valid.scaled_interfaces) {
		CPM::guard lock(mutex);
		if (tracker_valid.scaled_interfaces)
			return scaled_interfaces;
		if ( ! tracker_valid.interfaces) {
			initInterfaces();
			if (updates_per_interface_request < 2 * (10 + _nodes.size())) {
				tracking.interfaces=true;
// 				cout << "Switching on interface tracking for cell " << cell_id << " after " << updates_per_interface_request << " < " << 2 * (10 + _nodes.size())  << " updates."<< endl;
			}
		}
// 		CPM::guard lock(mutex);
		scaled_interfaces.clear();
		for (const auto& i : _interfaces) {
			if (i.second)
				scaled_interfaces.insert(scaled_interfaces.end(), make_pair(i.first,double(i.second) / boundary_scaling) );
		}
		tracker_valid.scaled_interfaces = true;
	}
	updates_per_interface_request = 0;
	return scaled_interfaces;
};

const EllipsoidShape& AdaptiveCPMShapeTracker::ellipsoidApprox() const {
	if (! tracker_valid.elliptic_approx) {
		CPM::guard lock(mutex);
		if (tracker_valid.elliptic_approx)
			return ellipsoid_approx;
		if (updates_per_ellipsoid_request < 100 ) {
// 			cout << "Switching on ellipsoid tracking for cell " << cell_id << " after " << n_updates - last_ellipsoid_update << " updates."<< endl;
			tracking.elliptic_approx = true;
		}
		initEllipsoidShape();
	}
	updates_per_ellipsoid_request = 0;
	return ellipsoid_approx;
}
	
const PDE_Layer& AdaptiveCPMShapeTracker::sphericalApprox() const { 
	if (tracker_valid.spherical_approx)
		return spherical_mapper->getData();
	
	CPM::guard lock(mutex);
	if (!spherical_mapper) {
		spherical_mapper = new MembraneMapper(MembraneMapper::MAP_CONTINUOUS);
	}

	// report radius for all surface nodes
// 	cout << "creating sphericalApprox around center " << center()  << endl;
	VDOUBLE c = center();
	spherical_mapper->attachToCenter(c);
	
	for (const auto& pt  : _nodes)
	{
		if (CPM::isSurface(pt)) {
			double distance = lattice.orth_distance(c, lattice.to_orth(pt)).abs();
			spherical_mapper->map(pt,distance);
		}
	}
	// interpolate and return
	spherical_mapper->fillGaps();
 	//spherical_mapper->flatten();
	tracker_valid.spherical_approx = true;
	return spherical_mapper->getData();
} 

void AdaptiveCPMShapeTracker::reset(AdaptiveCPMShapeTracker* other) {
	
	assert(other->cell_id == cell_id);
// 	assert(other->n_updates<n_updates);
	
	CPM::guard lock(mutex);
	node_sum_L = other->node_sum_L;
	node_sum = other->node_sum;
	node_count = other->node_count;
	_interface_length = other->_interface_length;
	overlay_node.active = false;
	tracker_valid = { true, false, false, false, false, false };
	
	if (tracking.interfaces && other->tracker_valid.interfaces) {
		bool brute_force_copy = true;
		if (other->_interfaces.size() == _interfaces.size()) {
			auto ui = other->_interfaces.begin();
			auto i = _interfaces.begin();
			for (; ui != other->_interfaces.end(); ++ui,++i) {
				if (ui->first != i->first) {
					break;
				}
				i->second = ui->second;
			}
			brute_force_copy = false;
		}
		
		if (brute_force_copy) { 
			_interfaces = other->_interfaces;
		}
		tracker_valid.interfaces = true;
	}
	
	if ( other->tracker_valid.elliptic_approx) {
		ellipsoid_approx = other->ellipsoid_approx;
		Ell_I = other->Ell_I;
		tracker_valid.elliptic_approx = true;
	}
	
	if ( other->tracker_valid.spherical_approx) {
		this->spherical_mapper = other->spherical_mapper;
		tracker_valid.spherical_approx = true;;
	}
}

void AdaptiveCPMShapeTracker::setOverlay(const CPM::Update& update) {
	if (overlay_node.active) { 
		throw string("AdaptiveCPMShapeTracker:: You need to reset before setting an overlay ");
		overlay_node.active = false;
	}
		
	if (update.opAdd()) {
		overlay_node = { true, true, update.focusUpdated().cellPos() };
	}
	else if (update.opRemove()) {
		overlay_node = { true, false, update.focus().cellPos() };
	}
	else {
		overlay_node.active = false;
	}
	do_apply(update); 
}

void AdaptiveCPMShapeTracker::unsetOverlay() {
	overlay_node.active = false;
}


void AdaptiveCPMShapeTracker::apply(const CPM::Update& update) {
	if (overlay_node.active) { 
		throw string("AdaptiveCPMShapeTracker:: You need to reset before setting an overlay ");
		overlay_node.active = false;
	}
	do_apply(update);
}


void AdaptiveCPMShapeTracker::do_apply(const CPM::Update& update) {
// Node tracking
	if (update.opNeighborhood() && ! tracking.interfaces) {
		tracker_valid.interfaces = false;
		tracker_valid.scaled_interfaces = false;
		return;
	}
	CPM::guard lock(mutex);
	if (update.opAdd()) {
		tracker_valid = { false, false, false, false, false, false };
		node_sum_L += update.focusUpdated().cellPos();
		node_sum   += lattice.to_orth(update.focusUpdated().cellPos());
		node_count++;
		tracker_valid.nodes = true;
	}
	else if (update.opRemove() ) {
		tracker_valid = { false, false, false, false, false, false };
		node_sum_L -= update.focus().cellPos();
		node_sum   -= lattice.to_orth(update.focus().cellPos());
		node_count--;
		tracker_valid.nodes = true;
	}
	else {
		tracker_valid.interfaces = false;
		tracker_valid.scaled_interfaces = false;
	}
	
// Interface tracking
	if (tracking.interfaces) {
		trackInterfaces(update);
	}
	else {
		// track total interface
		auto boundaryStencil = update.boundaryStencil();
		if (CPMShape::scalingMode == CPMShape::BoundaryScalingMode::Classic) {
			boundary_stencil()->setPosition(update.focus().pos());
			boundaryStencil = boundary_stencil();
		}
		if (update.opAdd()) {
			// only track the total surface
			for (const auto& stat : boundaryStencil->getStatistics()) {
				_interface_length += (stat.cell == cell_id) ? - double(stat.count) : double(stat.count);
			}
		}
		else if (update.opRemove()) {
			for (const auto& stat : boundaryStencil->getStatistics()) {
				_interface_length += (stat.cell == cell_id) ? double(stat.count) : - double(stat.count);
			}
		}
	}
	updates_per_interface_request++;
	
	if (!update.opNeighborhood()) {
		if (tracking.surface) {
			assert(0);
		}
		if (tracking.elliptic_approx) {
			trackEllipse(update.opAdd() ? update.focusUpdated().cellPos() : update.focus().cellPos(), update.opAdd() - update.opRemove());
		}
		updates_per_ellipsoid_request++;
	}

}

void AdaptiveCPMShapeTracker::apply(const CPM::Update& update, AdaptiveCPMShapeTracker* other)
{
	if (other->cell_id != cell_id)
		assert(0);
	
	CPM::guard lock(mutex);
	if (overlay_node.active) { 
		throw string("AdaptiveCPMShapeTracker:: You need to reset before setting an overlay ");
		overlay_node.active = false;
	}
	tracker_valid = { false, false, false, false, false, false };
	// cellPos may have been fixed meanwhile (extremely rare case ...)
	if (update.opAdd()) {
		node_sum_L += update.focusUpdated().cellPos();
		node_sum   += lattice.to_orth( update.focusUpdated().cellPos() );
		node_count++;
	}
	else if (update.opRemove()) {
		node_sum_L -= update.focus().cellPos();
		node_sum   -= lattice.to_orth( update.focus().cellPos() );
		node_count--;
	}
	tracker_valid.nodes = true;
	
	if ( other->tracker_valid.interfaces ) {
		// copy tracking from cache
		bool brute_force_copy = true;
		if (other->_interfaces.size() == _interfaces.size()) {
			auto ui = other->_interfaces.begin();
			auto i = _interfaces.begin();
			for (; ui != other->_interfaces.end(); ++ui,++i) {
				if (ui->first != i->first) {
					break;
				}
				i->second = ui->second;
			}
			brute_force_copy = false;
		}

		if (brute_force_copy) { 
			_interfaces = other->_interfaces;
		}
		_interface_length = other->_interface_length;
		tracker_valid.interfaces = true;
	}
	else if (tracking.interfaces) {
		// we have to track the interfaces
		trackInterfaces(update);
	}
	else {
		_interface_length = other->_interface_length;
	}
	
	if (!update.opNeighborhood()) {
		if (other->tracking.elliptic_approx) {
			ellipsoid_approx = other->ellipsoid_approx;
			Ell_I = other->Ell_I;
			tracker_valid.elliptic_approx = true;
		}
		else if (tracking.elliptic_approx) {
			trackEllipse(update.opAdd() ? update.focusUpdated().cellPos() : update.focus().cellPos(), int(update.opAdd()) - int(update.opRemove()));
		}
	}
	
	updates_per_interface_request++;
	updates_per_ellipsoid_request++;
}


void AdaptiveCPMShapeTracker::trackInterfaces(const CPM::Update& update) const {

	auto boundaryStencil = update.boundaryStencil();
	if (CPMShape::scalingMode == CPMShape::BoundaryScalingMode::Classic) {
		boundary_stencil()->setPosition(update.focus().pos());
		boundaryStencil = boundary_stencil();
	}
	
	if (update.opNeighborhood()) {
		for (const auto& stat : boundaryStencil->getStatistics()) {
			if (stat.cell == cell_id) {
				if ( (_interfaces[update.focus().cellID()] -= stat.count) == 0) {
					_interfaces.erase(update.focus().cellID());
				}
				_interfaces[update.focusUpdated().cellID()] += stat.count;
			}
		}
	}
	else if (update.opAdd()) {
		for (const auto& stat : boundaryStencil->getStatistics()) {
			if (stat.cell == cell_id) {
				 if ( (_interfaces[update.focus().cellID()] -= stat.count) == 0 ) {
					 _interfaces.erase( update.focus().cellID() );
				 }
				_interface_length -= stat.count;
			}
			else {
				_interfaces[stat.cell] += stat.count;
				_interface_length += stat.count;
			}
		}
	}
	else if (update.opRemove()) {
		for (const auto& stat : boundaryStencil->getStatistics()) {
			if (stat.cell == cell_id) {
				_interfaces[update.focusUpdated().cellID()] += stat.count;
				_interface_length += stat.count;
			}
			else {
				if ( (_interfaces[stat.cell] -= stat.count) == 0 ) {
					 _interfaces.erase( stat.cell );
				 };
				_interface_length -= stat.count;
			}
		}
	}
	tracker_valid.interfaces = true;
	tracker_valid.scaled_interfaces = false;
}

void AdaptiveCPMShapeTracker::trackEllipse(const VINT pos, int dir) {
	VDOUBLE delta,deltacom;
	if (ellipsoid_updates > 1e4) {
		initEllipsoidShape();
	}
	else  {
// 		cout << "TrackEllipse " << ellipsoid_approx.lengths[0];
		if (dir!=0)
		{
			double updated_node_count = node_count;
			VDOUBLE updated_center = center();
			double old_node_count = node_count - dir;
			auto orth_pos = lattice.to_orth(pos);
			VDOUBLE old_center = (node_sum - dir *orth_pos) / old_node_count;
			
			deltacom = old_center - updated_center;
			delta =  updated_center - orth_pos;
	// 			cout << " p  " << update.add_state.pos 
	// 				 << " cu " << updated_center
	// 				 << " c  " << center
	// 				 << " dc " << deltacom
	// 				 << " d  " << delta << endl;
			if (lattice.getDimensions()==3){
	// old Iij with respect to new center of mass	(Huygens-Steiner)
				Ell_I[Ell_XX] += double(old_node_count)*(sqr(deltacom.y)+sqr(deltacom.z));
				Ell_I[Ell_YY] += double(old_node_count)*(sqr(deltacom.x)+sqr(deltacom.z));
				Ell_I[Ell_ZZ] += double(old_node_count)*(sqr(deltacom.x)+sqr(deltacom.y));
				Ell_I[Ell_XY] -= double(old_node_count)*deltacom.x*deltacom.y;
				Ell_I[Ell_XZ] -= double(old_node_count)*deltacom.x*deltacom.z;
				Ell_I[Ell_YZ] -= double(old_node_count)*deltacom.y*deltacom.z;
				// new Iij with respect to new com			
				Ell_I[Ell_XX] += double(dir)*(sqr(delta.y) + sqr(delta.z));
				Ell_I[Ell_YY] += double(dir)*(sqr(delta.x) + sqr(delta.z));
				Ell_I[Ell_ZZ] += double(dir)*(sqr(delta.x) + sqr(delta.y));
				Ell_I[Ell_XY] -= double(dir)*(delta.x*delta.y);
				Ell_I[Ell_XZ] -= double(dir)*(delta.x*delta.z);
				Ell_I[Ell_YZ] -= double(dir)*(delta.y*delta.z);
				ellipsoid_approx = computeEllipsoid3D(Ell_I, updated_node_count) ;
			}                                                    
			else if(lattice.getDimensions()==2){            
	// old Iij with respect to new center of mass	(Huygens-Steiner)		
	//                 for(int i=0;i<3;i++) printf("I[%i]=%f ",i,I[i]); 
				Ell_I[Ell_XX] += double(old_node_count)*sqr(deltacom.y);
				Ell_I[Ell_YY] += double(old_node_count)*sqr(deltacom.x);
				Ell_I[Ell_XY] -= double(old_node_count)*deltacom.x*deltacom.y;
		//		 for(int i=0;i<3;i++) printf("temp_I[%i]=%f ",i,temp_I[i]); 
		// new Iij with respect to new com			
				Ell_I[Ell_XX] += double(dir)*sqr(delta.y);
				Ell_I[Ell_YY] += double(dir)*sqr(delta.x);
				Ell_I[Ell_XY] -= double(dir)*(delta.x*delta.y);
				ellipsoid_approx =  computeEllipsoid2D(Ell_I, updated_node_count) ;
			} else assert(0);
// 			cout << " -> " << ellipsoid_approx.lengths[0] << " n0 " << old_center <<  " n1 " << updated_center << endl;
			
			ellipsoid_approx.center = updated_center;
			ellipsoid_approx.volume = updated_node_count;
		}
		ellipsoid_updates++;
	}
	tracker_valid.elliptic_approx = true;
}
	
	
EllipsoidShape AdaptiveCPMShapeTracker::computeEllipsoid3D(const valarray<double> &I, int N) {
	EllipsoidShape es;
	if (N<2)
	{
		if (N==1) {
			es.lengths.push_back( 1 );
			es.lengths.push_back( 1 );
			es.axes.push_back(VDOUBLE());
			es.axes.push_back(VDOUBLE());
		}
		else {
			es.lengths.push_back( 0 );
			es.lengths.push_back( 0 );
		}
		es.axes.push_back(VDOUBLE());
		es.axes.push_back(VDOUBLE());
		es.eccentricity = 0;
		return es;
	}
	
	// From of the inertia tensor (principal moments of inertia) we compute the eigenvalues and
	// obtain the cell length by assuming the cell was an ellipsoid
	Eigen::Matrix3f eigen_m;
	eigen_m << I[Ell_XX], I[Ell_XY], I[Ell_XZ],
	           I[Ell_XY], I[Ell_YY], I[Ell_YZ],
	           I[Ell_XZ], I[Ell_YZ], I[Ell_ZZ];
	           
	Eigen::SelfAdjointEigenSolver<Eigen::Matrix3f> eigensolver(eigen_m);
	if (eigensolver.info() != Eigen::Success) {
		cerr << "Cell::calcLengthHelper3D: Computing eigenvalues was not successfull!" << endl;
	}

	Eigen::Vector3f eigen_values = eigensolver.eigenvalues();
	Eigen::Matrix3f EV = eigensolver.eigenvectors();
		
	Eigen::Matrix3f Am;
	Am << -1,  1,  1,
	       1, -1,  1,
	       1,  1, -1;
	Eigen::Array3f axis_lengths = 2.0*((Am * eigen_values).array() * (2.5/double(N))).sqrt();
	Eigen::Vector3i sorted_indices;
	for (uint i=0; i<3; i++) {
		if (isnan(axis_lengths(i))) axis_lengths(i)=0;
		es.lengths.push_back(axis_lengths(i));
		es.axes.push_back(VDOUBLE(EV(0,i),EV(1,i),EV(2,i)).norm());
	}
	// sort axes by length
	bool done=false;
	while (!done) {
		for (uint i=0;1;i++) {
			if (es.lengths[i] < es.lengths[i+1]) {
				swap(es.lengths[i],es.lengths[i+1]);
				swap(es.axes[i],es.axes[i+1]);
			}
			if (i==1) {
				done=true;
				break;
			}
		}
	}
	// due to symmetry
	if (es.axes[0].z<0) es.axes[0] *= -1;
	else if (es.axes[0].z==0 && es.axes[0].y < 0) es.axes[0] *= -1;
	if (es.axes[1].z<0) es.axes[1] *= -1;
	else if (es.axes[1].z==0 && es.axes[1].y < 0) es.axes[1] *= -1;
	if (es.axes[2].z<0) es.axes[2] *= -1;
	else if (es.axes[2].z==0 && es.axes[2].y < 0) es.axes[2] *= -1;
	return es;
};

EllipsoidShape AdaptiveCPMShapeTracker::computeEllipsoid2D(const valarray<double> &I, int N) {
	EllipsoidShape es;
	if (N<2){
		if (N==1) {
			es.lengths.push_back( 1 );
			es.lengths.push_back( 1 );
			es.axes.push_back(VDOUBLE());
			es.axes.push_back(VDOUBLE());
			es.eccentricity = 0;
			return es;
		}
		else {
			es.lengths.push_back( 0 );
			es.lengths.push_back( 0 );
			es.axes.push_back(VDOUBLE());
			es.axes.push_back(VDOUBLE());
			es.eccentricity = 0;
			return es;
		}
	}
	
	// From of the inertia tensor (principal moments of inertia) we compute the eigenvalues and
	// obtain the cell length by assuming the cell was an ellipsoid
	Eigen::Matrix2f eigen_m;
	eigen_m << I[Ell_XX], I[Ell_XY], 
			   I[Ell_XY], I[Ell_YY];

	Eigen::SelfAdjointEigenSolver<Eigen::Matrix2f> eigensolver(eigen_m);
	if (eigensolver.info() != Eigen::Success) {
		cerr << "Cell::calcLengthHelper2D: Computing eigenvalues was not successfull!" << endl;
	}

	Eigen::Vector2f eigen_values = eigensolver.eigenvalues();
	Eigen::Matrix2f EV = eigensolver.eigenvectors();	
	
	double l0 = (eigen_values(1))>0. ? 4*sqrt(eigen_values(1)/double(N)) : 0;
	double l1 = (eigen_values(0))>0. ? 4*sqrt(eigen_values(0)/double(N)) : 0;
	
	if (l0>l1)
	{
		es.lengths.push_back(l0);
		es.lengths.push_back(l1);
		es.axes.push_back(VDOUBLE(2.*EV(0,0),2.*EV(1,0),0).norm());
		es.axes.push_back(VDOUBLE(2.*EV(0,1),2.*EV(1,1),0).norm());
		es.eccentricity  = sqrt( 1 - (eigen_values(0) / eigen_values(1)) );
	} else
	{
		es.lengths.push_back(l1);
		es.lengths.push_back(l0);
		es.axes.push_back(VDOUBLE(2.*EV(0,1),2.*EV(1,1),0).norm());
		es.axes.push_back(VDOUBLE(2.*EV(0,0),2.*EV(1,0),0).norm());
		es.eccentricity  = sqrt( 1 - (eigen_values(1) / eigen_values(0)) );
	}
	// due to symmetry
	if (es.axes[0].y<0) es.axes[0] *= -1;
	if (es.axes[1].y<0) es.axes[1] *= -1;
	return es;
}

void AdaptiveCPMShapeTracker::initNodes() const {
	node_sum = node_sum_L = VINT(0,0,0);
	node_count = _nodes.size();
	_interface_length=0;
	if (_nodes.size()>0) {
		auto boundaryStencil = boundary_stencil();
		for (auto node : _nodes) {
			node_sum_L += node;
			node_sum += lattice.to_orth(node);
			lattice.resolve(node);
			boundaryStencil->setPosition(node);
			for (const auto& stat : boundaryStencil->getStatistics()) {
				if (stat.cell != cell_id) {
					_interface_length += stat.count;
				}
			}
		}
		tracker_valid.nodes = true;
	}
}

StatisticalLatticeStencil * AdaptiveCPMShapeTracker::boundary_stencil() const
{
	if (!d_boundary_stencil) {
		d_boundary_stencil = make_unique<StatisticalLatticeStencil>( CPM::getLayer(),
			CPMShape::scalingMode == CPMShape::BoundaryScalingMode::Classic ?
				CPM::getLayer()->lattice().getNeighborhoodByOrder(1) :
				CPMShape::boundaryNeighborhood()
		);
	}
	return d_boundary_stencil.get();
}


void AdaptiveCPMShapeTracker::initInterfaces() const {
	_interfaces.clear();
	_interface_length=0;

	auto boundaryStencil = boundary_stencil();
	if (node_count>0) {
		for (auto node : _nodes) {
			lattice.resolve(node);
			boundaryStencil->setPosition(node);
			for (const auto& stat : boundaryStencil->getStatistics()) {
				if (stat.cell != cell_id) {
					_interfaces[stat.cell] += stat.count;
					_interface_length += stat.count;
				}
			}
		}
		if (overlay_node.active) {
			auto node = overlay_node.pos;
			lattice.resolve(node);
			boundaryStencil->setPosition(node);
			for (const auto& stat : boundaryStencil->getStatistics()) {
				int f = overlay_node.add ? 1: -1;
				if (stat.cell != cell_id) {
					_interfaces[stat.cell] += f * stat.count;
					_interface_length += f * stat.count;
				}
			}
		}
	}
	tracker_valid.interfaces = true;
	tracker_valid.scaled_interfaces = false;
}

void AdaptiveCPMShapeTracker::initSurfaceNodes() const {
	surface_nodes.clear();
	for (const auto& node : _nodes) {
		if (CPM::isSurface(node)) {
			if (overlay_node.active && ! overlay_node.add && node == overlay_node.pos) continue;
			surface_nodes.push_back(node);
		}
	}
	if ( overlay_node.active && overlay_node.add && CPM::isSurface(overlay_node.pos) )
		surface_nodes.push_back(overlay_node.pos);
	tracker_valid.surface = true;
}

void AdaptiveCPMShapeTracker::initEllipsoidShape() const {
	Ell_I = 0;
	
	uint num_nodes = this->node_count;
	VDOUBLE c = center();

	if (lattice.getDimensions()==2){
		Ell_I[Ell_XX] = 0;
		Ell_I[Ell_YY] = 0;
		Ell_I[Ell_XY] = 0;
		for (const auto& node : _nodes)
		{
			VDOUBLE delta = c - lattice.to_orth(node);
			Ell_I[Ell_XX] += sqr(delta.y);
			Ell_I[Ell_YY] += sqr(delta.x);
			Ell_I[Ell_XY] += -delta.x*delta.y;  
		}
		// Just add the node which is missing in the nodes container
		if (overlay_node.active) {
			VDOUBLE delta = c - lattice.to_orth(overlay_node.pos);
			double f = overlay_node.add ? 1 : -1;
			Ell_I[Ell_XX] += f * sqr(delta.y);
			Ell_I[Ell_YY] += f * overlay_node.add;
			Ell_I[Ell_XY] += -f * delta.x*delta.y;
		}
		ellipsoid_approx = computeEllipsoid2D(Ell_I,num_nodes);
	} 
	else if (lattice.getDimensions()==3){
		for (const auto& node : _nodes)
		{
			VDOUBLE delta = c - lattice.to_orth(node);
			Ell_I[Ell_XX]+=  sqr(delta.y) + sqr(delta.z);
			Ell_I[Ell_YY]+=  sqr(delta.x) + sqr(delta.z);
			Ell_I[Ell_ZZ]+=  sqr(delta.x) + sqr(delta.y);
			Ell_I[Ell_XY]+= -delta.x*delta.y;
			Ell_I[Ell_XZ]+= -delta.x*delta.z;
			Ell_I[Ell_YZ]+= -delta.y*delta.z;
		}
		// Just add the node which is missing in the nodes container
		if (overlay_node.active) {
			VDOUBLE delta = c - lattice.to_orth(overlay_node.pos);
			double f = overlay_node.add ? 1 : -1;
			Ell_I[Ell_XX]+= f * sqr(delta.y)+sqr(delta.z) ;
			Ell_I[Ell_YY]+= f * sqr(delta.x)+sqr(delta.z) ;
			Ell_I[Ell_ZZ]+= f * sqr(delta.x)+sqr(delta.y) ;
			Ell_I[Ell_XY]+= -f * delta.x*delta.y ;
			Ell_I[Ell_XZ]+= -f * delta.x*delta.z ;
			Ell_I[Ell_YZ]+= -f * delta.y*delta.z ; 
		}
		ellipsoid_approx = computeEllipsoid3D(Ell_I,num_nodes);
	} 
	else assert(0);
	
	ellipsoid_approx.center = c;
	ellipsoid_approx.volume = num_nodes;
	ellipsoid_updates = 0;
	updates_per_ellipsoid_request = 0;
	tracker_valid.elliptic_approx = true;
}


CPMShapeTracker::CPMShapeTracker(CPM::CELL_ID cell_id, const CPMShape::Nodes& cell_nodes)
: updated_shape(cell_id, cell_nodes), current_shape(cell_id, cell_nodes) {
	updated_is_current = true;
};

void CPMShapeTracker::setUpdate(const CPM::Update& update) { 
	if ((update.opAdd() && update.opRemove()) || update.opNeighborhood()) return;
	
	if (!updated_is_current) {
		updated_shape.reset(&current_shape); 
	}
	updated_shape.setOverlay(update);
	updated_is_current = false;
	cache_tainted = false;
};

void CPMShapeTracker::reset()
{
	current_shape.reset();
	updated_shape.reset();
	cache_tainted = true;
	updated_is_current = false;
}


void CPMShapeTracker::applyUpdate(const CPM::Update& update, bool ignore_cache)
{
	if (update.opAdd() && update.opRemove()) return;

	ignore_cache = ignore_cache || cache_tainted || update.opNeighborhood();
	// In case this change is not tracked in 'other' or this has changed meanwhile
	if (ignore_cache) {
		// Neighborhood updates are not tracked in updated_shape so far
		current_shape.apply(update);
		updated_is_current = false;
	}
	else {
		current_shape.apply(update, &updated_shape);
		
		updated_shape.unsetOverlay();
		updated_is_current = true; 
	}
	// taint update cache;
	cache_tainted = true;
}
