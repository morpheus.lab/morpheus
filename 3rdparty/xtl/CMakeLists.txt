# include(FetchContent)

# FetchContent_Declare(xtl
#   PREFIX "xtl"
#   GIT_REPOSITORY https://github.com/QuantStack/xtl.git
#   GIT_TAG origin/master
# )
# FetchContent_MakeAvailable(xtl)
# FetchContent_GetProperties(xtl)
# SET(xtl_DIR ${xtl_BINARY_DIR} CACHE PATH "Path to xtl" FORCE)
# message(STATUS "xtl downloaded to ${xtl_BINARY_DIR}")


SET(XTL_INCLUDE_DIR ${CMAKE_BINARY_DIR}/3rdparty/include)
add_library(xtl INTERFACE)
target_include_directories(xtl INTERFACE ${XTL_INCLUDE_DIR})

IF(NOT EXISTS ${CMAKE_BINARY_DIR}/3rdparty/include/xtl/xtl_config.hpp)
	include(ExternalProject)
	SET(xtl_cmake_args -DCMAKE_INSTALL_PREFIX=${CMAKE_BINARY_DIR}/3rdparty )
	ExternalProject_Add( xtl-dl
		PREFIX ""
		GIT_REPOSITORY https://github.com/xtensor-stack/xtl.git
		GIT_TAG 0.7.5
		# Configuration paramenters
		CMAKE_ARGS ${xtl_cmake_args}
		EXCLUDE_FROM_ALL TRUE
	)
	add_dependencies(xtl xtl-dl)
	message(STATUS "xtl downloaded to ${XTL_INCLUDE_DIR}/xtl")
ENDIF()

