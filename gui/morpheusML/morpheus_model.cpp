#include "morpheus_model.h"
#include "config.h"
#include "morpheusML_rules.h"

const int MorphModel::NodeRole = Qt::UserRole + 1;
const int MorphModel::XPathRole = Qt::UserRole + 2;
const int MorphModel::TagsRole = Qt::UserRole + 3;

QList<QString> model_parts() {
	QList<QString> parts;
	parts << "Description" << "Space" << "Time" << "Global" << "CellTypes" << "CPM";
	parts << "CellPopulations" << "Analysis";
	return parts;
}
const QList<QString> MorphModelPart::all_parts_sorted = model_parts();

QMap<QString,int> model_part_indices() {
	QMap<QString,int> indices;
	auto parts = model_parts();
	for (uint i=0; i<parts.size(); i++) {
		indices[parts[i]] = i;
	}
	return indices;
}

const QMap<QString,int> MorphModelPart::all_parts_index = model_part_indices();

//------------------------------------------------------------------------------

MorphModel::MorphModel(QObject *parent) :
    QAbstractItemModel(parent)
{
	initModel(true);
}

//------------------------------------------------------------------------------

MorphModel::MorphModel(QString xmlFile, QObject *parent)
try : QAbstractItemModel(parent),  xml_file(xmlFile) 
{
	if (xml_file.xmlDocument.documentElement().nodeName() != "MorpheusModel")
        throw ModelException(ModelException::UndefinedNode, QString("File \"%1\" has wrong wrong root node '%2'.\nExpected 'MorpheusModel'!").arg(xmlFile,xml_file.xmlDocument.nodeName()));

    initModel();
}
catch (QString e) {
	throw ModelException(ModelException::FileIOError, e);
}

MorphModel::MorphModel(QDomDocument model, QObject *parent) :
QAbstractItemModel(parent), xml_file(model)
{
	if (xml_file.xmlDocument.documentElement().nodeName() != "MorpheusModel")
		throw ModelException(ModelException::UndefinedNode, QString("Imported Document has wrong wrong root node '%1'.\nExpected 'MorpheusModel'!").arg(xml_file.xmlDocument.documentElement().nodeName()));
	
	initModel();
}


MorphModel::MorphModel(QByteArray data, QObject *parent) :
QAbstractItemModel(parent), xml_file(data)
{
	if (xml_file.xmlDocument.documentElement().nodeName() != "MorpheusModel")
		throw ModelException(ModelException::UndefinedNode, QString("Document has wrong wrong root node '%1'.\nExpected 'MorpheusModel'!").arg(xml_file.xmlDocument.documentElement().nodeName()));
	
	initModel();
}

void MorphModel::initModel(bool from_scratch)
{
	
	QList<MorphModelEdit> edits;
	auto xml_doc = xml_file.xmlDocument;
	if (!from_scratch) {
		edits = applyAutoFixes(xml_doc);
		// Create XML node controller tree by creating a node for the xml document's root
		rootNodeContr = new nodeController(xml_doc.documentElement());
	}
	else {
		try {
			rootNodeContr = new nodeController(xml_doc.documentElement());
			rootNodeContr->attribute("version")->set(QString::number(morpheus_ml_version));
			// Now we clear the history of changes ...
			rootNodeContr->saved();
			rootNodeContr->clearTrackedChanges();
		}
		catch (QString& error) {
			qDebug() << "Error creating MorpheusModel from plain template ... ";
			qDebug() << error;
			rootNodeContr = nullptr;
			throw error;
		}
		
	}
	rootNodeContr->setParent(this);
	loadModelParts();
	
	sweep_lock = false;
	dep_graph_model_edit_stamp  = -1;
	param_sweep.setRandomSeedRoot(rootNodeContr->firstActiveChild("Time"));
	
	temp_folder = QDir::temp();
	QString name_patt = "morpheus_%1";
	int id = 0;
	QString name;
	do {
		name = name_patt.arg(QString().setNum(id));
		id ++;
		if (id>10000) break;
	} while (temp_folder.exists(name));
	temp_folder.mkpath(name);
	temp_folder.cd(name);
	
	

	if (!edits.empty()) {
		ModelDescriptor& desc = const_cast<ModelDescriptor&>(rootNodeContr->getModelDescr());
		for (int i=0; i<edits.size();i++) {
			desc.auto_fixes.append(edits[i]);
			desc.change_count++;
		}
		QMessageBox::information(qApp->activeWindow(),
			"Auto Fixes",
			QString("Your Morpheus model %2 was upgraded to the current version %1.\nSee the FixBoard for details.").arg(morpheus_ml_version).arg(xml_file.name) );
	}
}


MorphModel::~MorphModel()
{
	qDebug() << "Cleaning up model " << this->rootNodeContr->getModelDescr().title;
	qDebug() << "Cleaning up path " << temp_folder;
	if (temp_folder.exists()) {
		qDebug() << "Deleting " << temp_folder;
		removeDir(temp_folder.absolutePath());
	}
}


bool MorphModel::removeDir(QString dir_path)
{
	bool result = true;
	QDir dir (dir_path);
	if (dir.exists()) {
		auto dir_list = dir.entryInfoList(QDir::NoDotAndDotDot | QDir::System | QDir::Hidden  | QDir::AllDirs | QDir::Files, QDir::DirsFirst);
		for (const auto& info : dir_list) {
			if (info.isDir()) {
				result = removeDir(info.absoluteFilePath());
			}
			else {
				result = QFile::remove(info.absoluteFilePath());
			}

			if (!result) {
				return result;
			}
		}
		result = dir.rmdir(dir.absolutePath());
	}
	return result;
}

QString MorphModel::getDependencyGraph(GRAPH_TYPE type)
{
	if (!temp_folder.exists()) {
		temp_folder.mkpath(temp_folder.absolutePath());
		if (!temp_folder.exists()) {
			qDebug() << "Cannot create temporary path " << temp_folder.absolutePath();
			return "";
		}
	}
	
	QString ext_string;
	switch (type) {
		case PNG: ext_string="png"; break;
		case SVG: ext_string="svg"; break;
		case DOT: ext_string="dot"; break;
	}
	QString graph_file = "model_graph."+ext_string;
	QString model_graph = "ModelGraph."+ext_string;
	QString graph_file_fallback = "model_graph.dot";
	QString model_graph_fallback = "ModelGraph.dot";
	
	// If the model did not change since the last rendering, just take the old rendering.
	if (dep_graph_model_edit_stamp == rootNodeContr->getModelDescr().edits) {
		if (temp_folder.exists(model_graph)) {
			return temp_folder.absoluteFilePath(model_graph);
		}
		if (temp_folder.exists(model_graph_fallback)) {
			return temp_folder.absoluteFilePath(model_graph_fallback);
		}
	}
	else {
		temp_folder.remove(graph_file);
		temp_folder.remove(graph_file_fallback);
	}
	
	QString model_file = temp_folder.absoluteFilePath("model.xml.gz");

	auto system_files = this->rootNodeContr->getModelDescr().sys_file_refs;
	QList<QString> original_paths;
	QDir model_dir = QFileInfo(this -> xml_file.path).absoluteDir();
	
	// Convert file references into absolute paths
	for (uint i=0; i<system_files.size(); i++ ) {
		original_paths.push_back(system_files[i]->get());
		if (system_files[i]->isActive() && ! system_files[i]->isDisabled()) {
			QFileInfo file(model_dir, system_files[i]->get().trimmed());
			QString file_path = file.absoluteFilePath();
			qDebug() << file_path;
			
			if (file_path.startsWith(":/")) {
				// the file is in the resources, we need to copy it
				QFile::copy(file_path, temp_folder.absoluteFilePath(file.fileName()));
				system_files[i]->set(file.fileName());
			}
			else {
				system_files[i]->set(file_path);
			}
		}
	}
	
	//Run mopsi on it
	xml_file.save(model_file, true);
	
	// revert file ref absolute paths
	for (uint i=0; i<system_files.size(); i++ ) {
		system_files[i]->set(original_paths[i]);
	}
	
	QProcess process;
    QProcessEnvironment env = QProcessEnvironment::systemEnvironment();
    env.insert("OMP_NUM_THREADS", "1");
    process.setProcessEnvironment(env);

	QString command = config::getPathToExecutable( config::getApplication().local_executable );
    if (command.isEmpty()) {
		return "";
	}
	
	QStringList arguments;
	if ( ! config::getApplication().local_GnuPlot_executable.isEmpty() )
		arguments << "--gnuplot-path" << config::getApplication().local_GnuPlot_executable;
#ifdef Q_OS_WIN32
	else if ( ! config::getPathToExecutable("gnuplot").isEmpty())
		arguments << "--gnuplot-path" << config::getPathToExecutable("gnuplot");
#endif
	
	arguments << "--model-graph" << ext_string << model_file;

    process.setWorkingDirectory(temp_folder.absolutePath());
	process.setStandardOutputFile(temp_folder.absoluteFilePath("model.out"));

    // run morpheus
	process.start(command,arguments);
	if (! process.waitForFinished(30000)) {
		process.close();
		process.kill();
	}
	
	if (temp_folder.exists(graph_file)) {
		dep_graph_model_edit_stamp = rootNodeContr->getModelDescr().edits;
		temp_folder.remove(model_graph);
		temp_folder.rename(graph_file, model_graph);
		return temp_folder.absoluteFilePath(model_graph);
	}
	else if (temp_folder.exists(graph_file_fallback)){
		// Fallback in case there is no graphviz lib available
		dep_graph_model_edit_stamp = rootNodeContr->getModelDescr().edits;
		temp_folder.remove(model_graph_fallback);
		temp_folder.rename(graph_file_fallback, model_graph_fallback);
		return temp_folder.absoluteFilePath(model_graph_fallback);
	}
	qDebug() << "Expected graph file does not exist " << temp_folder.absoluteFilePath(graph_file);
	qDebug() << process.readAllStandardOutput();
	return "";
}

// struct Replacement { QDomElement parent; const QDomNode disabled_node; const QDomNode comment; };
// void replaceDisabled(QList<Replacement>& replacements, QDomElement element) {
// 	if (element.hasAttribute("internal:disabled") ) {
// 		qDebug() <<  element.nodeName() << "internal:disabled" << element.attributeNode("internal:disabled").nodeValue();
// 		 if (element.attributeNode("internal:disabled").nodeValue() == "true") {
// 			 // do the comment replacement
// 			auto parent = element.parentNode();
// 			// create the comment to replace the data node in the xml
// 			auto comment = element.ownerDocument().createComment("");
// 			auto disabledNode = element.ownerDocument().createElement("Disabled");
// 			// reparent the data node and store it into the comment
// 			disabledNode.appendChild(parent.replaceChild(comment, element));
// 			element.removeAttribute("internal:disabled");
// 			QString dis_node_text;
// 			QTextStream s(&dis_node_text);
// 			disabledNode.save(s,4);
// 			comment.setNodeValue(dis_node_text);
// 			element.setAttribute("internal:disabled","true");
// 			// store the replacement for rollback
// 			Replacement r {parent.toElement(), element, comment};
// 			replacements.append(r);
// 			return;
// 		 }
// 	}
// 	
// 	auto childs = element.childNodes();
// 	for (int i=0; i<childs.size(); i++ ) {
// 		if (childs.at(i).isElement())
// 			replaceDisabled(replacements, childs.at(i).toElement());
// 	}
// }

QByteArray MorphModel::getXMLText() const {
// 	rootNodeContr->synchDOM();
// 	auto xml_doc = xml_file.xmlDocument;
// 	QList<Replacement> replacements;
// 	replaceDisabled(replacements, xml_doc.documentElement());
// 	
// 	QByteArray data(xml_doc.toByteArray(4));
// 	
// 	// revert comment replacements
// 	for ( auto& r :replacements) {
// 		r.parent.replaceChild(r.disabled_node, r.comment).clear();
// 		// r.comment.clear();
// 	}
	return xml_file.domDocToText();
}


struct XPathMatch {
	QDomNode node;
	QString  wildcard;
	QStringList captures;
};


struct XCondition {
	QString attribute;
	QString value;
	bool capture;
	bool negate = false;
	bool regex = false;
};
using XConditionList = QList<XCondition>;

struct XPathElement {
	QString tag;
	XConditionList conditions;
};

typedef QList<XPathElement> XPath;

QList<XPathElement> parseXPath(QString path) {
	QStringList tags = path.remove(" ").split("/", Qt::SkipEmptyParts);
	QList<XPathElement> xpath;
	if (tags.isEmpty()) return xpath;
	QString xattribute;
	if (tags.last().startsWith("@")) {
		// Last denotes an attribute, not a tag
		xattribute = tags.takeLast();
	}
	for (auto tag : tags) {
		int bracket_open = tag.indexOf('[');
		QStringList raw_conditions;
		if (bracket_open>0) {
			if (tag[tag.size()-1] == ']') {
				raw_conditions = tag.mid(bracket_open+1, tag.length()-bracket_open-2).split(",",Qt::SkipEmptyParts);
				tag.truncate(bracket_open);
			}
			else {
				throw QString("Missing closing bracket in ") + tag;
			}
		}
		
		XConditionList conditions;
		for (auto c : raw_conditions) {
			if (c.isEmpty()) continue;

			XCondition cond;
			if ( c[0] == '(' && c[c.size()-1] == ')') {
				cond.capture = true;
				c.remove(0,1);
				c.remove(-1,1);
			}
			else { 
				cond.capture = false;
			}
			auto operator_match = QRegularExpression("!?=~?").match(c);
			if (operator_match.hasMatch() ) {
				 /// index 0 has the whole match
				auto o = operator_match.captured(0);
				cond.negate = o[0] == '!';
				cond.regex = o[o.size()-1] == '~';
				cond.attribute = c.left(operator_match.capturedStart(0));
				cond.value = c.right(c.length() - operator_match.capturedEnd(0));
			}
			else {
				cond.attribute = c;
			}
			if (cond.attribute[0] == '@')
				cond.attribute.remove(0,1);
			conditions.push_back(cond);
		}
		
		xpath.push_back( { tag, conditions });
	}
	
	if (!xattribute.isEmpty()) {
		xpath.push_back({ xattribute, {} });
	}
	
	return xpath;
}

QList<QDomElement> findNodeRecursive(QDomElement parent, QString tag,int max_depth = std::numeric_limits<int>::max() ) 
{ 
	QList<QDomElement> result;
	bool is_attribute = tag.startsWith("@");
	if (is_attribute) tag.remove(0,1);
	
	QList<QDomElement> candidates {parent};
	int current_depth_candidates = 1;
	int next_depth_candidates = 0;
	int current_depth=0;
	
	while (!candidates.isEmpty()) {
		auto cand = candidates.takeFirst();
		current_depth_candidates--;
		
		if (is_attribute) {
			if (current_depth != 0) {
				if (cand.attributes().contains(tag)) {
					result.push_back(cand);
				}
			}
		}
		
		auto childs = cand.childNodes();
		for (int i=0; i<childs.size(); i++) {
			if (!childs.item(i).isElement()) continue;

			auto child = childs.item(i).toElement();
			if (!is_attribute && child.nodeName() == tag) {
				result.push_back(child);
			}
			
			if ( current_depth < max_depth) {
				candidates.push_back(child);
				next_depth_candidates++;
			}
		}
		if (current_depth_candidates==0) {
			current_depth++;
			current_depth_candidates = next_depth_candidates;
			next_depth_candidates = 0;
		}
	}
	
	return result;
}

QList<XPathMatch> matchPath(QDomElement domNode, XPath search_tags, bool create = false) {
	
	QList<XPathMatch> sub_matches;
	if (search_tags.isEmpty()) {
		sub_matches.append({domNode, {}});
		return sub_matches;
	}
	sub_matches.append( { domNode, {} } );
// 	QString  search_plain_path = QString("/");
	
	while ( ! search_tags.empty() && ! sub_matches.empty() ) {
		QList<XPathMatch> new_matches;
		if (search_tags.front().tag.startsWith("@")) {
			if (search_tags.front().tag == "@text") {
// 					qDebug() << "looking for text ";
				for (int j=0; j<sub_matches.size();j++) {
					Q_ASSERT(sub_matches[j].node.isElement());
					QDomNode child = sub_matches[j].node.firstChild();
					while ( ! child.isNull() ) {
						if (child.isText() &&  ! child.nodeValue().isEmpty() )  {
							new_matches.append({ child, "", sub_matches[j].captures });
						}
						child = child.nextSibling();
					}
				}
				if (new_matches.isEmpty() && create) {
					auto text_node = sub_matches[0].node.ownerDocument().createTextNode("");
					auto match = sub_matches[0];
					match.node = sub_matches[0].node.appendChild(text_node);
					new_matches.append(match);
				}
			}
			else {
// 					qDebug() << "looking for attr " << search_tags_copy.front().replace("@","");
				QString attr_name = search_tags.front().tag.remove(0,1);
				for (int j=0; j<sub_matches.size();j++) {
					Q_ASSERT(sub_matches[j].node.isElement());
					if (sub_matches[j].node.toElement().hasAttribute(attr_name)) {
						auto match = sub_matches[j];
						match.node = sub_matches[j].node.toElement().attributeNode(attr_name);
						new_matches.append( match );
					}
				}
				if (new_matches.isEmpty() && create) {
					auto attr_node = sub_matches[0].node.ownerDocument().createAttribute(attr_name);
					auto match = sub_matches[0];
					match.node = sub_matches[0].node.appendChild(attr_node);
					new_matches.append(match);
				}
			}

		}
		else { // if search tag does not start with @
			for (int j=0; j<sub_matches.size();j++) {
				Q_ASSERT(sub_matches[j].node.isElement());
				
				QList<XPathMatch> candidates;
				if (search_tags.first().tag == "*") {
					// wildcard search
					if (search_tags.size()<2) {
						auto childs = sub_matches[j].node.childNodes();
						for (int n=0; n<childs.size(); n++ ) {
							if (!childs.item(n).isElement()) continue;
							auto match = sub_matches[j];
							match.node = childs.item(n);
							match.wildcard = match.node.nodeName();
// 							qDebug() << "wildcard_string " << match.wildcard;
							candidates.push_back ( match );
						}
					}
					else {
						search_tags.pop_front();
						auto cand_nodes = findNodeRecursive(sub_matches[j].node.toElement(), search_tags.front().tag);
						for (auto& cand : cand_nodes) {
							
							QDomNode d_node = cand.parentNode();
							if (d_node == sub_matches[j].node) {
								candidates.push_back({cand, "", sub_matches[j].captures});
// 								qDebug() << "wildcard_string " << cand.nodeName();
								continue;
							}
							
							QString wildcard_string;
							while (true) {
								wildcard_string.prepend(d_node.nodeName());
								d_node = d_node.parentNode();
								if (d_node == sub_matches[j].node)
									break;
								wildcard_string.prepend( "/" );
							}
// 							qDebug() << "wildcard_string " << wildcard_string;
							candidates.push_back({cand,wildcard_string, sub_matches[j].captures});
						}
					}
					
				}
				else {
					QDomNode child =  sub_matches[j].node.firstChild();
					while ( ! child.isNull() ) {
						if (child.isElement() && child.nodeName() == search_tags.front().tag) {
							candidates.push_back ({child,"",sub_matches[j].captures});
						}
						child = child.nextSiblingElement();
					}
				}
				
				for (auto& child : candidates  ) {
					bool all_match = true;
					// TODO check conditions extract captures ...
					QStringList captures;
					for (auto cond : search_tags.front().conditions) {
// 							qDebug() << "Checking condition " << cond.attribute << " = " << cond.value;
						auto attr = child.node.attributes().namedItem(cond.attribute);
						if ( attr.isNull() ) { all_match = false; break; }
						// Check a condition if value is present
						if (!cond.value.isEmpty() ) {
							auto value = attr.nodeValue().trimmed();
							bool r =
								cond.regex
								? QRegularExpression("\\A"+cond.value+"\\z").match(value).hasMatch()
								:  cond.value == value;

							r = cond.negate ? !r : r;
							
							if (!r) { all_match = false; break; }
						}
						// Capture the node value 
						if (cond.capture) { 
							captures.append(attr.nodeValue());
						}
					}
					
					if (all_match) {
						child.captures.append(captures);
						new_matches.append( child );
// 						qDebug() << "Matched " << search_tags.front().tag;
					}
				}
				// we might also check, whether tags==1 and tag is identical to an attribute ...
			}
			if (new_matches.isEmpty() && create) {
				auto match = sub_matches[0];
				match.node = sub_matches[0].node.ownerDocument().createElement(search_tags.front().tag);
				sub_matches[0].node.appendChild(match.node);
				for (auto cond : search_tags.front().conditions) {
					match.node.toElement().setAttribute(cond.attribute,cond.value);
				}
				new_matches.append(match);
			}
		}
		sub_matches = new_matches;
		if (search_tags.front().tag=="*") search_tags.pop_front();
		if (!search_tags.empty()) search_tags.pop_front();
	}
	
	return sub_matches;
}

QString createXPath(QDomNode node) {
	if (node.nodeType() == QDomNode::DocumentNode) return "";
	auto parentPath = createXPath(node.parentNode());
	if (node.nodeType() == QDomNode::ElementNode) {
		if (!node.previousSiblingElement(node.nodeName()).isNull() || !node.nextSiblingElement(node.nodeName()).isNull()) {
			auto attrs = node.attributes();
			
			for (const auto& unique_attr : QStringList { "symbol","symbol-ref", "type"}) {
				if (attrs.contains(unique_attr)) {
					return parentPath +"/"+node.nodeName()+"[@"+unique_attr +"=\""+attrs.namedItem(unique_attr).nodeValue()+"\"]";
				}
			}
			
			auto pchilds = node.parentNode().childNodes();
			int siblings=0;
			for ( int i=0; i<pchilds.size(); i++) {
				if (pchilds.at(i).nodeName() == node.nodeName()) {
					if (pchilds.at(i) == node) {
						return parentPath + "/" + node.nodeName() + "["+QString::number(siblings)+"]";
					}
					else siblings++;
				}
			}
		}
		// 		Look for siblings and 
		return parentPath + "/" + node.nodeName();
	}
	if (node.nodeType() == QDomNode::AttributeNode) {
		return parentPath + "/@" + node.nodeName();
	}
	return "";
}

QList<XPathMatch> matchPath(QDomDocument document, XPath search_tags, bool create = false) {
	if ( ! search_tags.isEmpty() && document.documentElement().nodeName() == search_tags.first().tag) {
		search_tags.pop_front();
		return matchPath(document.documentElement(), search_tags, create );
	}
	else {
		return {};
	}
}

QList<MorphModelEdit> MorphModel::applyFixes(QDomDocument document, const QList<MorphModel::AutoFix>& fixes) {
	QList<MorphModelEdit> edits;
	for (int i=0; i<fixes.size(); i++) {
		
// 		qDebug() << "Autofix rule nr. " << (i+1);
		auto search_path_string = fixes[i].match_path;
		auto search_path = parseXPath(search_path_string);
		QStringList search_tags;
		for (const auto& el : search_path) search_tags.append(el.tag);
		qDebug() << "Searching " << search_tags.join("/");

		QList<XPathMatch> matches = matchPath(document, search_path );
		
		if (matches.size()) {
			// moving the nodes in tree
			int n_success=0;
			for (int j=0; j<matches.size();j++) {
				auto match_search_tags = search_tags;
				auto wildcard_pos = match_search_tags.indexOf("*");
				if (wildcard_pos) {
					auto wildcard_tags = matches[j].wildcard.split("/",Qt::SkipEmptyParts);
					if (wildcard_tags.size() ==0) {
						match_search_tags.removeAt(wildcard_pos);
					}
					else {
						match_search_tags[wildcard_pos] = wildcard_tags[0];
						for (int i=1; i<wildcard_tags.size(); i++) {
							match_search_tags.insert(wildcard_pos+i, wildcard_tags[i]);
						}
					}
				}
				MorphModelEdit ed;
				if (! fixes[i].require_path.isEmpty()) {
					QString require_path = fixes[i].require_path;
					// adjust target path placeholder with the captures
					for (const auto& cap : matches[j].captures ) {
						if ( !require_path.contains("%") ) break;
						require_path = require_path.arg(cap);
					}
					
					// Relative Path resolution
					// Creating relative path for checking require_path
					QStringList search_req_tags = match_search_tags;
					auto require_tags = parseXPath(require_path);
					
					while ( !search_req_tags.isEmpty() && !require_tags.isEmpty() && search_req_tags.front() == require_tags.front().tag) {
		// 				qDebug() << "Removed leading tag " << search_req_tags.front() << " for relative path";
						search_req_tags.pop_front();
						require_tags.pop_front();
					}
					QDomElement require_parent = matches[j].node.parentNode().toElement();
					for (int i=1; i<search_req_tags.size(); i++) {
						require_parent = require_parent.parentNode().toElement();
					}
					
					if ( matchPath(require_parent, require_tags).isEmpty() ) {
						qDebug() << "Required path " << require_path << " not found.";
						continue;
					}
				}
				
				QString search_xpath = createXPath(matches[j].node);
				search_xpath.remove("/MorpheusModel");
				
				
				if (fixes[i].operation == AutoFix::DELETE) {
					if (matches[j].node.nodeType() == QDomNode::AttributeNode) {
						ed.name =  matches[j].node.nodeName();
						ed.xml_parent = matches[j].node.parentNode();
						
						matches[j].node.parentNode().toElement().removeAttribute(matches[j].node.nodeName());
						
						ed.edit_type = MorphModelEdit::AttribRemove;
						ed.info = QString("Attribute ") + search_xpath + " was removed";
						ed.severty = fixes[i].severty;
						qDebug() << ed.info;
						edits.append(ed);
					}
					else if (matches[j].node.nodeType() == QDomNode::ElementNode) {
						ed.name =  matches[j].node.nodeName();
						ed.xml_parent = matches[j].node.parentNode();
						
						matches[j].node.parentNode().removeChild(matches[j].node);
						
						ed.edit_type = MorphModelEdit::NodeRemove;
						ed.info = QString("Element ") + search_xpath + " was removed";
						ed.severty = fixes[i].severty;
						qDebug() << ed.info;
						edits.append(ed);
					}
					else if (matches[j].node.nodeType() == QDomNode::TextNode) {
						ed.name =  matches[j].node.parentNode().nodeName();
						ed.xml_parent = matches[j].node.parentNode();
						
						matches[j].node.parentNode().removeChild(matches[j].node);
						
						ed.edit_type = MorphModelEdit::NodeRemove;
						ed.info = QString("Text ") + search_xpath + " was removed";
						ed.severty = fixes[i].severty;
						qDebug() << ed.info;
						edits.append(ed);
					}
						
					continue;
				}
				
				// Creating the new Path and put the nodes there
				QString target_path = fixes[i].target_path;
				for (const auto& cap : matches[j].captures ) {
					if ( !target_path.contains("%") ) break;
					target_path = target_path.arg(cap);
				}
				if ( target_path.contains("*") ) {
					target_path.replace(target_path.indexOf("*"),1, matches[j].wildcard);
				}
				
				auto target_tags = parseXPath(target_path);
				if (target_tags.isEmpty()) continue;
				auto target_name = target_tags.takeLast();
				QString target_xpath = target_path;
				target_xpath.remove("/MorpheusModel");
				
				// Relative Path resolution
				QStringList search_target_tags = match_search_tags;
				while ( !search_target_tags.isEmpty() && !target_tags.isEmpty() && search_target_tags.front() == target_tags.front().tag) {
// 					qDebug() << "Removed leading tag " << search_target_tags.front() << " for relative path";
					search_target_tags.pop_front();
					target_tags.pop_front();
				}
				
				QDomElement target_parent = matches[j].node.parentNode().toElement();
				QString target_xpath_relative="";
				for (int i=1; i<search_target_tags.size(); i++) {
					target_parent = target_parent.parentNode().toElement();
					               target_xpath_relative += "../";
				}
				
				if (target_xpath_relative.isEmpty()) 
					               target_xpath_relative = "./";
				for (const auto& tag : target_tags ) { 
                    target_xpath_relative += tag.tag + "/"; }
				            target_xpath_relative += target_name.tag;
			
// 				qDebug() << "Moving " << createXPath(matches[j].node) << " to " << target_xpath_relative;
				// Find and if required create Path
				auto target_matches = matchPath(target_parent, target_tags, true);
				
				if (target_matches.isEmpty()) {
					qDebug() << "Autofix target not found. " << target_xpath_relative;
					continue;
				}
				else if (target_matches.size() > 1) {
					qDebug() << "Autofix target is not unique. Selecting first match ...";
				}
				target_parent = target_matches[0].node.toElement();

				if (target_name.tag.startsWith("@")) {
					
					QString attr_name = target_name.tag;
					attr_name.remove(0,1);
					
					QString node_type_string;
					if (matches[j].node.nodeType() == QDomNode::AttributeNode)
					node_type_string = "Attribute ";
					else if (matches[j].node.nodeType() == QDomNode::TextNode)
						node_type_string = "Text ";
					else {
						qDebug() << "Autofix " << search_xpath << " -> " << target_xpath_relative << "Unable to move a node to an attribute!";
						continue;
					}
					QString new_value = matches[j].node.nodeValue();
					// Purge prohibited characters
					if (attr_name != "text")
						new_value = new_value.remove(QRegularExpression("[\n\r]")).trimmed();
					if (fixes[i].value_conversions.contains(new_value)) {
						new_value = fixes[i].value_conversions[new_value];
					}
						
					ed.edit_type = MorphModelEdit::AttribRename;
					ed.xml_parent = target_parent;
					ed.name = attr_name;
					ed.severty =fixes[i].severty;
						
					if (attr_name == "text") {
						if ( target_parent == matches[j]. node.parentNode() && matches [j].node.isText() ) {
							// Text of the same parental node, just convert values
							matches[j].node.setNodeValue(new_value);
							ed.info = node_type_string + search_xpath + " was changed to " + new_value;
						}
						else if ( target_parent.text().isEmpty() || fixes[i].replace_existing ) {
							// Assign to text ...
							ed.info = node_type_string + search_xpath + (fixes[i].operation == AutoFix::MOVE ? " was moved to " : " was copied to " ) + target_xpath;

							// Create a TextNode Child, if none exists
							QDomText new_text = target_parent.ownerDocument().createTextNode(matches[j].node.nodeValue());
							target_parent.appendChild(new_text);
							edits.append(ed);
						
							if (fixes[i].operation == AutoFix::MOVE) {
								if (matches[j].node.isAttr())
									matches[j].node.parentNode().toElement().removeAttribute(matches[j].node.nodeName());
								else
									matches[j].node.parentNode().toElement().removeChild(matches[j].node);
							}
						}
						else {
							continue;
						}
					}
					else {
						if (target_parent == matches[j].node.parentNode() && matches[j].node.isAttr() &&  matches[j].node.nodeName() == attr_name ) {
							// same attribute, just convert the values
							matches[j].node.setNodeValue(new_value);
							// Assign to an attribute ...
							ed.info = node_type_string + search_xpath + " was changed to " + new_value;
						}
						else if ( fixes[i].replace_existing || ! target_parent.attributes().contains(attr_name) ) {
							// Assign to an attribute ...
							ed.info = node_type_string + createXPath(matches[j].node) + (fixes[i].operation == AutoFix::MOVE ? " was moved to " : " was copied to " ) + target_xpath;
							
							target_parent.setAttribute( ed.name, new_value);
							
							if (fixes[i].operation == AutoFix::MOVE) {
								if (matches[j].node.isAttr()) {
									matches[j].node.parentNode().toElement().removeAttribute(matches[j].node.nodeName());
								}
								else {
									matches[j].node.parentNode().toElement().removeChild(matches[j].node);
								}
							}
						}
						else {
							continue;
						}
					}
				}
				else {
					if (target_parent.namedItem(target_name.tag).isNull() || fixes[i].replace_existing) {
						QDomElement target_node;
						if (fixes[i].operation == AutoFix::MOVE) 
							target_node = target_parent.appendChild(matches[j].node).toElement();
						else 
							target_node = target_parent.appendChild(matches[j].node.cloneNode()).toElement();
						
						target_node.setTagName(target_name.tag);
						
						for (const auto& cond : target_name.conditions) {
							if (cond.attribute == "text") {
								QDomText text;
								for ( int i=0; i<target_node.childNodes().size(); i++) { 
									if (target_node.childNodes().item(i).isText()) {
										text = target_node.childNodes().item(i).toText();
										break;
									}
								};
								if (text.isNull()) {
									text = target_node.ownerDocument().createTextNode(cond.value);
									target_node.appendChild(text);
								}
								else {
									text.setNodeValue(cond.value);
								}
							}
							else
								target_node.setAttribute(cond.attribute, cond.value);
						}
						
						ed.name =  matches[j].node.attributes().namedItem("name").nodeValue();
						ed.xml_parent = target_node;
						if (fixes[i].operation == AutoFix::MOVE) {
							ed.edit_type = MorphModelEdit::NodeMove;
							ed.info = QString("Element ") + search_xpath + " was moved to " + target_xpath;
							ed.severty =fixes[i].severty;
						}
						else {
							ed.edit_type = MorphModelEdit::NodeAdd;
							ed.info = QString("Element ") + search_xpath + "\nwas copied to " + target_xpath;
							ed.severty =fixes[i].severty;
						}
						
						if (ed.name.isEmpty())
							ed.name = target_name.tag;
					}
					
				}
				qDebug() << ed.info;
				edits.append(ed);
				n_success++;
			}
			if (matches.size())
				qDebug() << "AutoFix: Moved " << n_success << " element(s) " << " matching " << search_path_string << " to " << fixes[i].target_path;
		}
	}
	return edits;
}


//------------------------------------------------------------------------------



QList<MorphModelEdit>  MorphModel::applyAutoFixes(QDomDocument document) {

	int morpheus_file_version = document.documentElement().attribute("version","1").toInt();
	qDebug() << "Reading version " << morpheus_file_version << " from Element " << document.documentElement().nodeName();
	// distrust these versions were fully upgraded
	bool silent_upgrade = false;
	if (morpheus_file_version == 4 || morpheus_file_version == 3 ) {
		morpheus_file_version--;
		silent_upgrade = true;
	}
	// we suspect that not all required patches for the current version have been applied yet if it's a version under development ...
	if (morpheus_ml_version_dev && morpheus_file_version == morpheus_ml_version) morpheus_file_version--; 
	QList<MorphModelEdit> edits;
	
	auto conversion_rules = MorpheusML::getRuleSets();
	
	if (morpheus_file_version != morpheus_ml_version) {
		while (morpheus_file_version < morpheus_ml_version) {
			
			int fix_version = morpheus_file_version+1;
			auto rules = conversion_rules.find(std::pair<int,int>(morpheus_file_version,fix_version));
			
			if ( rules != conversion_rules.end() ) {
				QList<MorphModel::AutoFix> qt_rules;
				for  (const auto& rule : rules->second ) {
					MorphModel::AutoFix qt_rule;
					qt_rule.match_path = QString::fromStdString(rule.match_path);
					qt_rule.target_path = QString::fromStdString(rule.target_path);
					qt_rule.require_path = QString::fromStdString(rule.require_path);
					qt_rule.replace_existing = rule.replace_existing;
					switch (rule.operation) {
						case MorpheusML::AutoFix::COPY   : qt_rule.operation = AutoFix::COPY; break;
						case MorpheusML::AutoFix::MOVE   : qt_rule.operation = AutoFix::MOVE; break;
						case MorpheusML::AutoFix::DELETE : qt_rule.operation = AutoFix::DELETE; break;
					}
					switch (rule.severty) {
						case MorpheusML::AutoFix::Severty::SILENT : qt_rule.severty = AutoFix::Severty::SILENT; break;
						case MorpheusML::AutoFix::Severty::INFO   : qt_rule.severty = AutoFix::Severty::INFO; break;
						case MorpheusML::AutoFix::Severty::CHECK  : qt_rule.severty = AutoFix::Severty::CHECK; break;
						case MorpheusML::AutoFix::Severty::WARN   : qt_rule.severty = AutoFix::Severty::WARN; break;
						case MorpheusML::AutoFix::Severty::ERROR  : qt_rule.severty = AutoFix::Severty::ERROR; break;
					}
					for (const auto& repl : rule.value_conversions) {
						qt_rule.value_conversions[QString::fromStdString(repl.first)] = QString::fromStdString(repl.second);
					}
					qt_rules.append(qt_rule);
				}
				
				auto applied = applyFixes(document, qt_rules);
				document.documentElement().setAttribute( "version", QString::number(fix_version) );
				if (!silent_upgrade) 
					edits.append(applied);
				else
					silent_upgrade = false;
			}
			else {
				throw  ModelException(ModelException::InvalidVersion, QString("Incompatible MorpheusML version %1").arg(morpheus_file_version) );
			}
			
			morpheus_file_version = fix_version;
		}
	}
// 	else if (morpheus_file_version == 4) {
// 		MorphModel::AutoFix event_rule;
// 		event_rule.match_path = "/MorpheusModel/*/Event/@trigger";
// 		event_rule.target_path = "/MorpheusModel/*/Event/@trigger";
// 		event_rule.severty = AutoFix::Severty::SILENT;
// 		event_rule.operation = AutoFix::Operation::MOVE;
// 		event_rule.value_conversions = {{"when true","when-true"},{"on change","on-change"}};
// 		applyFixes(document, {event_rule} );
// 	}
	
	document.documentElement().setAttribute( "version", QString::number(morpheus_ml_version) );
	
	qDebug() << "Number of Edits applied: " << edits.size();
	return edits;
}

bool MorphModel::close()
{
    bool modelChanged = rootNodeContr->getModelDescr().change_count > 1;

    if ( modelChanged ) {
        // The current model was changed
        QMessageBox msgBox(qApp->activeWindow());
        msgBox.setText(QString("Model %1 has been changed.").arg(xml_file.name));
        msgBox.setInformativeText("Do you want to save changes?");
        msgBox.setStandardButtons( QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);
        msgBox.setDefaultButton(QMessageBox::Save);
        int answer = msgBox.exec();

        switch(answer){
            case QMessageBox::Cancel:
                return false;

            case QMessageBox::Save:
                {
                    if (xml_file.name.isEmpty()) {
                        if (! xml_file.saveAsDialog() )
                            return false;
                    }
                    else {
                        xml_file.save();
                    }
                    break;
                }
            case QMessageBox::Discard:
                break;
        }
    }
    return true;
}

//------------------------------------------------------------------------------

QModelIndex MorphModel::index(int row, int column, const QModelIndex &parent) const {
    if (parent.isValid()) {
        if (row<0 || row >= indexToItem(parent)->childs.size()) {
            qDebug() << "Unable to create index for child " << row << "," << column << " of " << indexToItem(parent)->name<< "(" <<indexToItem(parent)->childs.size() <<")";
            return QModelIndex();
        }
        return createIndex(row, column, indexToItem(parent)->childs[row]) ;
    }
    else
    {
        if (row<0 || row >= rootNodeContr->childs.size()) {
            qDebug() << "Unable to create index for root child " << row << "," << column << " of " << rootNodeContr->name<< "(" <<rootNodeContr->childs.size() <<")";
            return QModelIndex();
        }
        return createIndex(row, column, rootNodeContr->childs[row]) ;
    }

}

//------------------------------------------------------------------------------

QModelIndex MorphModel::parent(const QModelIndex &child) const {
   if (child.isValid()) {
       return itemToIndex(indexToItem(child)->parent);
   }
   else
       return QModelIndex();
}

//------------------------------------------------------------------------------

Qt::ItemFlags MorphModel::flags( const QModelIndex & index ) const {

	if (index.isValid()) {
		if (indexToItem(index)->isDisabled())
			return Qt::ItemIsDragEnabled | Qt::ItemIsSelectable |  Qt::ItemIsEnabled;
		else if (indexToItem(index)->isInheritedDisabled()) 
			return Qt::ItemIsSelectable |  Qt::ItemIsEnabled;
		else
			return Qt::ItemIsDragEnabled | Qt::ItemIsDropEnabled |  Qt::ItemIsEnabled | Qt::ItemIsSelectable;
	}
	else
		return Qt::NoItemFlags; // TODO Qt::ItemIsDropEnabled | Qt::ItemIsEnabled ;
}

//------------------------------------------------------------------------------

int MorphModel::columnCount(const QModelIndex &/*parent*/) const {
    return 3;
}

//------------------------------------------------------------------------------

int MorphModel::rowCount(const QModelIndex &parent) const {
	nodeController* node = indexToItem(parent);
    if (node)
        return node->getChilds().size();
    else
        return 0;
}

//------------------------------------------------------------------------------


QVariant MorphModel::data(const QModelIndex &index, int role) const {
	if (role == Qt::DisplayRole) {

		switch(index.column()) {

			case 0: return indexToItem(index)->name;

			case 1: {
				QString val;
				nodeController* node = indexToItem(index);
				AbstractAttribute* name = node->attribute("name"); if (name && ! name->isActive()) name = NULL;
				AbstractAttribute* value = node->attribute("value"); if (value && ! value->isActive()) value = NULL;
				AbstractAttribute* symbol = node->attribute("symbol"); if (symbol && ! symbol->isActive()) symbol = NULL;
				AbstractAttribute* symbol_ref = node->attribute("symbol-ref"); if (symbol_ref && ! symbol_ref->isActive()) symbol_ref = NULL;

				if ( node->name == "DiffEqn" ) {
					if (symbol_ref) val = QString("d%1 / dt = ").arg(symbol_ref->get());
				}
				else if ( node->name== "Equation" || node->name== "Rule" ) {
					if (symbol_ref) val = symbol_ref->get() + " = ";
				}
				else if (node->name== "Function") {
					if (symbol) val = symbol->get() + " = ";
				}
				else if (node->name == "Contact") {
					AbstractAttribute* t1=node->attribute("type1");
					AbstractAttribute* t2=node->attribute("type2");
					if (t1 && t2)  {
						val = t1->get() + " - " + t2->get();
					}
				}
				else if (symbol) {
					if ( value ) 
						val = symbol->get() + " = ";
					else
						val = symbol->get();
					
				}
				else if ( symbol_ref && value ) {
					val = symbol_ref->get() + " = ";
				}
				else {
					if (name) {
						if (symbol)
							val = QString("%2 [%1]").arg(name->get(),symbol->get());
						else
							val = name->get();
					}
					else if (symbol) {
							val = symbol->get();
					}
					else {
						return QVariant();
					}
				}
				return val.replace(QRegExp("\\s+")," ");
			}
			case 2: {
				QString val;
				nodeController* node = indexToItem(index);
				nodeController* text_node(node);
				AbstractAttribute* value = node->attribute("value"); if (value && ! value->isActive()) value = NULL;
				AbstractAttribute* symbol_ref=node->attribute("symbol-ref"); if (symbol_ref && ! symbol_ref->isActive()) symbol_ref = NULL;

//                qDebug() << "node->name: " << node->name  << " | value: "<<  (value?value->get():"N.A.")  << " | symbol-ref: "<< (symbol_ref ? symbol_ref->get() : "N.A.") ;

				if ( node->name == "Equation" || node->name== "Rule" || node->name == "Function" || node->name == "DiffEqn" || node->name == "InitPDEExpression") {
					text_node = node->firstActiveChild("Expression");
					if (text_node) {
						
						val = text_node->getText();
						if (val.length() > 100 )
							val = val.left(60) + " ...";
					}
				}
				else if (node->name == "Expression" ){
					val = "";
				}
				else if (node->name == "Lattice") {
					val = node->attribute("class")->get();
				}
				else if ( text_node->hasText() ) {
					val = text_node->getText();
					if (val.length() > 100 )
						val = val.left(60) + " ...";
				}
				else if (value) {
					val = value->get();
				}
				else if (symbol_ref) {
					val = symbol_ref->get();
				}
				else {
					return QVariant();
				}
				return val.replace(QRegExp("\\s+")," ");
			}

		}
	}
	else if (role==Qt::ForegroundRole) {
		nodeController* item = indexToItem(index);
		if (item->isDisabled() || item->isInheritedDisabled()) {
			return QBrush(Qt::darkGray);
		}
		else if (index.column()==0 && ( item->getName() == "System" || item->getName() == "CellType" || item->getName() == "PDE" || item->getName() == "Triggers") )
			return QBrush(QColor(0,100,180));
	}
	else if (role==Qt::FontRole) {
		switch (index.column()) {
			case 1: {
				nodeController* node = indexToItem(index);
//                AbstractAttribute* value=node->attribute("value");
				AbstractAttribute* symbol_ref=node->attribute("symbol-ref");
				if ( symbol_ref && symbol_ref->isActive()) {
					QFont font = QApplication::font();
					font.setItalic(true);
					return font;
				}
			}

		}
	}
	else if (role==Qt::DecorationRole && index.column() == 0) {
		nodeController * node = indexToItem(index);
		if ( node->isDisabled() /*&& ! node->isInheritedDisabled()*/) {
			return QIcon::fromTheme("media-pause",QApplication::style()->standardIcon(QStyle::SP_MediaPause));
		}
		if ( isSweeperAttribute( node->textAttribute())  ) {
			return QIcon::fromTheme("media-seek-forward",QApplication::style()->standardIcon(QStyle::SP_MediaSeekForward));
		}
		if ( node->attribute("value") && isSweeperAttribute( node->attribute("value")) ) {
			return QIcon::fromTheme("media-seek-forward",QApplication::style()->standardIcon(QStyle::SP_MediaSeekForward));
		}
	}
	else if (role == NodeRole) {
		return QVariant::fromValue(indexToItem(index));
	}
	else if (role == XPathRole) {
		auto node = indexToItem(index);
		return node ? QVariant(node->getXPath()) : QVariant(QStringList());
	}
	else if (role == TagsRole) {
		auto node = indexToItem(index);
		if (node->allowsTags())
			return QVariant(node->getEffectiveTags());
		else 
			return QVariant(QStringList("*"));
	}
	return QVariant();
}

//------------------------------------------------------------------------------

QVariant MorphModel::headerData( int section, Qt::Orientation orientation, int role ) const {
	
	if (orientation == Qt::Horizontal) {
		if (role == Qt::DisplayRole) {
			switch (section) {
				case 0: return QVariant("Element");
				case 1: return QVariant("Name/Symbol");
				case 2: return QVariant("Value");
			}
		}
	}
	return QVariant();
}

//------------------------------------------------------------------------------

nodeController* MorphModel::indexToItem(const QModelIndex& idx) const {
	if (idx.isValid()) {
		if (idx.model() != this) {
			qDebug() << "Cannot convert index from wrong model!";
			return nullptr;
		}
		return static_cast< nodeController* >(idx.internalPointer());
	}
	else {
		return rootNodeContr;
	}
}

//------------------------------------------------------------------------------

QModelIndex MorphModel::itemToIndex(nodeController* node) const {
	if (node == NULL) {
		return QModelIndex();
	}
    if (node == rootNodeContr)
		return QModelIndex();
    else {
        Q_ASSERT(node->parent);
        int pos = node->parent->childIndex(node);
        if (pos == -1) {
            qDebug() << "Unable to locate child node " << node->name << " " << node;
            return QModelIndex();
        }
        return createIndex(pos,0,(void*)node);
    }
}


void MorphModel::prepareActivationOrInsert(nodeController* node, QString name) {
	
	auto childInfo = node->childInformation();
	
	if (childInfo.is_choice) {
		while (childInfo.max_occurs != "unbounded" && node->activeChilds().size() >= childInfo.max_occurs.toInt()) {
			qDebug() << "Enabled node is exchanging";
			auto other = node->firstActiveChild();
			other->setDisabled(true);
			qDebug() << "Disabling node " << other->getName() ;
			emit dataChanged(itemToIndex(other), itemToIndex(other));
		}
	}
	else if (childInfo.max_occurs != "unbounded" && childInfo.children[name].max_occurs != "unbounded") {
		while (childInfo.max_occurs.toInt() * childInfo.children[name].max_occurs.toInt() <= node->activeChilds(name).size() ) {
			nodeController* other =  node->firstActiveChild(name);
			other->setDisabled(true);
			emit dataChanged(itemToIndex(other),itemToIndex(other));
		}
	}
}
//------------------------------------------------------------------------------

QModelIndex MorphModel::insertNode(const QModelIndex &parent, QDomNode child, int pos) {
	nodeController* contr = indexToItem(parent);
	QModelIndex result;
	try {
		if ( ! contr )
			throw ModelException(ModelException::InvalidNodeIndex, QString("MorphModel::insertNode: Request to insert into invalid index!"));
		
		if (! child.isComment() ) {
			prepareActivationOrInsert(contr, child.nodeName());
			auto info = contr->childInformation();
		}
		
		if (pos<0 || pos>contr->getChilds().size()) {
			pos = contr->getChilds().size();
		}
		
// 		qDebug() << "Inserting Node " << child.nodeName() << " into " << contr ->getName() << " at " << pos;
		
		beginInsertRows(parent.sibling(parent.row(),0), pos, pos);
		auto child_node = contr->insertChild(child,pos);
		endInsertRows();
		result = itemToIndex(child_node);
	} 
	catch (ModelException e) {
		QMessageBox::critical(NULL,"Insert Node Error",e.message);
	}
	catch (...)  {
		QMessageBox::critical(NULL,"Insert Node Error","Unknown error while inserting a Data Node into the Model");
	}
	return result;
}

//------------------------------------------------------------------------------

QModelIndex MorphModel::insertNode(const QModelIndex &parent, QString child, int pos)
{
	nodeController* contr = indexToItem(parent);
// 	if (!contr) { qDebug() << "MorphModel::insertNode received invalid index " << parent; return QModelIndex(); }
	QModelIndex result;
	try {
	
		if ( ! contr )
			throw ModelException(ModelException::InvalidNodeIndex,QString("MorphModel::insertNode: Request to insert into invalid index!"));
		if (!contr->getAddableChilds(true).contains(child))
			throw ModelException(ModelException::UndefinedNode,QString("MorphModel::insertNode: Requested node %1 cannot be inserted!").arg(child));
		prepareActivationOrInsert(contr, child);
		
		if (pos<0 || pos>contr->getChilds().size()) {
			pos = contr->getChilds().size();
		}
// 		qDebug() << "Inserting Node " << child << " into " << contr ->getName() << " at " << pos;
		beginInsertRows(parent.sibling(parent.row(),0), pos, pos);
		auto child_node = contr->insertChild(child,pos);
		endInsertRows();
		result = itemToIndex(child_node);
	} 
	catch (ModelException e) {
		QMessageBox::critical(NULL,"Insert Node Error",e.message);
	}
	catch (QString e) {
		QMessageBox::critical(NULL,"Insert Node Error",e);
	}
	catch (...)  {
		QMessageBox::critical(NULL,"Insert Node Error","Unknown error while inserting a Data Node into the Model");
	}
	return result;
}

//------------------------------------------------------------------------------


QSharedPointer<nodeController> MorphModel::removeNode(const QModelIndex &parent, int row) {
	if (indexToItem(parent)->getChilds().size()<=row) {
		qDebug() << "MorphModel::removeNode out of bounds " << indexToItem(parent)->childs[row]->name << "["<<row<<"]";
		return QSharedPointer<nodeController>();
	}
	if (parent == itemToIndex(getRoot())) {
		auto node = index(row,0,parent).data(MorphModel::NodeRole).value<nodeController*>();
		int part_id = MorphModelPart::all_parts_index[node->getName()];
		if (parts.at(part_id).enabled) {
			qDebug() << "Forwarding part removal" << part_id;
			return removePart(part_id);
		}
	}

// 	qDebug() << "removing Node" << index(row,0,parent).data(MorphModel::NodeRole).value<nodeController*>()->getName();
	beginRemoveRows(parent.sibling(parent.row(),0), row, row);
	// --> the row of the view might not be identical to the row in the model !
	auto child = QSharedPointer<nodeController>(indexToItem(parent)->removeChild( row ));
	endRemoveRows();
	return child;
}

void MorphModel::setDisabled(const QModelIndex &node_index, bool disable) {
	nodeController* node = indexToItem(node_index);
	if (node) {
		if (disable == node->isDisabled()) return;
		if (!disable) prepareActivationOrInsert(node->parent, node->name);
		node->setDisabled(disable);
		emit dataChanged(node_index, node_index);
	}
}

void MorphModel::notifyNodeInsertion(nodeController* parent, int first_row, int last_row) {
	auto parent_idx = itemToIndex(parent);
	beginInsertRows(parent_idx, first_row, last_row);
	endInsertRows();
}

// ------------------------------------------------------------------------------

QStringList MorphModel::mimeTypes () const {
    QStringList types;
    types << "text/plain" << IndexListMimeData::indexList;
    return types;
}

// ------------------------------------------------------------------------------

QMimeData* MorphModel::mimeData(const QModelIndexList &indexes) const {
    if ( ! indexes.empty()) {
        QModelIndex dragIndex = indexes.front();

        IndexListMimeData* m_data = new IndexListMimeData();
        m_data->setIndexList(indexes);

        QDomNode xml = indexToItem(dragIndex)-> xmlNode.cloneNode();
        QDomDocument doc("");
        doc.appendChild(xml);
        m_data->setText(doc.toString());

        return m_data;
    }
    return 0;
}

//------------------------------------------------------------------------------

bool MorphModel::canDropMimeData(const QMimeData *data, Qt::DropAction action, int row, int column, const QModelIndex &parent) const {
	if (data->formats().contains(IndexListMimeData::indexList)) {
		const IndexListMimeData* tree_data = qobject_cast<const IndexListMimeData*>(data);
		QModelIndex move_index = tree_data->getIndexList().front();
		nodeController* move_contr = indexToItem(move_index);
		return indexToItem(parent)->canInsertChild(move_contr,row);
	}
	else if (data->hasText()) {
		QDomDocument doc;
		doc.setContent(data->text());
		return indexToItem(parent)->canInsertChild(doc.documentElement().nodeName(),row);
	}
	return false;
}

bool MorphModel::dropMimeData( const QMimeData * data, Qt::DropAction action, int row, int column, const QModelIndex & new_parent ) {
	if (action == Qt::MoveAction) {
		if (data->formats().contains(IndexListMimeData::indexList)) {
			const IndexListMimeData* tree_data = qobject_cast<const IndexListMimeData*>(data);
			QModelIndex move_index = tree_data->getIndexList().front();
			nodeController* move_contr = indexToItem(move_index);
//            qDebug() << "unpacked node " << contr->name;
			if (row==-1)
					row = rowCount(new_parent);

			if (move_index.parent() == new_parent) {
				// Remember, that moving the element changes indices of the subsequent elements ...
				int final_row;
				if (move_index.row()<row)
					final_row = row-1;
				else {
					final_row = row;
				}
				if (final_row == move_index.row())
					return true;
//              qDebug() << "Moving child within parent " << move_contr->name << " from " << move_index.row() << " to " << final_row << " of " << rowCount(move_index.parent()); 
					// Obviously, notifiers for the view need the old indices ...
				if ( ! beginMoveRows(move_index.parent(),move_index.row(),move_index.row(),new_parent,row) ) {
					qDebug() << "Dropped DnD move due to invalid beginMoveRows().";
					return false;
				}

				// Just reorder the nodes of the parent node controller.
				indexToItem(new_parent)->moveChild(move_index.row(),final_row);
				endMoveRows();
				return true;
			}
			else {
				// Reparent the node ...
				if (indexToItem(new_parent)->canInsertChild(move_contr,row) && ! move_contr->parent->isChildRequired(move_contr) ) {

// 				qDebug() << "Moving child " << move_contr->name << " from " << move_contr->parent->getName()<< "[" << move_index.row() << "] to " << indexToItem(new_parent)->getName()<< "[" << row << "] of " << rowCount(new_parent);
				
				// Determine wether to disable some other node in the new parent
				if ( ! move_contr->isDisabled() ) {
					prepareActivationOrInsert(indexToItem(new_parent), move_contr->getName());
// 						nodeController::NodeInfo childInfo = indexToItem(new_parent)->childInformation(move_contr->getName());
// 						if (childInfo.is_exchanging) {
// 							nodeController* groupChild = indexToItem(new_parent)->findGroupChild(childInfo.group);
// 							if (groupChild) {
// 								groupChild->setDisabled(true);
// 								dataChanged(itemToIndex(groupChild),itemToIndex(groupChild));
// 							}
// 						}
// 						else if (childInfo.maxOccure.toInt() == 1 && indexToItem(new_parent)->firstChild(move_contr->getName()) != NULL ) {
// 							nodeController* uniqueChild = indexToItem(new_parent)->firstChild(move_contr->getName());
// 							uniqueChild->setDisabled(true);
// 							dataChanged(itemToIndex(uniqueChild),itemToIndex(uniqueChild));
// 						}
				}

				if ( ! beginMoveRows(move_index.parent(),move_index.row(),move_index.row(),new_parent,row) ) {
					qDebug() << "Dropped DnD move due to invalid beginMoveRows().";
					return false;
				}

				if ( ! indexToItem(new_parent)->insertChild(move_contr,row)) {
					qDebug() << "oops, someting went wrong in move";
				}
				endMoveRows();
				return true;
			}
				else qDebug() << "Dropped DnD move ...";
			}
		}
		else {
		// External data is always copied
			action = Qt::CopyAction;
		}
	}

	if (action==Qt::CopyAction) {
		if (data->hasText()) {
			QDomDocument doc;
			doc.setContent(data->text());
			if (indexToItem(new_parent)->canInsertChild(doc.documentElement().nodeName(),row)) {
				return insertNode(new_parent,doc.documentElement(),row).isValid();
			}
			else if ( doc.firstChild().isComment() ) {
				return insertNode(new_parent,doc.firstChild(),row).isValid();
			}
			else {
				qDebug() << data->text();
				qDebug() << doc.firstChild().isComment();
				qDebug() << "Not accepting d&d node " <<doc.documentElement().nodeName() << " for parent " <<indexToItem(new_parent)->getName();
			}
		}
	}
//    internalDragIndex = QModelIndex();
	return false;
}

//------------------------------------------------------------------------------

nodeController* MorphModel::find(QStringList path, bool create) {
	// Check whether the part exists
	if (path.first() == "MorpheusModel") path.takeFirst();
	auto part = path.first();
	
	if (!MorphModelPart::all_parts_index.contains(part))
		return nullptr;
	int idx = MorphModelPart::all_parts_index.value(part);
	if (! parts[idx].enabled) {
		if (create)
			activatePart(idx);
		else
			return nullptr;
	}
	
	return rootNodeContr->find(path,create);
}


bool MorphModel::addPart(QString name) {
	if (!MorphModelPart::all_parts_index.contains(name))
		return false;
	int idx = MorphModelPart::all_parts_index.value(name);
	return activatePart(idx);
}


bool MorphModel::activatePart(int idx) {
	if (idx<0 || idx>=parts.size())
		return false;
	auto& part = parts[idx];
	if (!part.enabled) {
		auto element_index = insertNode(QModelIndex(),part.label); //  TODO itemToIndex(rootNodeContr)
		if (! element_index.isValid())
			return false;
		part.element = indexToItem(element_index);
		part.enabled = true;
		emit modelPartAdded(idx);
	}
    return true;
}


bool MorphModel::addPart(QDomNode xml) {
	if (!MorphModelPart::all_parts_index.contains(xml.nodeName()))
		return false;
	
	int idx = MorphModelPart::all_parts_index.value(xml.nodeName());
	auto& part = parts[idx];
	if (!part.enabled) {
		auto element_index = insertNode(itemToIndex(rootNodeContr),xml);
		if (! element_index.isValid())
			return false;
		part.element = indexToItem(element_index);
		emit modelPartAdded(idx);
	}
    return true;
}

QSharedPointer<nodeController> MorphModel::removePart(QString part) {
	if (!MorphModelPart::all_parts_index.contains(part))
		return QSharedPointer<nodeController>();
	return removePart(MorphModelPart::all_parts_index.value(part));
}

QSharedPointer<nodeController> MorphModel::removePart(int idx) {
	if (idx<0 || idx>=parts.size())
		return QSharedPointer<nodeController>();
	if (parts[idx].enabled) {
		auto element_index = itemToIndex(parts[idx].element);
		parts[idx].enabled = false;
		parts[idx].element = nullptr;
		emit modelPartRemoved( idx );
		auto node = removeNode(itemToIndex(rootNodeContr), element_index.row());
		return node;
	}
	return QSharedPointer<nodeController>();
	
}

void MorphModel::loadModelParts() {

	const QList<nodeController* >& xml_parts = rootNodeContr->getChilds();
	QMap<QString, nodeController*> named_xml_parts;
	for (auto part : xml_parts) named_xml_parts[part->getName()] = part;
	
	auto all_parts = MorphModelPart::all_parts_sorted;
	
	parts.clear();
	for (int i=0; i< all_parts.size(); i++ ) {
		MorphModelPart part;
		part.label = all_parts[i];
		part.enabled = named_xml_parts.contains(part.label);
		if (part.enabled) {
			part.element = named_xml_parts[part.label];
		}
		parts.push_back(part);
	}

	MorphModelPart param_sweep;
	param_sweep.label = "ParamSweep";
	param_sweep.enabled = true;
	parts.push_back(param_sweep);
}

//------------------------------------------------------------------------------


bool MorphModel::isSweeperAttribute(AbstractAttribute* attr) const
{
	return param_sweep.contains(attr);
}

//------------------------------------------------------------------------------

void MorphModel::removeSweeperAttribute(AbstractAttribute* attr) {
	param_sweep.removeAttribute(attr);
	disconnect(attr, SIGNAL(deleted(AbstractAttribute*)),&param_sweep,SLOT(removeAttribute(AbstractAttribute*)));
}


bool MorphModel::addSweeperAttribute(AbstractAttribute* attr) {

	param_sweep.addAttribute(attr);
	connect(attr, SIGNAL(deleted(AbstractAttribute*)),&param_sweep,SLOT(removeAttribute(AbstractAttribute*)));
    if ( ! isSweeperAttribute(attr) ) {
        QList<AbstractAttribute*> s;
        s.append(attr);
        return true;
    }
    else return false;
}

bool MorphModel::isEmpty() const {
    return xml_file.is_plain_model && rootNodeContr->getModelDescr().change_count==0;
}

