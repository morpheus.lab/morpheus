//////
//
// This file is part of the modelling and simulation framework 'Morpheus',
// and is made available under the terms of the BSD 3-clause license (see LICENSE
// file that comes with the distribution or https://opensource.org/licenses/BSD-3-Clause).
//
// Authors:  Joern Starruss and Walter de Back
// Copyright 2009-2016, Technische Universität Dresden, Germany
//
//////

#ifndef SBML_IMPORT_H
#define SBML_IMPORT_H

#include "morpheusML/morpheus_model.h"
#ifndef HAVE_LIBSBML
namespace SBMLImporter {
	const bool supported = false;
	inline SharedMorphModel importSBML(QString path = "") { return SharedMorphModel(); };
	inline SharedMorphModel importSBML(QByteArray data, bool global = true) { return SharedMorphModel(); };
	
	inline SharedMorphModel importSBMLTest(QString file) { return SharedMorphModel(); };
}

namespace SBML2MorpheusML_Converter {
	using ResultType = std::pair<SharedMorphModel,QStringList>;
	struct TargetDescr {
		SharedMorphModel model;
		QString cell_type;
		QString tag;
	};
	inline ResultType readSBML(QString path, TargetDescr target) { return {SharedMorphModel(),{}}; };
}

#else // HAVE_LIBSBML
#include "sbml_import_impl.h"

#endif

#endif // SBML_IMPORT_H
