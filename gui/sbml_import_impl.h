#ifndef SBML_IMPORT_IMPL_H
#define SBML_IMPORT_IMPL_H

#include <QDomDocument>
#include <QMap>
#include <QStringList>
#include <QDialog>
#include <QLineEdit>
#include <QLabel>
#include <QCheckBox>
#include <QPushButton>
#include <QGroupBox>
#include <QLayout>
#include <QStyle>
#include <QDebug>
#include "config.h"
#include "sbml_converter.h"

class SBMLImporter: public QDialog {
	Q_OBJECT
public:
	SBMLImporter(QWidget* parent, SharedMorphModel current_model);
	
// 	SharedMorphModel getMorpheusModel() { return model;};
	bool haveNewModel() { return model_created; };
// the interface for making this feature pluggable
	static const bool supported = true;
 	static SharedMorphModel importSBML(QString path = "");
	static SharedMorphModel importSBML(QByteArray data, bool global = true);
// 	static SharedMorphModel importSEDMLTest(QString file);
// 	static SharedMorphModel importSBMLTest(QString file);
// 	static SharedMorphModel convertSBML(QString file);
// 	static SharedMorphModel convertSBML(QByteArray data);
	
private:
	QLineEdit* path;
	QPushButton *path_dlg;
	QComboBox* into_celltype;
	QLineEdit* tag;
	bool model_created;
	SharedMorphModel model;
	
	/// Use Dialog data to import the SBML file. @p data will override the SBML model in the Dialog.
	SBML2MorpheusML_Converter::ResultType import(QByteArray data = QByteArray());

private slots:
	void fileDialog();
};

#endif // SBML_IMPORT_IMPL_H
